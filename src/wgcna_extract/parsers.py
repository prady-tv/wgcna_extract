#!/usr/bin/env python3
# -*- coding: utf-8; mode:python; -*-
"""Commonly used parsers"""

from csv import reader
from functools import reduce
from os import PathLike
from pathlib import Path
from typing import Dict, Iterable, Optional, Tuple

import numpy as np
import pandas as pd


def _fp_to_farb(fp: Path) -> str:
    return str(fp).split('-')[-1].split('.')[0]


def _clean_str(instr: str, clean: Iterable[str]) -> str:
    """remove each item from clean, that occurs in instr"""
    return reduce(lambda x, y: x.replace(y, ''), clean, instr)


class GxfRecord:
    """
    Record wrapper for gxf files

    Args:
        gxf: Gxf File path (x may be {f, t, a, ...})
        columns: names of gxf table columns
        id_handle: column-name of id
        attr_handle: column-name of attributes
        attr_sep: delimiter of attributes
        attr_parser: key-value-separator of attributes
        drop: drop these characters if they appear in values
    """

    def __init__(self, gxf: PathLike, columns: Iterable[str], **kwargs):
        self._gxf: Path = Path(gxf)
        self.columns = pd.Index(tuple(columns) or ())
        """Names of columns"""

        self.id_handle: str = kwargs.get('id_handle', 'locus_tag')
        """column-name to be set as id"""

        self.attr_handle: str = kwargs.get('attr_handle', 'attributes')
        """column-name to be used as attribute name"""

        self.attr_sep: str = kwargs.get('attr_sep', ';')
        """Attributes are delimited by this character"""

        self.attr_parser: str = kwargs.get('attr_parser', ' ')
        """Attributes are associated by this character"""

        self.drop: Tuple[str] = kwargs.get('drop', ())
        """Drop characters from list if they appear in attribute value"""

        self.translator = kwargs.get('translator', 'note')
        """Drop characters from list if they appear in attribute value"""

        self._records: Optional[pd.DataFrame] = None
        self._extd_records: Optional[pd.DataFrame] = None

    @property
    def records(self) -> pd.DataFrame:
        """gxf records"""
        if self._records is None:
            self._records = self._parse_records()

            # drop empty
            self._records.replace('', np.nan, inplace=True)
            self._records.dropna(axis=1, how='all', inplace=True)
        return self._records

    @records.setter
    def records(self, value: pd.DataFrame):
        self._records = value

    @records.deleter
    def records(self):
        self._records = None

    @property
    def attributes(self) -> pd.DataFrame:
        """Parsed attributes"""
        return self.records.copy().set_index(self.id_handle)

    def __repr__(self):
        return '\n'.join([
            f'{attr}: {getattr(self, attr, None)}'
            for attr in ('id_handle', 'attr_handle', 'attr_sep', 'attr_parser',
                         'drop', 'columns')
        ])

    def _parse_gxf_entry(self, entry_line: str) -> Optional[Dict[str, str]]:
        """
        Parse single entry

        DONT: No need to parallellize

        Args:
            entry_line: Line read from GFF records. Comment lines are ignored.

        Returns:
            ``None`` if comment, else, entry resolved into genomic features
        """
        if '\t' not in entry_line:
            return None
        if entry_line[0] in ('#', '!', ">"):
            return None

        # recrods as they appear in gff file
        entry_records = {
            key: value
            for key, value in zip(self.columns,
                                  entry_line.rstrip('\n').split('\t'))
        }

        # resolve attributes record using appropriate separator
        if self.attr_sep:
            for attr_list in entry_records[self.attr_handle].split(
                    self.attr_sep):
                key, *val = attr_list.split(self.attr_parser)
                entry_records[key] = _clean_str('_'.join(val), self.drop)
        return entry_records

    def _parse_records(self) -> pd.DataFrame:
        """Record-parser"""
        with open(self._gxf) as gxf_h:
            gxf_lines = gxf_h.readlines()
        records = [self._parse_gxf_entry(entry) for entry in gxf_lines]
        return pd.DataFrame([rec for rec in records if rec is not None])

    def translate(self, gid: Optional[str] = None) -> Optional[str]:
        """
        if gid is string: try translation

        if cgo translate or if gid is ``None``, return ``None``
        """
        if gid is None:
            return None
        try:
            return self.attributes.loc[gid, self.translator]
        except KeyError:
            return None


class GffRecord(GxfRecord):

    def __init__(self,
                 gff_file,
                 columns: Iterable[str] = ('seqid', 'source', 'type', 'start',
                                           'end', 'score', 'strand', 'phase',
                                           'attributes'),
                 **kwargs):
        # defaults for gff file
        id_handle = kwargs.get('id_handle') or 'locus_tag'
        attr_handle = kwargs.get('attr_handle') or 'attributes'
        attr_sep = kwargs.get('attr_sep') or ';'
        attr_parser = kwargs.get('attr_parser') or '='
        drop = kwargs.get('drop') or ('"', "'")
        super().__init__(gff_file,
                         columns,
                         id_handle=id_handle,
                         attr_handle=attr_handle,
                         attr_sep=attr_sep,
                         attr_parser=attr_parser,
                         drop=drop)

    def translate(self, gid: Optional[str] = None):
        f_val = super().translate(gid)
        if f_val is None:
            return None
        try:
            return f_val.split(':')[1]
        except IndexError:
            return None


class GtfRecord(GxfRecord):

    def __init__(self,
                 gtf_file,
                 columns: Iterable[str] = ('seqid', 'source', 'type', 'start',
                                           'end', 'score', 'strand', 'phase',
                                           'attributes'),
                 **kwargs):
        # defaults for gtf file
        id_handle = kwargs.get('id_handle') or 'transcript_id'
        attr_handle = kwargs.get('attr_handle') or 'attributes'
        attr_sep = kwargs.get('attr_sep') or '; '
        attr_parser = kwargs.get('attr_parser') or ' '
        drop = kwargs.get('drop') or ('"', "'")
        super().__init__(gtf_file,
                         columns,
                         id_handle=id_handle,
                         attr_handle=attr_handle,
                         attr_sep=attr_sep,
                         attr_parser=attr_parser,
                         translator='Reference',
                         drop=drop)

    def translate(self, gid: Optional[str] = None) -> Optional[str]:
        f_val = super().translate(gid)
        if f_val is None:
            return None
        try:
            goid = f_val.split('(')
            return None if 'unknown' in goid else goid[0]
        except IndexError:
            return None


class GafRecord(GxfRecord):

    def __init__(
            self,
            gaf_file,
            columns: Iterable[str] = ('DB', 'DB_Object_ID', 'DB_Object_Symbol',
                                      'Qualifier', 'GO_ID', 'DB:Reference',
                                      'Evidence Code', 'With_From', 'Aspect',
                                      'DB_Object_Name', 'DB_Object_Synonym',
                                      'DB_Object_Type', 'Taxon', 'Date',
                                      'Assigned_By', 'Annotation_Extension',
                                      'Gene_Product_From_ID'),
            **kwargs):
        # defaults for gaf file
        id_handle = kwargs.get('id_handle') or 'DB_Object_Symbol'
        attr_handle = kwargs.get('attr_handle') or 'DB_Object_Name'
        attr_sep = kwargs.get('attr_sep')
        attr_parser = kwargs.get('attr_parser')
        drop = kwargs.get('drop') or ('"', "'", 'putative', ' ')
        self._go_info: Optional[pd.DataFrame] = None
        super().__init__(gaf_file,
                         columns,
                         id_handle=id_handle,
                         attr_handle=attr_handle,
                         attr_sep=attr_sep,
                         attr_parser=attr_parser,
                         drop=drop)

    @property
    def go_info(self) -> pd.DataFrame:
        if self._go_info is None:
            go_qual: Dict[str, Dict[str, str]] = {}
            for _, record in self.records.T.items():
                g_id = record['DB_Object_ID'].replace(':mRNA',
                                                      '').replace('.mRNA', '')
                if g_id not in go_qual:
                    go_qual[g_id] = {}
                go_qual[g_id][record['Qualifier']] = record['GO_ID']
                go_qual[g_id]['name'] = record['DB_Object_Name']
                go_qual[g_id]['aspect'] = record['Aspect']
            self._go_info = pd.DataFrame(go_qual).T
        return self._go_info


def parse_modules(base_dir: Path) -> Dict[str, str]:
    """
    Create a Dict of gene_id: module

    Args:
        base_dir: directory containing *-nodes-*.tsv records

    Returns:
        lable for each module
    """
    lables: Dict[str, str] = {}
    for nodefile in base_dir.glob('*-nodes-*.tsv'):
        with open(nodefile) as nf_handle:
            csv_lines = reader(nf_handle.readlines(), delimiter='\t')
            next(csv_lines)
            for line in csv_lines:
                if line[0]:
                    lables[line[0]] = _fp_to_farb(nodefile)
    return lables


def obo_equiv(obo_filename: Path) -> Dict[str, str]:
    """
    Parse obo file (GO) to fish out id: name equivalence

    Args:
        obo_filename: path to obo file

    Returns:
        equivalence dict
    """
    with open(obo_filename) as db:
        dblines = [
            line.split(': ', 1)[-1].rstrip('\n') for line in db.readlines()
            if line[:6] in ['id: GO', 'name: ']
        ]
    return dict(
        ((gid, dblines[i + 1]) for i, gid in enumerate(dblines) if not i % 2))
