#!/usr/bin/env python3
# -*- coding:utf-8; mode: python; -*-

from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
from pathlib import Path
from typing import Any, Dict

import matplotlib.pyplot as plt
import pandas as pd
from sklearn.linear_model import LinearRegression


def cli() -> Dict[str, Any]:
    """Parse command line arguments"""
    parser = ArgumentParser(description="Trait-associations",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('--eigengenes',
                        help='eigengene file',
                        type=Path,
                        default='eigengenes.csv')
    parser.add_argument('--meta',
                        help='csv file of metadata',
                        type=Path,
                        default='meta.csv')
    # parser.add_argument('--col',
    #                     type=str,
    #                     help='column name to use as experiment aggregator',
    #                     default='trait')
    parser.add_argument('--out-dir',
                        type=Path,
                        help='base directory for outputs',
                        default='associations')
    return vars(parser.parse_args())


def load_data(meta: Path, eigengenes: Path):
    """Load dataframes"""
    metadata = pd.DataFrame(pd.read_csv(meta, index_col=0))
    eigenvals = pd.DataFrame(pd.read_csv(eigengenes, index_col=0))
    eigenvals.rename(columns={col: col[2:]
                              for col in eigenvals.columns},
                     inplace=True)
    return eigenvals, metadata.loc[eigenvals.index.intersection(
        metadata.index)]


def analyse_col(meta_series: pd.Series, eigenvals: pd.DataFrame):
    trait_mod = pd.DataFrame(
        {
            trait: (meta_series == trait).values
            for trait in meta_series.unique()
        },
        index=meta_series.index,
        dtype=int)
    correlations = pd.DataFrame(index=eigenvals.columns,
                                columns=trait_mod.columns,
                                dtype=float)
    scores = pd.Series(index=eigenvals.columns, dtype=float)
    reg = LinearRegression()
    for mod, mod_eig in eigenvals.items():
        reg.fit(trait_mod, mod_eig)
        scores[mod] = reg.score(trait_mod, mod_eig)
        correlations.loc[mod] = reg.coef_
    correlations = (correlations.T * scores).T
    correlations = correlations.loc[scores.sort_values(ascending=False).index]
    # correlations = correlations[sorted(
    #     correlations.columns,
    #     key=lambda x: correlations.abs().max(axis=0)[x],
    #     reverse=True)]
    return correlations


def plot_heat(heat_df: pd.DataFrame, outbase: Path):  # type: ignore
    fig, ax = plt.subplots()
    hm = ax.imshow(heat_df, cmap="RdBu")
    ax.set_xticks(range(heat_df.shape[1]), heat_df.columns)
    ax.set_yticks(range(heat_df.shape[0]), heat_df.index)
    ax.tick_params(axis='x', labelsize=6)
    ax.tick_params(axis='y', labelsize=8)
    ax.set_title(heat_df.columns.name)
    plt.colorbar(hm)
    fig.autofmt_xdate()
    fig.set_figwidth(4)
    fig.set_figheight(6)
    plt.tight_layout()
    plt.savefig(outbase.with_suffix('.svg'), dpi=700)


def main():
    """main routine"""
    cliargs = cli()
    eigenvals, metadata = load_data(cliargs['meta'],
                                    eigengenes=cliargs['eigengenes'])
    outbase = cliargs['out_dir']
    outbase.mkdir(exist_ok=True, parents=True)
    for col_name, col_ser in metadata.items():
        correlations = analyse_col(col_ser, eigenvals)
        correlations.columns.name = col_name
        plot_heat(correlations, outbase / col_name)
        correlations.to_csv((outbase / col_name).with_suffix('.csv'))


if __name__ == '__main__':
    main()
