#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-

import sys
from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
from pathlib import Path
from typing import Any, Dict, Optional, Tuple

import numpy as np
import pandas as pd
import yaml
from psprint import DEFAULT_PRINT
from scipy import stats


def _human_print(sim_info, outfile, max_name_len):
    """Print in human readable format"""
    DEFAULT_PRINT.pref_max = max_name_len
    DEFAULT_PRINT.print_kwargs['file'] = outfile
    DEFAULT_PRINT.switches['short'] = False
    print = DEFAULT_PRINT.psprint
    for mod, sim_mod in sim_info.items():
        print(f'{sim_mod}', mark='act', pref=mod, text_color='lg')
    outfile.close()


def _w_stream(filename: Optional[str] = None):
    if filename is None:
        return sys.stdout
    return open(Path(filename), 'w')


def _trim_sim(sims: pd.DataFrame,
              threshold: float = 0.9) -> Dict[str, Tuple[str, ...]]:
    sim_bool: pd.DataFrame = (sims < (1 - threshold))

    sim_info = {}
    for module, truth in sim_bool.iterrows():
        sim_mods_list = []
        for (col, val) in truth.iteritems():
            if val:
                sim_mods_list.append(col)
        sim_info[module] = sim_mods_list

    for keys in sim_info:
        sim_info[keys].remove(keys)
    sim_info = {key: tuple(val) for key, val in sim_info.items() if val}
    return sim_info


def cli() -> Dict[str, Any]:
    """Read command line"""
    parser = ArgumentParser(
        formatter_class=ArgumentDefaultsHelpFormatter,
        description=
        '''Look for direct and reciprocal overlap between eigengene expression of modules.'''
    )
    parser.add_argument(
        "--eigen-expr",
        dest='expr',
        type=Path,
        help="Eigengene csv file, Cols: Modules Rows: Conditions.")
    parser.add_argument(
        "--threshold",
        type=float,
        default=0.9,
        help="Threshold beyond which, overlap is considered cognizable.")
    parser.add_argument('--no-reciprocal',
                        action='store_false',
                        dest='signed',
                        help='Do not consider reciprocal similarities.')
    parser.add_argument('--outfile',
                        type=_w_stream,
                        help='Path to output file',
                        default=sys.stdout)
    parser.add_argument('--yaml', type=Path, help='Path to yaml dump file')

    return vars(parser.parse_args())


def similarity(expr: pd.DataFrame, reciprocal: bool = False) -> pd.DataFrame:
    """
    Calculate module expression similarities

    Args:
        expr: expression (index: conditions, cols: modules)
        reciprocal: calculate reciprocal similarities (add instead of subtract)

    Returns:
        Triangular symmetrical matrix of similarities between expressions.
    """
    expr_to = np.array(expr.to_numpy(), dtype=np.float_)[:, :, None]
    expr_from = np.array(expr.to_numpy(),
                         dtype=np.float_)[:, None, :] * (1 - 2 * reciprocal)
    expr_diff = expr_to - expr_from
    mad_diff = stats.median_abs_deviation(expr_diff, axis=0)
    return pd.DataFrame(mad_diff, columns=expr.columns, index=expr.columns)


def main():
    """Main routine"""
    cliargs = cli()
    eigen_expr = pd.DataFrame(pd.read_csv(cliargs['expr'], index_col=0))
    eigen_expr.rename(
        columns={mod: mod.replace('AE', '')
                 for mod in eigen_expr.columns},
        inplace=True)
    _max_name_len = max([len(name) for name in eigen_expr.columns])

    sims = similarity(eigen_expr)
    if cliargs['signed']:
        sims: pd.DataFrame = pd.DataFrame(
            pd.concat([sims,
                       similarity(eigen_expr,
                                  reciprocal=True)]).groupby(level=0).min())
    sim_info = _trim_sim(sims, cliargs['threshold'])

    _human_print(sim_info, cliargs['outfile'], _max_name_len)
    if cliargs['yaml'] is not None:
        sim_info['args'] = ('threshold', cliargs['threshold'], 'signed',
                            cliargs['signed'])
        with open(cliargs['yaml'], 'w') as dumpfile:
            yaml.safe_dump(sim_info, dumpfile)


if __name__ == '__main__':
    main()
