#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-

from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
from pathlib import Path
from typing import List, Tuple

import numpy as np
import pandas as pd
from Bio.SeqIO import parse as fasta_parser
from matplotlib import pyplot as plt
from psprint import print

from wgcna_extract.binom_enrich import (QualCounter, _color_axes,
                                        binom_test_array)
from wgcna_extract.motif_occurrence import find_occurrence, parse_motifs


def cli():
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter,
                            description='''
    Description:

    Binomial p-values for:
    k: Occurrence of motif in RNA-sequences across all RNA
    ''')
    parser.add_argument('-m',
                        '--motif',
                        type=Path,
                        default=Path('./pwms'),
                        help='motif sequences base directory (format: PWM)')
    parser.add_argument('-o',
                        '--out-dir',
                        type=Path,
                        default=Path('enrichments'),
                        nargs='?',
                        help='base directory for output files.')
    parser.add_argument('-t',
                        '--threshold',
                        type=float,
                        default=0.2,
                        help='PWM cut-off for motif sequence')
    parser.add_argument('-f',
                        '--feat-seq',
                        type=Path,
                        default=Path('./features.fa'),
                        help='file containing target fasta sequences')

    return vars(parser.parse_args())


def _feat_stats(feat_seqs: List[int]) -> Tuple[int, int, int]:
    """
    minimum, mean, maximum feature length
    """
    return np.min(feat_seqs), int(np.round(
        np.mean(feat_seqs))), np.max(feat_seqs)


def main() -> int:
    """Main routine"""

    cliargs = cli()

    out_dir = cliargs['out_dir']
    out_dir.mkdir(parents=True, exist_ok=True)
    (out_dir / 'sheets').mkdir(parents=True, exist_ok=True)
    (out_dir / 'images').mkdir(parents=True, exist_ok=True)

    feat_seq = {
        str(record.id): str(record.seq)
        for record in fasta_parser(cliargs['feat_seq'], 'fasta')
    }
    _, feat_mean, _ = _feat_stats([len(seq) for seq in feat_seq.values()])
    print('Features found: ', mark='bug')
    print(f'{list(feat_seq.keys())[0]}, ...')
    print('Parsing motifs...', mark='info')
    motifs = parse_motifs(cliargs['motif'], threshold=cliargs['threshold'])
    mot_min, _, mot_max = _feat_stats(
        [motif.length for motif in motifs.values() if motif.length])

    non_prob_ref = np.power(
        1 - 1 / np.power(4, np.arange(mot_min, mot_max + 1)), feat_mean)
    prob = pd.Series({
        m_id:
        1 - np.power(non_prob_ref[motif.length - mot_min], motif.degen)
        for m_id, motif in motifs.items() if motif
    })

    prob.name = 'frac'

    occurrence_path = out_dir / 'sheets' / f'occurrences_t{cliargs["threshold"]}.tsv'

    print('Finding occurrences in features...', mark='info')
    motif_occur = find_occurrence(feat_seq, motifs)
    motif_occur.to_csv(occurrence_path, sep='\t')
    motif_occur.name = 'Motifs'
    global_counter = QualCounter(pd.DataFrame(motif_occur))
    occur = global_counter.term_feat_freq['Motifs']['count']
    occur = occur.reindex(prob.index)
    occur.fillna(0, inplace=True)
    expect = prob * global_counter.num_feat
    p_val = binom_test_array(occur, len(feat_seq), prob)
    nlogp_vals = -p_val.apply(np.log10)
    nlogpmax = nlogp_vals.replace(np.inf, np.nan).dropna().max()
    nlogp_vals.replace(np.inf, nlogpmax, inplace=True)
    nlogp_vals = nlogp_vals * (((expect > occur) * -2) + 1)
    nlogp_vals.sort_values(inplace=True, ascending=False)
    nlogp_vals.name = '-log(p-value) [directed]'
    bitinfo = -prob.apply(np.log2)
    bitinfo.name = 'bits'
    out_data = pd.concat((nlogp_vals, bitinfo), axis=1)
    out_data.to_csv(out_dir / 'Relevant_motifs.csv')
    fig, ax = plt.subplots(1)
    _color_axes(ax, fig, 'dark')
    ax.bar(range(len(nlogp_vals)), nlogp_vals)
    ax.set_title('Motif enrichment/depletion in transcripts over random.')
    ax.set_xticks(range(len(nlogp_vals)),
                  nlogp_vals.index)  # type: ignore [operator]
    ax.set_xlabel('motif')
    ax.set_ylabel('-log(p-value) [directed]')
    ax.tick_params(axis='x', which='major', labelsize=2)
    fig.autofmt_xdate()
    plt.savefig(out_dir / 'Relevant_motifs.svg')
    return 0


if __name__ == '__main__':
    main()
