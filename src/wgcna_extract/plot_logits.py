#!/usr/bin/env python3
# -*- coding: utf-8; mode:python; -*-
#
# Copyright © 2022-2023 Pradyumna Paranjape
# This file is part of WGCNA_Extract.
#
# WGCNA_Extract is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# WGCNA_Extract is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FIT FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with WGCNA_Extract.  If not, see <https://www.gnu.org/licenses/>.
#
"""Plot log-odds (logit) in bits from DataFrame"""

import matplotlib
from matplotlib import pyplot as plt
from matplotlib.figure import Figure

matplotlib.rc('xtick', labelsize=10)
matplotlib.rc('ytick', labelsize=10)


def plot_logit():
    # Plot
    _subpl = plt.subplots(layout='constrained',
                          figsize=(3.54, 3.54 * 9 / 16),
                          dpi=300)
    fig: Figure = _subpl[0]
    ax = _subpl[1]
    x_ticks = []

    x = 0
    width = 0.3

    for feat, row in logit.iterrows():
        # next row
        x += 1
        row_dict = row.dropna().to_dict()
        offset = x - width * len(row_dict) / 2
        for module, val in row_dict.items():
            offset += width
            ax.bar(offset,
                   val,
                   width,
                   label=module,
                   color=str(module).replace(
                       'grey60', 'grey'
                   ).replace(
                       'T1640', 'red'
                   ).replace(
                       'T1630', 'orange'
                   ).replace(
                       'T1620', 'yellow'
                   ).replace(
                       'T1470', 'blue'
                   ),
                   edgecolor='black')
        x_ticks.append(x + width / 2)
    if x < 30:
        ax.set_xticks(x_ticks, logit.index.to_list())  # type: ignore
    ax.axhline(0, color='black')  # type: ignore
    # if len(xlabels) < 30:
    #     ax.set_xticks(range(len(xlabels)), xlabels)  # type: ignore [operator]
    ax.set_ylabel('Log-Odds (bits)')
    ax.set_xlabel(f'{feature} {units}')
    ax.set_title(f'Genetic Feature: {feature}')
    # Place a common legend for panel if possible.
    # Legend eats up all the space, masking the plots
    # legend_unique(ax, ncols=3)
    if x > 15:
        fig.autofmt_xdate()
    plt.savefig(out_dir / f'images/{feature}_log_odds.png')
