#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-

from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
from pathlib import Path
from typing import Any, Dict

from Bio import SeqIO
from Bio.Seq import Seq


def cli() -> Dict[str, Any]:
    """Parse CLI"""
    parser = ArgumentParser(
        formatter_class=ArgumentDefaultsHelpFormatter,
        description='Generate UTR sequences by removing CDS from transcritps.')
    parser.add_argument(
        'transcripts',
        default='transcripts.fa',
        type=Path,
        help='File path to fasta file, transcripts identified by gene_id.')
    parser.add_argument(
        'cds',
        default='cds.fa',
        type=Path,
        help='File path to fasta file, cds identified by gene_id.')
    parser.add_argument(
        'utrs',
        default='utr.fa',
        type=Path,
        help='File path to fasta file, utrs identified by gene_id.')
    args = parser.parse_args()
    return vars(args)


def load_inputs(transcripts: Path, cds: Path):
    """
    Load sequences

    Args:
        transcripts: transcripts fasta file
        cds: cds fasta file

    Returns:
        Dicts
    """
    return SeqIO.to_dict(SeqIO.parse(transcripts, 'fasta')), SeqIO.to_dict(
        SeqIO.parse(cds, 'fasta'))


def edit_seqs(transcripts, cds):
    """
    Edit transcript records to generate UTR sequences.

    55555CCCCDDDDSSSSUUUUUUUUUUUU

    5' region, CCCCDDDDSSSS are trimmed and only UUUUUUUUUU is retained.

    Args:
        transcripts: transcript fasta sequences.

    Returns:
        UTR sequence records
    """
    utrs = []
    for t_id, trans in transcripts.items():
        if t_id not in cds:
            utrs.append(Seq(id=t_id, seq=trans))
            continue
        t_seq = str(trans)
        look_up_len = min(len(cds), 20)
        u_idx = t_seq.index(cds[-look_up_len:]) + look_up_len
        utrs.append(Seq(id=t_id, seq=t_seq[u_idx:]))
    return utrs


def main():
    cliargs = cli()
    transcripts, cds = load_inputs(cliargs['transcripts'], cliargs['cds'])
    utrs = edit_seqs(transcripts, cds)
    SeqIO.write(utrs, cliargs['utrs'], format='fasta')


if __name__ == '__main__':
    main()
