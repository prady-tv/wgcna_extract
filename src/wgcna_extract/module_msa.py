#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-

from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
from os import sched_getaffinity
from pathlib import Path
from subprocess import PIPE, Popen
from typing import Any, Dict, Iterable, Tuple

import pandas as pd
from Bio.SeqIO import parse
from psprint import print

MAX_THREADS = max((len(sched_getaffinity(0)) - 1), 1)
"""Maximum usable threads = (# available - 1) or 1"""


def _fp_to_farb(fp: Path) -> str:
    return str(fp).split('-')[-1].split('.')[0]


def _proc_clustal(sequences: str, out_file: Path):
    """
    Call a clustalo subprocess

    Args:
        sequences: fasta- formatted sequences to be aligned
    """
    alignment_proc = Popen([
        'clustalo', '--in', '-', '--outfmt', 'clustal', '--threads',
        str(MAX_THREADS), '--out',
        str(out_file)
    ],
                           stdin=PIPE,
                           stdout=PIPE,
                           stderr=PIPE,
                           text=True)
    _, stderr = alignment_proc.communicate(sequences)
    if ((alignment_proc.returncode) or stderr):
        print('Error calling Alignment', mark='err')
        print(stderr)
        return


def cli() -> Dict[str, Any]:
    """Parse CLI"""
    parser = ArgumentParser(
        formatter_class=ArgumentDefaultsHelpFormatter,
        description='ClustalOmega of sequences corresponding to WGCNA Modules.'
    )
    parser.add_argument(
        'nuc_seq',
        default='sequences.fa',
        type=Path,
        help='File path to fasta sequences, identified by gene_id.')
    parser.add_argument(
        '--base-dir',
        default='.',
        type=Path,
        help='Path to directory containing Cytoscape files: *-nodes-*.tsv')
    parser.add_argument(
        '--outputs',
        default=None,
        type=Path,
        help=
        'Alignments outputs parent. If None, BASE_DIR/alignments/MODULE.aln')
    args = parser.parse_args()
    if args.outputs is None:
        args.outputs = args.base_dir / 'alignments'
    return vars(args)


def load_sequences(seqfa: Path,
                   modules: Dict[str, Iterable[str]]) -> Dict[str, list]:
    """
    Load sequences reading from fasta file

    Args:
        seqfa: all sequence records in fasta format (handles: gene_ids)
        modules: output from `:class:module_gene_ids`

    Returns:
        Sequence handles
    """
    mod_seq: Dict[str, list] = {}
    for mod, gene_ids in modules.items():
        mod_seq[mod] = [
            record for record in parse(seqfa, 'fasta') if record.id in gene_ids
        ]
    return {
        k: mod_seq[k]
        for k in sorted(mod_seq, key=lambda x: len(mod_seq[x]))
    }


def module_gene_ids(base_dir: Path) -> Dict[str, Tuple[str, ...]]:
    """
    Create a Module: gene_ids dict

    Args:
        base_dir: directory containing *-nodes-*.tsv records

    Returns:
        Dict with list of corresponding gene_ids for each module
    """
    module_dict: Dict[str, Tuple[str]] = {}
    for nodefile in base_dir.glob('*-nodes-*.tsv'):
        nodes_records = pd.DataFrame(
            pd.read_csv(nodefile, delimiter='\t', index_col=0))
        module_dict[_fp_to_farb(nodefile)] = tuple(
            nodes_records.index.tolist())

    return module_dict


def call_clust(sequences: Dict[str, Iterable[Any]], out_base: Path):
    """
    Call (bash) program `clustalo` to align module sequences

    Args:
        sequences: module sequences (generator) to be aligned
    """
    for module, members in sequences.items():
        mod_fa = '\n'.join(
            ('>' + gene.id + '\n' + str(gene.seq) for gene in members))
        print(f'Calling module: {module}', mark='info')
        _proc_clustal(mod_fa, (out_base / module).with_suffix('.aln'))


def main():
    cliargs = cli()
    modules = module_gene_ids(cliargs['base_dir'])
    sequences = load_sequences(cliargs['nuc_seq'], modules)
    cliargs['outputs'].mkdir(parents=True, exist_ok=True)
    call_clust(sequences, cliargs['outputs'])


if __name__ == '__main__':
    main()
