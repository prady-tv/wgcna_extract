*************************
wgcna_extract
*************************

Gist
==========

Source Code Repository
---------------------------

|source| `Repository <https://gitlab.com/pradyparanjpe/wgcna_extract.git>`__

|pages| `Documentation <https://pradyparanjpe.gitlab.io/wgcna_extract>`__

Badges
---------

|Pipeline|  |Coverage|  |PyPi Version|  |PyPi Format|  |PyPi Pyversion|


Description
==============

Create a python project from template.

What does it do
--------------------

Some great thing

.. |Pipeline| image:: https://gitlab.com/pradyparanjpe/wgcna_extract/badges/master/pipeline.svg

.. |source| image:: https://about.gitlab.com/images/press/logo/svg/gitlab-icon-rgb.svg
   :width: 50
   :target: https://gitlab.com/pradyparanjpe/wgcna_extract.git

.. |pages| image:: https://about.gitlab.com/images/press/logo/svg/gitlab-logo-gray-stacked-rgb.svg
   :width: 50
   :target: https://pradyparanjpe.gitlab.io/wgcna_extract

.. |PyPi Version| image:: https://img.shields.io/pypi/v/wgcna_extract
   :target: https://pypi.org/project/wgcna_extract/
   :alt: PyPI - version

.. |PyPi Format| image:: https://img.shields.io/pypi/format/wgcna_extract
   :target: https://pypi.org/project/wgcna_extract/
   :alt: PyPI - format

.. |PyPi Pyversion| image:: https://img.shields.io/pypi/pyversions/wgcna_extract
   :target: https://pypi.org/project/wgcna_extract/
   :alt: PyPi - pyversion

.. |Coverage| image:: https://gitlab.com/pradyparanjpe/wgcna_extract/badges/master/coverage.svg?skip_ignored=true
