#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-
"""Find a real gene that is closest (Euclidean) to eigengenes"""

from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
from pathlib import Path
from typing import Any, Dict, Tuple

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt


def _fp_to_farb(fp: Path) -> str:
    return str(fp).split('-')[-1].split('.')[0]


def cli() -> Dict[str, Any]:
    """Command line argument parser"""
    parser = ArgumentParser(
        formatter_class=ArgumentDefaultsHelpFormatter,
        description='''Locate Real Genes best resembling eigengenes''')
    parser.add_argument('--metadata',
                        type=Path,
                        default='meta.csv',
                        help='metadata about phases')
    parser.add_argument(
        '--eigengenes',
        type=Path,
        default='eigengenes.csv',
        help='Eigengenes csv, cols: module names, rows: phases')
    parser.add_argument(
        '--base-dir',
        dest='basedir',
        type=Path,
        default='./',
        help='base directory containing cytoscape-exports of *-nodes*- files')
    parser.add_argument('--counts',
                        type=Path,
                        default='counts.csv',
                        help='Counts csv, cols: phases names, rows: gene_ids')
    parser.add_argument('--output',
                        type=Path,
                        default='eigen_real_genes.csv',
                        help='Output, in eigengenes format, with real genes')
    args = parser.parse_args()
    return vars(args)


def load_modules(basedir: Path) -> Dict[str, pd.Series]:
    """Load Modules as pd.Series (gene-id: gene-annotation)"""
    modules = {}
    for nodefile in basedir.glob('CytoscapeInput-nodes-*.tsv'):
        modules[_fp_to_farb(nodefile)] = pd.DataFrame(
            pd.read_csv(nodefile, index_col=0, delimiter='\t'))['altName']
    return modules


def load_data(
    counts: Path, eigengenes: Path, meta: Path, basedir: Path
) -> Tuple[Dict[str, pd.DataFrame], pd.DataFrame, pd.DataFrame]:
    """
    Load eigengenes and counts

    Args:
        counts: csv file with expression counts genes x phases
        eigengenes: csv file with eigengene expressions phases x modules
        basedir: path to base directory containing *-nodes-* exports

    Returns:
        Tuple:
            norm_df: Loaded dataframe: genes x phases, normalized between 0 and 1
            eigengenes_df: Loaded dataframe: phases x modules
            metadata: Loaded dataframe (metadata)
    """
    eigengenes_df = pd.DataFrame(pd.read_csv(eigengenes, index_col=0))
    eigengenes_df.rename(
        columns={farb: farb[2:]
                 for farb in eigengenes_df.columns},
        inplace=True)
    rel_expr = eigengenes_df.divide(eigengenes_df.mean(axis=1),
                                    axis=0).apply(np.log2)
    eigengenes_df = rel_expr.divide(np.max(np.abs(rel_expr), axis=1), axis=0)
    counts_df = pd.DataFrame(pd.read_csv(counts, index_col=0))
    modules = load_modules(basedir)
    metadata = pd.DataFrame(pd.read_csv(meta, index_col=0))
    norm_genes = {}
    for mod_farb, mod in modules.items():
        print(mod_farb)
        if mod_farb == 'grey':
            mod_farb = 'grey60'
        abs_expr: pd.DataFrame = counts_df.loc[mod.index]
        rel_expr = abs_expr.divide(abs_expr.mean(axis=1),
                                   axis=0).apply(np.log2)
        norm_expr = rel_expr.divide(np.max(np.abs(rel_expr), axis=1), axis=0)
        norm_genes[mod_farb] = pd.concat((mod, norm_expr), axis='columns')
    return norm_genes, eigengenes_df, metadata


def plot_eigen(basedir: Path, eigen: pd.DataFrame, real_eigen: pd.Series,
               norm_genes: Dict[str, pd.DataFrame], meta: pd.DataFrame):
    for farb, norm in norm_genes.items():
        print(f'plotting {farb}')
        norm_vals = norm.iloc[:, 1:]
        norm_name = norm.iloc[:, 0]
        fig, ax = plt.subplots(1, 1)
        ax.plot(norm_vals.loc[real_eigen[farb]],
                color='k',
                label=norm_name.loc[real_eigen[farb]])
        ax.plot(eigen[farb], ls='--', color=farb.replace('grey60', 'silver'))
        ax.grid(True, axis='x')
        xlabs = [meta.loc[srr, 'group'] for srr in eigen.index]
        ax.xaxis.set_ticks(range(len(xlabs)), xlabs)
        ax.set_xticklabels(xlabs, rotation=45, fontsize=5)
        plt.legend()
        plt.savefig((basedir / farb).with_suffix('.png'), dpi=700)


def main():
    """Main routine"""
    cliargs = cli()
    norm_genes, eigengenes, meta = load_data(cliargs['counts'],
                                             cliargs['eigengenes'],
                                             cliargs['metadata'],
                                             cliargs['basedir'])
    real_eigen_d = {}
    for farb, norm_df in norm_genes.items():
        euclid: pd.Series = np.power(
            np.sum(np.power(norm_df.iloc[:, 1:] - eigengenes[farb], 2),
                   axis=1), 0.5)
        real_eigen_d[farb] = euclid.idxmax()
    real_eigen = pd.Series(real_eigen_d)
    plot_eigen(cliargs['basedir'], eigengenes, real_eigen, norm_genes, meta)


if __name__ == '__main__':
    main()
