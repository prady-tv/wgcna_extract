#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-

import sys
from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
from json import decoder
from os import PathLike
from pathlib import Path
from typing import Any, Dict

import pandas as pd

from wgcna_extract.chromosomal import analyse
from wgcna_extract.enriched_go import go_gtf_equiv
from wgcna_extract.parsers import GffRecord, GtfRecord, parse_modules


def cli() -> Dict[str, Any]:
    """Parse command line"""
    parser = ArgumentParser(
        formatter_class=ArgumentDefaultsHelpFormatter,
        description='''Count enriched GO terms from modules.''')
    parser.add_argument('--gtf', type=Path, help='gtf file')
    parser.add_argument('--gff', type=Path, help='gff file')
    parser.add_argument('base-dir',
                        default='.',
                        type=Path,
                        help='Analyse files in this directory.')
    parser.add_argument('--json',
                        type=Path,
                        help='json file',
                        default=Path('brite.json'))
    parser.add_argument('--out-dir',
                        default='enrichments',
                        type=Path,
                        help='Directory created inside BASE-DIR')
    parser.add_argument('--p-value',
                        type=float,
                        help='Enrichment p-value cut off (p < P_VALUE)',
                        default=1e-3)
    return vars(parser.parse_args())


def parse_json(brite_json: PathLike, go_equiv: Dict[str, str]):
    """
    Args:
        brite_json: json of BRITE records
    """
    jd = decoder.JSONDecoder()
    js_content = Path(brite_json).read_text()
    brite = jd.decode(js_content)
    pw_data = {}
    for keg_l1 in brite.get('children', {}):
        l1 = keg_l1['name'].split('_', 1)[-1].split('[')[0]
        for keg_l2 in keg_l1.get('children', {}):
            l2 = keg_l2['name'].split(' ', 1)[-1].split('[')[0]
            for keg_l3 in keg_l2.get('children', {}):
                l3 = keg_l3['name'].split(' ', 1)[-1].split('[')[0]
                for gene_prod in keg_l3.get('children', {}):
                    kid = gene_prod['name'].split(' ')[0]
                    gtf_handle = kid.replace('_', '.').replace('LMJF', 'LmjF')
                    if gtf_handle in go_equiv:
                        pw_data[go_equiv[gtf_handle]] = l1, l2, l3
    pw_df = pd.DataFrame(
        pw_data,
        index=['kegg_path_level1', 'kegg_path_level2', 'kegg_path_level3'])
    return pw_df.T


def main() -> int:
    """Main routine"""
    cliargs = cli()
    out_dir = cliargs['base-dir'] / cliargs['out_dir']
    out_dir.mkdir(parents=True, exist_ok=True)
    (out_dir / 'sheets').mkdir(parents=True, exist_ok=True)
    (out_dir / 'images').mkdir(parents=True, exist_ok=True)
    gtf_thes = GtfRecord(cliargs['gtf'])
    gff_thes = GffRecord(cliargs['gff'])
    go_equiv = go_gtf_equiv(out_dir, gff_thes, gtf_thes)
    all_quals = parse_json(cliargs['json'], go_equiv)
    module_labels = parse_modules(cliargs['base-dir'])
    alpha = cliargs.get('p_value', 0.001)
    _dependency = {}
    for feature in all_quals.columns:
        _dependency[feature] = analyse(all_quals,
                                       module_labels,
                                       feature,
                                       alpha,
                                       out_dir,
                                       force_resolution=True,
                                       groupby='module',
                                       xlabels='feature',
                                       resolution=0.125,
                                       module_plots=True)

    dependency = pd.DataFrame(_dependency).T
    dependency.to_csv(out_dir / 'sheets/KEGG-PWAY-dependence.csv')
    return 0


if __name__ == '__main__':
    sys.exit(main())
