#!/usr/bin/env python3
#-*- coding: utf-8; mode: python -*-

import sys
from pathlib import Path
from typing import Optional

import pandas as pd


def fish_linkers(parent: Path, mod_color, target_gene_id, target_gene_name):
    edge_file = parent / f"CytoscapeInput-edges-{mod_color}.tsv"
    edges = pd.DataFrame(
        pd.read_csv(edge_file, index_col=None, header=0, sep='\t'))
    from_links = edges[(edges["fromNode"] == target_gene_id) |
                       (edges["fromAltName"] == target_gene_name)]
    to_links = edges[(edges["toNode"] == target_gene_id) |
                     (edges["toAltName"] == target_gene_name)]
    from_partners = from_links[["toNode", "toAltName"]]
    from_partners.columns = 'Node', 'AltName'
    to_partners = to_links[["fromNode", "fromAltName"]]
    to_partners.columns = 'Node', 'AltName'
    partners = pd.concat((from_partners, to_partners))
    partners.to_csv(parent / f"links_{target_gene_id}.csv")


def spot_color(parent: Path, *args) -> Optional[str]:
    for nodefile in Path(parent).glob("*nodes*.tsv"):
        if any((g in nodefile.read_text() for g in args)):
            return nodefile.stem.split("-")[-1]
    sys.exit(1)


def main(*args):
    if len(args) < 2:
        print('''
        usage: fish_linkers.py PARENT_DIR GENE_ID, GENE_NAME
        ''')
        sys.exit(1)
    args = args[1:]
    parent = Path(args[0])
    mod_color = spot_color(parent, *args[1:])
    fish_linkers(parent, mod_color, *args[1:])


if __name__ == "__main__":
    main(*sys.argv)
