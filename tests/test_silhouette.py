#!/usr/bin/env python
# -*- coding: utf-8; mode: python; -*-

#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-

# Copyright (c) Pradyumna Swanand Paranjape <pradyparanjpe@rediffmail.com>
#
# This file is part of wgcna_extract.
#
# wgcna_extract is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# wgcna_extract is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with wgcna_extract. If not, see <https://www.gnu.org/licenses/>.
#
"""
Test silhouette functions
"""

import unittest


class TestExtractLabel(unittest.TestCase):
    "Test label extraction"

    def test_extract_label_default(self):
        """Extract labels from current directory"""


def main():
    """Main subroutine"""


if __name__ == '__main__':
    main()
