#!/usr/bin/env python3
# -*- coding: utf-8; mode:python; -*-
#
# Copyright © 2022-2023 Pradyumna Paranjape
# This file is part of WGCNA_Extract.
#
# WGCNA_Extract is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# WGCNA_Extract is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FIT FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with WGCNA_Extract.  If not, see <https://www.gnu.org/licenses/>.
#
"""
Re-annotate GTF file to include:

- Inferred PolyCistrons as `EXON`
- Original `EXON` as `GENE`
"""

import numpy as np
import pandas as pd

from wgcna_extract.parsers import GxfRecord


def extract_genetic_info(gxf: GxfRecord) -> pd.DataFrame:
    """
    Extract following information from records:
        - Chromosome
        - Poly-Cistron
        - i-ord: transcript-ord on Poly-Cistron from
            the side of transcription initiation
        - j-ord: transcript-ord on Poly-Cistron from
            the side of `J` base nucleotide
        - i-rel-loc: relative location from initialization of transcription

    Args:
        gxf: parsed gxf record
    """
    attr = gxf.attributes
    gxfinfo = pd.DataFrame(index=attr.index,
                           columns=pd.Index(('chromosome', 'poly-cistron',
                                             'i-ord', 'j-ord', 'i-start')))

    # initialize
    cis_num: int = 0
    current_ori: str = '.'
    current_chr: str = ''
    cis_start: int = 0
    cis_end: int = -1

    # determine poly-cistron, i-ord (signed)
    # Following is a `sequential` mapping function
    for _idx, orient in attr['strand'].items():
        idx = str(_idx)  # affirm string
        if attr.loc[idx, 'seqid'] != current_chr:
            # Chromosome changed
            cis_num = 0
            cis_start = int(attr.loc[idx, 'start'])  # type: ignore [arg-type]
            current_ori = orient
            current_chr = str(attr.loc[idx, 'seqid'])
            cis_end = int(attr.loc[idx, 'end'])  # type: ignore [arg-type]
        elif orient != current_ori:
            # Orientation has changed
            cis_num += 1
            cis_start = int(attr.loc[idx, 'start'])  # type: ignore [arg-type]
            current_ori = orient
            cis_end = int(attr.loc[idx, 'end'])  # type: ignore [arg-type]
        else:
            # Same cistron
            cis_end = int(attr.loc[idx, 'end'])  # type: ignore [arg-type]
        gxfinfo.loc[idx, 'poly-cistron'] = cis_num
        gxfinfo.loc[idx, 'chromosome'] = str(
            int(str(idx).split('.')[0].split('_')[-1]))

    # Elaborate poly-cistron name
    gxfinfo['poly-cistron'] += 1
    gxfinfo['poly-cistron'] = gxfinfo['poly-cistron'].apply(
        lambda x: f'.{int(x):0>2}')
    gxfinfo['poly-cistron'] = gxfinfo['chromosome'] + gxfinfo['poly-cistron']

    len_cis: pd.Series = pd.Series(index=gxfinfo.index, dtype=np.int_)
    true_init: pd.Series = pd.Series(index=gxfinfo.index, dtype=np.int_)

    for cis_id in gxfinfo['poly-cistron'].unique():
        cis_idx = gxfinfo.index[gxfinfo['poly-cistron'] == cis_id]
        len_cis[cis_idx] = len(cis_idx)
        true_init[cis_idx] = np.min(gxfinfo.loc[cis_idx, 'i-start'])

    # Filter index for transcripts only
    gxfinfo = gxfinfo.loc[gxfinfo.index.str.contains(r'\w+_\d+\.T?\d+',
                                                     regex=True)]
    return gxfinfo.map(lambda x: str(x))  # type: ignore [operator]
