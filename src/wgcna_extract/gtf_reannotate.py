#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-
#
# Copyright © 2022-2023 Pradyumna Paranjape
# This file is part of WGCNA_Extract.
#
# WGCNA_Extract is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# WGCNA_Extract is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FIT FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with WGCNA_Extract.  If not, see <https://www.gnu.org/licenses/>.
#
"""Reannotate Leishmania HU3 SL-Seq generated GTF with 'POLY-CISTRONs'."""

import csv
import sys
from pathlib import Path
from typing import Any, Dict, Iterable, List, Optional, Tuple

from Bio.SeqIO import parse as fasta_parser


def add_loci(entries: List[List[str]],
             tag_key: str = 'locus_tag',
             feat_sep: str = '; ',
             equ: str = ' ') -> List[List[str]]:
    """
    Add unique locus tag as a feature (last column).

    Parameters
    ----------
    entries : List[List[str]]
        List of entries separated
    tag_key : str
        key (name) of tag
    feat_sep : str
        delimits features
    equ : str
        separates key from value

    Returns
    -------
    List[List[str]]
        Feature entries with loci added
    """
    for idx, entry in enumerate(entries):
        chrom = entry[0].replace('.', '_')
        tag = feat_sep + tag_key + ' ' + f'{chrom}.locus_{idx+1:0>6}'
        entry[-1] = entry[-1].rstrip(' ').rstrip(feat_sep) + tag
    return entries


def insert_entries(original: List[Any],
                   additions: List[Tuple[int, Any]]) -> List[Any]:
    """
    Insert additions at indicated place.

    As insertions occur, rectify index of all following items.

    Parameters
    ----------
    original : List
        original list
    additions : List[Tuple[int, Any]]
        car : index for insertion (non-cumulative)
        cdr : value
        ('car and cdr' is Emacs jargon)

    Returns
    -------
    List
        list with entries inserted at appropriate places

    Examples
    --------
    Additions are inserted in order, at places where they would have gone
    If insertions had not changed list indices.

    >>> to_modify = [1, 2, 3, 4, 5]
    >>> additions = [(1, 'a'), (3, 'b'), (4, 'c')]
    >>> insert_entries(to_modify, additions)
    [1, 'a', 2, 3, 'b', 4, 'c', 5]
    """
    for item_no, item in enumerate(list(zip(additions))):
        idx, entry = item[0]
        original.insert(idx + item_no, entry)
    return original


def locate_polycis(
        gtf_entries: List[List[str]]) -> List[Tuple[int, List[str]]]:
    """
    Parse entries to annotate co-oriented transcripts as 'polycistron'.

    .. warning::
        - Must be performed sequentially.
        - GTF must be pre-sorted.

    Parameters
    ----------
    gtf_entries : List[List[str]]
        List of GTF entries with fields pre-separated.

    Returns
    -------
    List[Tuple[int, List[str]]]
        Additional entries : indexed (car) List of Polycistron entries (cdr)


    Entry format
    ------------

    ::

        0   1     2           3   4   5 6   7 8
        SRC tvlab polycistron BEG END 0 SGN 0 polycistron_id SRC.PCNN; gene_count CC


    Legend
    ~~~~~~
    NN : int
        Cistron number
    CC : int
        Number of genes in this polycistron
    """
    polycis: List[Tuple[int, List[str]]] = []
    src, beg, end, sgn, gene_lead, pcnum, pccount = '', -1, -1, '.', 0, 0, 0
    # this generally doesn't have a header
    for line_no, entry in enumerate(gtf_entries):
        if line_no == 0:
            src, beg, sgn, gene_lead, pccount = (entry[0], int(entry[3]),
                                                 entry[6], line_no, 0)
            continue
        if not ((entry[0] == src) and (entry[6] == sgn)):
            # 1. chromosome change
            # 2: sign change
            # 3: (disabled) end exists and beg is beyond 5000 nt from end
            #    and ((end is None) or (int(line[3]) < (int(end) + 5000)))
            chrom = src.replace(".", "_")
            polycis.append((gene_lead, [
                src, 'tvlab', 'polycistron',
                str(beg),
                str(end), '0', sgn, '0',
                f'polycistron_id "{chrom}.PC{pcnum:0>2}"; gene_count {pccount}'
            ]))
            if entry[0] == src:
                pcnum += 1
            else:
                pcnum = 1
            src, beg, sgn, gene_lead, pccount = (entry[0], int(entry[3]),
                                                 entry[6], line_no, 0)
        end = int(entry[4])
        pccount += 1
    # After last entry
    chrom = src.replace(".", "_")
    polycis.append((gene_lead, [
        src, 'tvlab', 'polycistron',
        str(beg),
        str(end), '0', sgn, '0',
        f'polycistron_id "{chrom}.PC{pcnum:0>2}"; gene_count {pccount}'
    ]))
    return polycis


def parse_feat(field: str,
               sep: str = ';',
               equ: str = ' ',
               drop: Optional[Iterable[str]] = None) -> Dict[str, str]:
    """
    Parse features.

    Trailing and leading spaces are stripped.

    Parameters
    ----------
    field : str
        field as a single string
    sep : str
        separates features
    equ : str
        separates feature key from value
    drop : Optional[Iterable[str]]
        drop strings from value, default: ``('"', "'")``

    Returns
    -------
    dict[str, str]
        feature-key : feature-value
    """
    if drop is None:
        drop = '"', "'"

    for sup in drop:
        while sup in field:
            field = field.replace(sup, '')

    return dict(
        tuple(item.rstrip(' ').strip(' ').split(equ, 1))  # type: ignore [misc]
        for item in field.split(sep) if equ in item)


def locate_intergenic(
        gtf_entries: List[List[str]],
        chromosome_sizes: Dict[str, int]) -> List[Tuple[int, List[str]]]:
    """
    Annotate upstream and downstream intergenic regions.

    Parameters
    ----------
    gtf_entries : List[List[str]]
        List of GTF entries with fields pre-separated.
    chromosome_sizes : Dict[str, int]
        size look-up for each chromosome

    Returns
    -------
    List[Tuple[int, List[str]]]
        Additional entries: Indexed List of intergenic annotations


    Entry format
    ------------

    ::

        0   1     2      3   4   5 6 7 8
        SRC tvlab STREAM BEG END 0 . 0 polycistron_lid LL; polycistron_rid RR


    Legend
    ~~~~~~
    STREAM : {'upstream', 'downstream'}
        ``5'`` or ``3'`` of both initiations
    LL : str
        Cistron ID on left (or SRC.PC00 = Left terminal)
    RR : str
        Cistron ID on right (or SRC.PC-1 = Right terminal)
    """
    additions: List[Tuple[int, List[str]]] = []
    ori, beg, src, l_pid = None, 0, None, None
    for line_no, entry in enumerate(gtf_entries):
        if entry[2] == 'polycistron':
            if src != entry[0]:
                if src is not None:
                    # Not the very first entry
                    # Register last itergenic
                    chrom = src.replace('.', '_')
                    additions.append((line_no, [
                        src, 'tvlab', f'{ori}stream',
                        str(beg + 1), chromosome_sizes[src], 0, '.', 0,
                        f'polycistron_lid "{l_pid}"; polycistron_rid "{chrom}.PC-1"'
                    ]))
                    beg = 0
                src = entry[0]
                chrom = src.replace('.', '_')
                l_pid = f'{chrom}.PC00'
            pid = parse_feat(entry[-1])['polycistron_id']
            ori = {'+': 'up', '-': 'down'}[entry[6]]
            additions.append((line_no, [
                src, 'tvlab', f'{ori}stream',
                str(beg + 1),
                str(int(entry[3]) - 1), 0, '.', 0,
                f'polycistron_lid "{l_pid}"; polycistron_rid "{pid}"'
            ]))
            l_pid = pid
            beg = int(entry[4])
    # The very last dangling region
    additions.append((line_no + 1, [
        src, 'tvlab', f'{ori}stream',
        str(beg + 1), chromosome_sizes[src], 0, '.', 0,
        f'polycistron_lid "{l_pid}"; polycistron_rid "{chrom}.PC-1"'
    ]))
    return additions


def load_gtf(gtf_file):
    """
    Load gtf file, drop comment lines.

    Parameters
    ----------
    gtf_file : Path
        GTF records file

    Returns
    -------
    List[str]
        Individual feature entries
    """
    with open(gtf_file) as gtf_h:
        csv_lines = csv.reader(gtf_h.readlines(), delimiter='\t')
    return [
        entry for entry in csv_lines
        if ((len(entry) == 9) and (entry[0][0] not in ('!', '#', '>')))
    ]


def main():
    """Entry Point."""
    # the only cli argument expected is the file-name
    args = sys.argv
    if len(sys.argv) != 4:
        print(
            f'Usage: {args[0]} IN_FILE_PATH.gtf genome.fna OUT_FILE_PATH.gtf')
        return 1
    args.pop(0)
    in_file, genome, out_file = [Path(args[n]) for n in range(3)]
    gtf_entries = load_gtf(in_file)
    chromosome_sizes = {
        str(record.id): len(record.seq)
        for record in fasta_parser(genome, 'fasta')
    }
    polycistrons = locate_polycis(gtf_entries)
    with_polycis = insert_entries(gtf_entries, polycistrons)
    intergenic = locate_intergenic(with_polycis, chromosome_sizes)
    with_intergenic = insert_entries(with_polycis, intergenic)
    with_locus = add_loci(with_intergenic)
    out_file.write_text('\n'.join(
        ('\t'.join((str(word) for word in entry)) for entry in with_locus)))


if __name__ == '__main__':
    main()
