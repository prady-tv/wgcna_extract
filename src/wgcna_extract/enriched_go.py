#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-
"""
Claculate GO enrichments using fisher exact test
"""

import sys
from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
from multiprocessing import Pool
from os import sched_getaffinity
from pathlib import Path
from typing import Any, Dict

import pandas as pd

from wgcna_extract.chromosomal import analyse
from wgcna_extract.contingency import contingency_mxn_qual
from wgcna_extract.enriched_terms import parse_annotation
from wgcna_extract.feature_stats import analyse_skew
from wgcna_extract.parsers import (GafRecord, GffRecord, GtfRecord,
                                   parse_modules)
from wgcna_extract.plots import plot_enumerate, plot_logit

NPROC = (len(sched_getaffinity(0)) - 1) or 1
"""Available processors - 1"""

VAGUE = ('', 'protein', 'conserved', 'hypothetical', 'putative', 'subunit',
         'domain', 'containing', 'unknown', 'uncharacterized', 'family',
         'uncharacterised')
"""Vague terms"""

INTRA_SEPARATORS = (' ', '_', '-', '/', ' ', ',', '|')
"""Intra-field separators"""

GO_DICT: Dict[str, str] = {
    'C': 'Cellular Component',
    'F': 'Molecular Function',
    'P': 'Biological Process',
    'GO:0000275':
    'mitochondrial proton-transporting ATP synthase complex, catalytic sector F(1)',
    'GO:0005742': 'mitochondrial outer membrane translocase complex',
    'GO:0005747': 'mitochondrial respiratory chain complex I',
    'GO:0005753': 'mitochondrial proton-transporting ATP synthase complex',
    'GO:0005762': 'mitochondrial large ribosomal subunit',
    'GO:0005763': 'mitochondrial small ribosomal subunit',
    'GO:0005838': 'proteasome regulatory particle',
    'GO:0005852': 'eukaryotic translation initiation factor 3 complex',
    'GO:0006012': 'galactose metabolic process',
    'GO:0006511': 'ubiquitin-dependent protein catabolic process',
    'GO:0007059': 'chromosome segregation',
    'GO:0010608': 'post-transcriptional regulation of gene expression',
    'GO:0019774': 'proteasome core complex, beta-subunit complex',
    'GO:0030990': 'intraciliary transport particle',
    'GO:0042759': 'long-chain fatty acid biosynthetic process',
    'GO:0044786': 'cell cycle DNA replication',
    'GO:0060271': 'cilium assembly',
    'GO:0071162': 'CMG complex',
    'GO:0090465': 'intracellular arginine homeostasis'
}


def _translate(args):
    gff_thes: GffRecord = args[0]
    gtf_thes: GtfRecord = args[1]
    gid: str = args[2]
    return (gff_thes.translate(gtf_thes.translate(gid)), gid)


def go_gtf_equiv(out_dir, gff_thes, gtf_thes):
    cache_file = out_dir / 'sheets/gtf-to-gaf-indices.csv'
    if cache_file.is_file():
        return pd.DataFrame(pd.read_csv(cache_file,
                                        index_col=0)).iloc[:, 0].to_dict()
    with Pool(NPROC) as executor:
        go_equiv = dict(
            (executor.map_async(_translate,
                                ((gff_thes, gtf_thes, gid)
                                 for gid in gtf_thes.attributes.index))).get())
        pd.Series(go_equiv).to_csv(cache_file)
    return go_equiv


def load_go(gff: Path, gtf: Path, gaf: Path, out_dir: Path) -> pd.DataFrame:
    """
    Load GO qualifiers (parsed GAF file)

    pd.DataFrame[index = <transcript_id> , cols = <Qualifier>]
    """
    gtf_thes = GtfRecord(gtf)
    gff_thes = GffRecord(gff)
    gaf_thes = GafRecord(gaf)
    go_equiv = go_gtf_equiv(out_dir, gff_thes, gtf_thes)
    go_data = gaf_thes.go_info.rename(index=go_equiv)  # type: ignore
    assert go_data is not None
    go_data.reindex(gtf_thes.attributes.index)
    go_data.rename(columns={
        col_name: col_name.replace('|', '-').replace('NOT', 'not')
        for col_name in go_data.columns
    },
                   inplace=True)
    return go_data


def cli() -> Dict[str, Any]:
    """Parse command line"""
    parser = ArgumentParser(
        formatter_class=ArgumentDefaultsHelpFormatter,
        description='''Count enriched GO terms from modules.''')
    parser.add_argument('base-dir',
                        default='.',
                        type=Path,
                        help='Analyse files in this directory.')
    parser.add_argument('--gff', type=Path, help='gff file')
    parser.add_argument('--gaf', type=Path, help='gaf file')
    parser.add_argument('--gtf', type=Path, help='gtf file')
    parser.add_argument('--out-dir',
                        default='enrichments',
                        type=Path,
                        help='Directory created inside BASE-DIR')
    parser.add_argument('--term-sep',
                        type=str,
                        nargs='+',
                        help='Terms are separated by',
                        default=INTRA_SEPARATORS)
    parser.add_argument('-v',
                        '--keep-vague',
                        action='store_false',
                        dest='mask_vague',
                        help=f"Don't mask vague terms: {VAGUE}")
    parser.add_argument('-p',
                        '--p-value',
                        type=float,
                        help='Enrichment p-value cut off',
                        default=1e-3)
    return vars(parser.parse_args())


def go_term_skew(names: pd.Series,
                 module_labels: Dict[str, str],
                 out_dir: Path,
                 alpha: float = 0.001):
    _module_labels = pd.Series(module_labels).groupby(
        level=0).value_counts().unstack()
    names = names.apply(lambda x: str(x).replace(',', '').replace('"', '').
                        replace("'", '').replace('(', '').replace(')', ''))
    annot_terms = parse_annotation(names)
    common_index = annot_terms.index.intersection(_module_labels.index)
    _module_labels = _module_labels.loc[common_index]
    annot_terms = annot_terms.loc[common_index]
    cont = contingency_mxn_qual(qual_1=annot_terms,
                                qual_2=_module_labels,
                                qual_1_name='terms',
                                qual_2_name='module')
    cont.to_csv(out_dir / 'sheets/GO-terms_contingency.csv')
    chi2_res = analyse_skew(cont, alpha)
    chi2_score, pfish, logit = chi2_res['chi2_score'], chi2_res[
        'pfish'], chi2_res['logit']

    if not pfish.empty:
        pfish.to_csv(out_dir / 'sheets/GO-terms_pfisher.csv')
    else:
        print('Nothing significant')
    if not logit.empty:
        logit.to_csv(out_dir / 'sheets/GO-terms_logit.csv')
        plot_logit(logit,
                   'GO-terms',
                   out_dir,
                   groupby='module',
                   units='',
                   xlabels='module',
                   force_resolution=True,
                   resolution=2,
                   module_plots=True)
        plot_enumerate(logit, out_dir, 'GO-terms')
    return chi2_score.pvalue


def main() -> int:
    """Main routine"""
    cliargs = cli()
    out_dir = cliargs['base-dir'] / cliargs['out_dir']
    out_dir.mkdir(parents=True, exist_ok=True)
    (out_dir / 'sheets').mkdir(parents=True, exist_ok=True)
    (out_dir / 'images').mkdir(parents=True, exist_ok=True)
    module_labels = parse_modules(cliargs['base-dir'])
    all_quals = load_go(cliargs['gff'], cliargs['gtf'], cliargs['gaf'],
                        out_dir)
    alpha = cliargs['p_value']
    _dependency = {}
    _dependency['GO-terms'] = go_term_skew(all_quals['name'], module_labels,
                                           out_dir, alpha)

    for feature in all_quals.columns:
        _dependency[feature] = analyse(all_quals,
                                       module_labels,
                                       feature,
                                       alpha,
                                       out_dir,
                                       resolution=0.125)

    dependency = pd.DataFrame(_dependency).T
    dependency.to_csv(out_dir / 'sheets/GO-dependence.csv')

    return 0


if __name__ == '__main__':
    sys.exit(main())
