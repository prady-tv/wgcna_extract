#!/usr/bin/env python3
# -*- coding: utf-8; mode: python -*-

from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
from pathlib import Path
from typing import Dict

import pandas as pd
from matplotlib import pyplot as plt
from numpy import log2
from scipy.stats import hypergeom

from wgcna_extract.chromosomal import extract_genetic_info
from wgcna_extract.enriched_go import go_gtf_equiv
from wgcna_extract.enriched_pathway import parse_json
from wgcna_extract.enriched_terms import parse_annotation
from wgcna_extract.parsers import (GafRecord, GffRecord, GtfRecord,
                                   parse_modules)

GO_DICT: Dict[str, str] = {
    'C': 'Cellular Component',
    'F': 'Molecular Function',
    'P': 'Biological Process',
    'GO:0000275':
    'mitochondrial proton-transporting ATP synthase complex, catalytic sector F(1)',
    'GO:0005742': 'mitochondrial outer membrane translocase complex',
    'GO:0005747': 'mitochondrial respiratory chain complex I',
    'GO:0005753': 'mitochondrial proton-transporting ATP synthase complex',
    'GO:0005762': 'mitochondrial large ribosomal subunit',
    'GO:0005763': 'mitochondrial small ribosomal subunit',
    'GO:0005838': 'proteasome regulatory particle',
    'GO:0005852': 'eukaryotic translation initiation factor 3 complex',
    'GO:0006012': 'galactose metabolic process',
    'GO:0006511': 'ubiquitin-dependent protein catabolic process',
    'GO:0007059': 'chromosome segregation',
    'GO:0010608': 'post-transcriptional regulation of gene expression',
    'GO:0019774': 'proteasome core complex, beta-subunit complex',
    'GO:0030990': 'intraciliary transport particle',
    'GO:0042759': 'long-chain fatty acid biosynthetic process',
    'GO:0044786': 'cell cycle DNA replication',
    'GO:0060271': 'cilium assembly',
    'GO:0071162': 'CMG complex',
    'GO:0090465': 'intracellular arginine homeostasis'
}


def logit(neigh_occur, module_occur, p_value):
    num_mod = module_occur.sum()
    num_neigh = neigh_occur.sum()
    phypergeom = pd.Series(1., index=neigh_occur.index)
    neigh_occur = pd.concat((neigh_occur,
                             pd.Series(0,
                                       index=module_occur.index.difference(
                                           neigh_occur.index))))
    for term in module_occur.index:
        phypergeom[term] = hypergeom(num_mod, module_occur[term], num_neigh).pmf(neigh_occur[term])
        phypergeom.sort_values(inplace=True)
        skewed_idx = phypergeom < p_value
    neigh_odds = neigh_occur[skewed_idx] * num_mod / (module_occur[skewed_idx] * num_neigh)
    return log2(neigh_odds).sort_values()


# read module supplied as first command line argument.
def annot_occur(module, neigh, p_value=0.001):
    # num_mod = module.shape[0]
    module_occur = parse_annotation(module['altName']).sum(axis=0)

    # read DRG neighbours supplied as first command line argument.
    # num_neigh = neigh.shape[0]
    neigh_occur = parse_annotation(neigh['altName']).sum(axis=0)
    print("\n\nAnnotation terms Skew")
    print(logit(neigh_occur, module_occur, p_value))

def pathway_occur(module, neigh, brite, go_equiv, p_value=0.001):
    all_quals = parse_json(brite, go_equiv)
    module_quals = all_quals.loc[all_quals.index.intersection(module.index)]
    neigh_quals = module_quals.loc[all_quals.index.intersection(neigh.index)]
    print("\n\nPathway skews")
    for col in module_quals.columns:
        print("\n", col)
        print(logit(neigh_quals[col].value_counts(), module_quals[col].value_counts(), p_value))


def chromosomal_occur(module, neigh, gtf, p_value=0.001):
    all_quals = extract_genetic_info(GtfRecord(gtf))
    module_quals = all_quals.loc[all_quals.index.intersection(module.index)]
    neigh_quals = module_quals.loc[all_quals.index.intersection(neigh.index)]
    print("\n\nGenetic skews")
    for col in module_quals.columns:
        print("\n", col)
        print(logit(neigh_quals[col].value_counts(), module_quals[col].value_counts(), p_value))

def go_qual_occur(module, neigh, go_equiv, gaf, p_value=0.001):
    gaf_thes = GafRecord(gaf)
    all_quals = gaf_thes.go_info.rename(index=go_equiv)  # type: ignore
    module_quals = all_quals.loc[all_quals.index.intersection(module.index)]
    neigh_quals = module_quals.loc[all_quals.index.intersection(neigh.index)]
    print("\n\nGO Qualifiers skew")
    for col in module_quals.columns:
        print("\n", col)
        print(logit(neigh_quals[col].value_counts(), module_quals[col].value_counts(), p_value))

def motifs_occur(module, neigh, motifs, p_value=0.001):
    rbpm_maps = pd.read_csv(motifs, index_col=0)
    neigh_occur = pd.Series(index=rbpm_maps.index)
    module_occur = pd.Series(index=rbpm_maps.index)
    for motif_name, motif_series in rbpm_maps.iterrows():
        transcripts = set(eval(motif_series.iloc[0]))
        neigh_occur.loc[motif_name] = len(transcripts.intersection(set(neigh.index.to_list())))
        module_occur.loc[motif_name] = len(transcripts.intersection(set(module.index.to_list())))
    print("\n\nMotif Skew")
    print(logit(neigh_occur, module_occur, p_value))

def cli():
    parser = ArgumentParser(
        formatter_class=ArgumentDefaultsHelpFormatter,
        description='''Count enriched annotation features GOI neighbours.''')
    parser.add_argument('--gtf', type=Path, help='gtf file')
    parser.add_argument('--gff', type=Path, help='gff file')
    parser.add_argument('--gaf', type=Path, help='gaf file')
    parser.add_argument('--gtf-gaf', type=Path, help='equiv file')
    parser.add_argument('--motifs', type=Path, help='transcript motifs')
    parser.add_argument('--json',
                        type=Path,
                        help='json file',
                        default=Path('brite.json'))
    parser.add_argument('neigh', type=Path, help='neighbours file')
    parser.add_argument('module', type=Path, help='modules file')
    parser.add_argument('--p-value',
                        type=float,
                        help='Enrichment p-value cut off (p < P_VALUE)',
                        default=1e-3)
    return vars(parser.parse_args())

def main():
    cliargs = cli()
    module = pd.read_csv(cliargs['module'], sep='\t', index_col=0)
    neigh = pd.read_csv(cliargs['neigh'], sep='\t', index_col=0)
    go_equiv = pd.read_csv(cliargs['gtf_gaf'], index_col=0).iloc[:, 0]
    p_value = cliargs['p_value']
    annot_occur(module, neigh, p_value)
    pathway_occur(module, neigh, cliargs['json'], go_equiv, p_value)
    chromosomal_occur(module, neigh, cliargs['gtf'])
    go_qual_occur(module, neigh, go_equiv, cliargs['gaf'], p_value)
    motifs_occur(module, neigh, cliargs['motifs'], p_value)


if __name__ == '__main__':
    main()
