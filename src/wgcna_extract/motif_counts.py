#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-
#
# Copyright © 2022-2023 Pradyumna Paranjape
# This file is part of WGCNA_Extract.
#
# WGCNA_Extract is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# WGCNA_Extract is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FIT FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with WGCNA_Extract.  If not, see <https://www.gnu.org/licenses/>.
#
"""Count mofifs per feature"""

import json
from argparse import ArgumentParser, RawDescriptionHelpFormatter
from functools import reduce
from multiprocessing import Pool
from os import sched_getaffinity
from pathlib import Path
from typing import Any, Dict, Iterable, List, Literal, Tuple, Union

import pandas as pd

from wgcna_extract.gtf_reannotate import parse_feat
from wgcna_extract.motif_map import CISMotif, chromo_int

NPROCS = (len(sched_getaffinity(0)) - 1) or 1
"""Available processors"""


class GTFeature:
    """
    Feature Locus

    Args:
        entry: GTF entry
        offset: Chromosome Offsets dict

    Answers 'whether a mapping is located within the featue'
    """

    def __init__(self,
                 entry: Iterable[str],
                 offsets: Dict[Union[str, int], int],
                 unique_tag: str = 'locus_tag'):
        _entry = list(entry)
        self.attributes = parse_feat(_entry[-1])
        """Miscellaneous attributes"""

        self.locus = self.attributes[unique_tag]
        """Unique locus handle"""

        offset = offsets[chromo_int(_entry[0])]

        self.start = int(_entry[3]) + offset
        """Begin"""

        self.end = int(_entry[4]) + offset
        """End"""

        self.orientation = _entry[6]
        """
        Orientation: +, -, .

        '.' always returns ``True`` when asked whether
            mapping is in correct orientation
        """

    def __repr__(self) -> str:
        return 'GTFeature: ' + ', '.join(
            (f'Locus = {self.locus}', f'start = {self.start=}',
             f'orientation = {self.orientation}'))

    def __gt__(self, other):
        return int(self.locus.split('.')[1].split('_')[1]) > int(
            other.locus.split('.')[1].split('_')[1])

    def __contains__(self, mapping: Tuple[int, int, bool, bool]):
        """
        Args:
            mapping:
                beg: beginning of mapping
                end: end of mapping (beg + len(motif))
                sense: ? orientation is sense
                matters: orientation matters (drop antisense)
        """
        beg, end, sense, matters = mapping
        if (matters and (self.orientation != '.')
                and ((self.orientation == '+') ^ sense)):
            return False
        if beg < self.start:
            return False
        if end > self.end:
            return False
        return True


def _correct_orient(orient_matters, map_sense, feat_sense):
    """
    True if orient does not matter
    OR
    map_sense == feat_sense
    OR
    feat_sense does not matter
    """
    return ((map_sense == 'feat_sense') or (feat_sense == '.')
            or (not orient_matters))


def _map_arbeiter(args) -> Tuple[str, Tuple[Literal['+', '-'], int]]:
    hits, motif_len, feat_locus, feat_start, feat_end, map_sense = args
    feat_stop = feat_end - motif_len
    count = reduce(
        lambda cumul, hit: cumul + int((feat_start <= hit <= feat_stop)), hits,
        0)
    return feat_locus, (map_sense, count)


def feature_occurrence(features: Iterable[GTFeature],
                       motifs: Dict[str, CISMotif],
                       motif_map: Dict[str, Dict[str, Dict[Literal['+', '-'],
                                                           List[int]]]],
                       orient_matters: bool = False) -> pd.DataFrame:
    """
    Build Occurrence table: x: features, y: motifs

    Difficult to parallelize, try writing a ufunc or a mapper function

    Args:
        features: List of features
        motif_map: mappings columns: motifs, rows: chromosomes
    """
    occur: Dict[str, Dict[str, Dict[Literal['+', '-'], int]]] = {}
    job_size = len(motifs)
    with Pool(NPROCS) as pool:
        for num, (motif_id,
                  mappings) in enumerate(motif_map['mapping'].items()):
            print(f'{100. * num / job_size: 0.2f}%')
            occur[motif_id] = {
                feat.locus: {
                    '+': 0,
                    '-': 0
                }
                for feat in features
            }
            prom = pool.map_async(
                _map_arbeiter,
                ((hits, len(motifs[motif_id]), feat.locus, feat.start,
                  feat.end, map_sense) for map_sense, hits in mappings.items()
                 for feat in features if _correct_orient(
                     orient_matters, map_sense, feat.orientation)))
            for feat_loc, (sense, count) in prom.get():
                occur[motif_id][feat_loc][sense] = count
    occur_df = pd.DataFrame(occur)
    occur_df.index.name = 'feature'
    occur_df.columns.name = 'motif'
    return occur_df


def cli() -> Dict[str, Any]:
    """
    Parse command line arguments
    """
    parser = ArgumentParser(description='''
    - Generate Occurrence tables from motif-maps, gtf feature:
        Occurrence: Motif, Features

    - Calculate binomial skews for motifs in features, groups of features
        Given propensity of A, T, G, C per feature/group

    ''',
                            formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument('-g', '--gtf', type=Path, help='GTF reference file')

    parser.add_argument('-c',
                        '--chargaff',
                        type=Path,
                        help='Nucleotide counts')
    parser.add_argument('-m',
                        '--motif-map',
                        type=Path,
                        help='JSON file with motifs mapped to gtf features')
    parser.add_argument('-M',
                        '--motif-wobble',
                        type=Path,
                        help='JSON file with motif wobble lists')
    parser.add_argument(
        '-O',
        '--orientation',
        action='store_true',
        help='Orientation of reads matters (drop all counter-sense maps)')
    parser.add_argument('-o',
                        '--output',
                        type=Path,
                        help='File to dump outputs')
    return vars(parser.parse_args())


def main():
    """Main routine"""
    cliargs = cli()
    # read data
    gtf = pd.DataFrame(
        pd.read_csv(cliargs['gtf'], sep='\t', header=None, index_col=False))

    with open(cliargs['motif_wobble']) as wobble_js:
        cisbp_wobble = json.load(wobble_js)
    cisbp = {
        motif_id: CISMotif(str(motif_id), wobble=wobble)
        for motif_id, wobble in cisbp_wobble.items()
    }

    offsets = pd.read_csv(
        (cliargs['motif_map'].parent / 'chromosome_offsets.csv'),
        index_col=0).iloc[:, 0].to_dict()

    features = [GTFeature(feat, offsets) for _, feat in gtf.iterrows()]

    with open(cliargs['motif_map']) as map_file:
        print('calculating occurrence', flush=True)
        cont_table = feature_occurrence(features, cisbp, json.load(map_file),
                                        cliargs['orientation'])
    cont_table.to_csv(cliargs['output'])


if __name__ == '__main__':
    main()
