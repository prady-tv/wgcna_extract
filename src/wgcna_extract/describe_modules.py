#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-

import sys
from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
from os import PathLike
from pathlib import Path
from typing import Any, Dict, Tuple

import pandas as pd
import yaml
from wordcloud import WordCloud


class DirectedWordCloud(WordCloud):

    def __init__(self, *args, **kwargs):
        self.colors = {}
        """Dict of word to color"""
        super().__init__(*args, **kwargs)

    def _monochrome_func(self, word, *_a, **_k):
        return self.colors.get(word, 'grey')

    def recolor(self, random_state=None, colormap=None):
        return super().recolor(random_state, self._monochrome_func, colormap)

    def generate_from_frequencies(self, frequencies, max_font_size=None):
        abs_freq = {
            str(word): abs(float(freq))
            for word, freq in frequencies.items()
        }
        for word, freq in frequencies.items():
            if freq > 0:
                self.colors[str(word)] = 'green'
            else:
                self.colors[str(word)] = 'red'

        return super().generate_from_frequencies(abs_freq, max_font_size)


def cli() -> Dict[str, Any]:
    """Parse command line"""
    parser = ArgumentParser(
        formatter_class=ArgumentDefaultsHelpFormatter,
        description='''Count enriched GO terms from modules.''')
    parser.add_argument('base-dir',
                        default='.',
                        type=Path,
                        help='Analyse modules from this directory.')
    return vars(parser.parse_args())


def extract(qual_file: Path) -> Tuple[str, Dict[str, Dict[str, float]]]:
    """Extract index as a list"""
    qual_df = pd.DataFrame(pd.read_csv(qual_file, index_col=0))
    if len(qual_df):
        qual_ser = qual_df.iloc[:, 0]
    else:
        return qual_file.stem, {}
    pos = qual_ser[qual_ser.ge(0)].dropna().to_dict()
    neg = qual_ser[qual_ser.le(0)].dropna().to_dict()
    desc = {}
    if pos:
        desc['enrich'] = pos
    if neg:
        desc['not'] = neg
    return qual_file.stem, desc


def depict_wordcloud(module: Path,
                     description: Dict[str, Dict[str, Dict[str, float]]]):
    word_freq = {}
    for qual, direction in description.items():
        word_freq = {
            **word_freq,
            **{
                str(word): freq
                for desc in direction.values() for word, freq in desc.items()
            }
        }
    if not word_freq:
        return
    wc = DirectedWordCloud(
        background_color='white',
        normalize_plurals=False).generate_from_frequencies(word_freq)
    wc.recolor()
    wc_svg = wc.to_svg(embed_font=True)
    f = open(module / 'wordcloud.svg', "w+")
    f.write(wc_svg)
    f.close()


def discover(base_dir: PathLike):
    """Discover modules and sheets"""
    _base_dir = Path(base_dir)
    org_desc: Dict[str, Dict[str, Dict[str, Dict[str, float]]]] = {}
    for module in _base_dir.glob('*'):
        if module.is_dir():
            # a module directory
            description = dict(
                extract(qualifier) for qualifier in module.glob('*')
                if (qualifier.is_file() and qualifier.stem != 'Motifs'
                    and qualifier.suffix not in ('.yml', '.svg')))
            org_desc[module.stem] = description
            if description:
                with open(module / 'description.yml', 'w') as of_h:
                    yaml.safe_dump(description, of_h)
                depict_wordcloud(module, description)
    org_desc = {mod: desc for mod, desc in org_desc.items() if len(desc)}
    org_desc = dict(
        sorted(org_desc.items(), key=lambda x: len(x[1]), reverse=True))
    return org_desc


def main() -> int:
    """Main routine"""
    cliargs = cli()
    org_desc = discover(cliargs['base-dir'])
    out_file = cliargs['base-dir'] / 'modules.yml'
    with open(out_file, 'w') as of_h:
        yaml.safe_dump(org_desc, of_h)
    return 0


if __name__ == '__main__':
    sys.exit(main())
