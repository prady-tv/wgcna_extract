#!/usr/bin/env python3
# -*- coding: utf-8; mode:python; -*-
#
# Copyright © 2022-2023 Pradyumna Paranjape
# This file is part of WGCNA_Extract.
#
# WGCNA_Extract is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# WGCNA_Extract is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FIT FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with WGCNA_Extract.  If not, see <https://www.gnu.org/licenses/>.
#
"""
Plot log-odds (logit) in bits from DataFrame.

- Split into two rows whenever necessary
- Extend to two columns if too many bars
- Subplots
"""

from functools import reduce
from math import ceil
from pathlib import Path
from typing import Dict, Hashable, Optional, Union

import matplotlib
import numpy as np
import pandas as pd
from matplotlib import patches
from matplotlib import pyplot as plt
from matplotlib.axes import Axes
from matplotlib.figure import Figure
from numpy.typing import NDArray

COLWIDTH = 9 / 2.54
ASPECT = np.sqrt(2)  # A4 size
matplotlib.rc('figure', labelsize=10)
matplotlib.rc('xtick', labelsize=8)
matplotlib.rc('ytick', labelsize=7)


def plot_logit(logit: pd.DataFrame, feature: str, out_dir: Path, **kwargs):
    """
    Plot Logit values.

    Parameters
    ----------
    logit : pd.DataFrame
        logit values: terms x modules
    feature : str
        name of feature
    out_dir : Path
        save image relative to
    kwargs : dict[str, Any]
        groupby: {'feature', 'module'}
            Group bars by common descriptor [default: feature]
        xlabels : {'feature', 'module', None}
            Label X axis based on
        units : str
            units of feature
        resolution : Union[int, float]
            How to declutter xlabels

            Contexts:
                - int: stagger with supplied periodicity
                - float: tilt anti-clockwise by this fraction of circle
                - 1: auto-tilt to fit
                - 0: Don't resolve [default]

        force_resolution : bool
            force resolution even when unnecessary
        module_plots : bool
            generate plots for each module
    """
    if 'unclassified' in logit.columns:
        logit.drop('unclassified', inplace=True, axis=1)

    if 'unclassified' in logit.index:
        logit.drop('unclassified', inplace=True, axis=0)

    groupby: str = kwargs.get('groupby') or 'feature'
    xlabels: Optional[str] = kwargs.get('xlabels')
    units: str = kwargs.get('units') or ''
    if units:
        units = ' ' + units
    logit = logit.dropna(how='all', axis=0).dropna(how='all', axis=1)

    if groupby != 'module':
        logit = logit.T

    # Crunch non NA values into dicts
    logit_groups: Dict[Hashable, Dict[Hashable, float]] = {
        grouper: groups.dropna().to_dict()
        for grouper, groups in logit.items()
    }

    # sort each group by bar height (directed)
    logit_groups = {
        group: {
            qual: height
            for qual, height in sorted(
                bar_group.items(), key=lambda x: x[1], reverse=True)
        }
        for group, bar_group in logit_groups.items()
    }

    # if grouping by modules, sort modules by content
    if groupby == 'module':
        logit_groups = {
            group: bar_group
            for group, bar_group in sorted(
                logit_groups.items(), key=lambda x: len(x[1]), reverse=True)
        }

    num_bars: int = reduce(lambda x, y: x + len(y), logit_groups.values(), 0)

    n_plot_rows = ceil(num_bars / 50)

    # Plot
    figcols = 1
    figrows = 1
    if num_bars >= 30:
        figcols = 2
    if num_bars >= 100:
        figrows = 2
    _subpl = plt.subplots(nrows=n_plot_rows,
                          layout='constrained',
#                          figsize=(figcols * COLWIDTH,
#                                   figrows * COLWIDTH / ASPECT),
                          dpi=300)
    fig: Figure = _subpl[0]
    axs: Union[NDArray, Axes] = _subpl[1]
    # fig.set_suptitle(f'Genetic Feature: {feature}')

    lpg = 0
    # last plotted group
    logit_group_keys = list(logit_groups.keys())

    crowded = False
    if isinstance(axs, Axes):
        crowded = split_plot_logit(axs,
                                   logit_groups,
                                   feature=feature,
                                   **kwargs)
    else:
        for ax_row, ax in enumerate(axs):
            split_bars = 0
            split_groups = {}
            while ((split_bars < (num_bars // n_plot_rows))
                   and (lpg < len(logit_groups))):
                group = logit_group_keys[lpg]
                if split_bars + len(logit_groups[group]) < 75:
                    lpg += 1
                    split_groups[group] = logit_groups[group]
                    split_bars += len(logit_groups[group])
                else:
                    break
            if split_groups:
                crowded |= split_plot_logit(ax,
                                            split_groups,
                                            feature=feature,
                                            **kwargs)
            else:
                axs[ax_row].remove()
                # gridspec.update(nrows=ax_row)

    fig.add_subplot(111, frameon=False)
    plt.tick_params(labelcolor='none',
                    which='both',
                    top=False,
                    bottom=False,
                    left=False,
                    right=False)
    if xlabels == 'module':
        fig.supxlabel('modules')
    else:
        fig.supxlabel(feature + units)
    fig.supylabel('log-odds (bits)')
    if crowded:
        fig.autofmt_xdate()
    plt.savefig(out_dir / f'images/{feature}_log_odds.png')
    if kwargs.get('module_plots', False):
        for module in logit:
            plot_module_logit(logit,
                              module=str(module),
                              feature=feature,
                              out_dir=out_dir,
                              **kwargs)


def split_plot_logit(ax: Axes, logit_groups: Dict, **kwargs):
    """Plot logit bars for subset of groups.

    When 'too-many' bars are to be plotted, split them in multiple axes and plot
    a subset using this function.

    Parameters
    ----------
    ax : Axes
        matplotlib axes
    logit_groups : Dict
        Subset of logit values to be plotted in this axes split.
    **kwargs : dict[str, Any]
        groupby: {'feature', 'module'}
            Group bars by common descriptor [default: feature]
        xlabels : {'feature', 'module', None}
            Label X axis based on
        units : str
            units of feature
        resolution : Union[int, float]
            How to declutter xlabels

            Contexts:
                - int: stagger with supplied periodicity
                - float: tilt anti-clockwise by this fraction of circle
                - 1: auto-tilt to fit
                - 0: Don't resolve [default]

        force_resolution : bool
            force resolution even when unnecessary
        module_plots : bool
            generate plots for each module
    """
    groupby: str = kwargs.get('groupby') or 'feature'
    xlabels: Optional[str] = kwargs.get('xlabels')

    vals = tuple(
        float(a_val) for val in logit_groups.values()
        for a_val in val.values() if a_val not in (np.inf, -np.inf))
    max_val = (max(vals) if vals else 0) + 1
    min_val = (min(vals) if vals else 0) - 1
    if min_val >= 0:
        min_val = -1
    if max_val <= 0:
        max_val = 1

    group_dict = {}
    atomic_dict = {}
    bar_list = []

    group_x = 0.
    width = 0.3
    group_gap = width * 2
    atomic_x = 0.
    inf_bars = []

    for grouper, row in logit_groups.items():
        # next row
        group_x += atomic_x + group_gap
        atomic_x = 0.
        for atomic, val in row.items():
            atomic_x += width
            module = grouper if groupby == 'module' else atomic
            color = ''.join(
                (char for char in str(module) if not char.isdigit()))
            if val == np.inf:
                val = max_val
            elif val == -np.inf:
                val = min_val
            bar, = ax.bar(group_x + atomic_x,
                          val,
                          width,
                          color=color,
                          edgecolor='black')
            if val in (max_val, min_val):
                inf_bars.append((ax, bar, color, (val == min_val)))
            if any(char.isdigit() for char in str(module)):
                bar.set_hatch('oo')
            # if (val == max_val + 1) or (val == min_val - 1):
            #     set_gradient(ax, bar, color=color)
            bar_list.append(bar)
            atomic_dict[group_x + atomic_x + width] = atomic
        group_dict[group_x + (atomic_x + width) / 2] = grouper
    ax.axhline(0, color='black')  # type: ignore
    for bar_args in inf_bars:
        infbars(*bar_args)
    if xlabels is None or xlabels.lower() == 'none':
        ax.set_xticks([], minor=False)  # type: ignore
        ax.set_xticks([], minor=True)  # type: ignore
        return False

    elif xlabels == groupby:
        ax.set_xticks(list(group_dict.keys()))  # type: ignore
        ax.set_xticklabels(list(group_dict.values()))  # type: ignore

    else:
        ax.set_xticks(list(atomic_dict.keys()))  # type: ignore
        ax.set_xticklabels(list(atomic_dict.values()))  # type: ignore

    # three ways to resolve xlabels
    # make x-labels vertical
    # stagger x-labels
    # tilt x-labels

    if len(ax.get_xticks()) > 30 or kwargs.get('force_resolution'):
        resolution: Union[int, float] = kwargs.get('resolution') or 0
        if not resolution:
            return False
        if isinstance(resolution, float):
            ax.set_xticklabels(ax.get_xticklabels(),
                               rotation=resolution * 360.,
                               ha='right')
            return False
        if resolution == 1:
            # auto-tilt
            return True
        pad_ticks(ax, resolution)
    return False


def pad_ticks(ax: Axes, period: int = 2):
    """
    Increase padding periodically to stagger tick labels.

    Parameters
    ----------
    ax : Axes
        matplotlib axes handle
    period : int
        padding returns to original after N labels
    """
    for tick_rank, tick in enumerate(ax.xaxis.get_major_ticks()):
        tick.set_pad((tick_rank % period) * tick.label.get_fontsize() +
                     tick.get_pad())


def infbars(ax: Axes,
            bar: patches.Rectangle,
            color: str,
            negative: bool = False):
    """
    Modify 'infinite' Bars.

    Parameters
    ----------
    ax : Axes
        matplotlib axes handle
    bar : patches.Rectangle
        bar handle
    color : str
        matplotlib-recognised color of bar (proximal)
    negative : bool
        bar is drawn in negative direction
    """
    # bar extent
    x, y = bar.get_xy()
    w, h = bar.get_width(), bar.get_height()

    # add 'inf' at the end of bar
    symbol_color = 'yellow' if color == 'red' else 'red'
    text_y = y + h
    if negative:
        text_y += 0.1 * h
    else:
        text_y -= 0.025 * h
    ax.text(x + w / 2,
            text_y, ('-' if negative else '') + '\u221e',
            ha='center',
            color=symbol_color)
    bar.set_edgecolor("none")
    line_width = bar.get_linewidth()
    line_color = 'black'
    line_style = bar.get_linestyle()
    # Draw vertical edges
    ax.plot((x, x), (y, y + h),
            color=line_color,
            linewidth=line_width,
            linestyle=line_style)
    ax.plot((x + w, x + w), (y, y + h),
            color=line_color,
            linewidth=line_width,
            linestyle=line_style)


def plot_module_logit(logit: pd.DataFrame, module: str, feature: str,
                      out_dir: Path, **kwargs):
    """
    Plot Logit values for module.

    Parameters
    ----------
    logit : pd.DataFrame
        logit values: terms x modules
    module: str
        module to plot
    feature : str
        name of feature
    out_dir : Path
        save image relative to
    kwargs : dict[str, Any]
        groupby: {'feature', 'module'}
            Group bars by common descriptor [default: feature]
        xlabels : {'feature', 'module', None}
            Label X axis based on
        units : str
            units of feature
        resolution : Union[int, float]
            How to declutter xlabels

            Contexts:
                - int: stagger with supplied periodicity
                - float: tilt anti-clockwise by this fraction of circle
                - 1: auto-tilt to fit
                - 0: Don't resolve [default]


        force_resolution : bool
            force resolution even when unnecessary
        module_plots : bool
            generate plots for each module
    """
    units: str = kwargs.get('units') or ''
    if units:
        units = ' ' + units
    logit_module = logit.dropna(how='all', axis=0).dropna(how='all',
                                                          axis=1)[module]

    # Crunch non NA values into dicts
    logit_module.dropna(inplace=True)
    logit_module.sort_values(ascending=False, inplace=True)

    max_val = logit_module[logit_module != np.inf].max() + 1
    min_val = logit_module[logit_module != -np.inf].min() - 1
    if min_val >= 0:
        min_val = -1
    if max_val <= 0:
        max_val = 1

    logit_module.replace(np.inf, max_val, inplace=True)
    logit_module.replace(-np.inf, min_val, inplace=True)

    # Plot
    figcols = 1
    figrows = 1
    if logit_module.size >= 30:
        figcols = 2
    if logit_module.size >= 100:
        figrows = 2
    _subpl = plt.subplots(nrows=1,
                          layout='constrained',
#                          figsize=(figcols * COLWIDTH,
#                                   figrows * COLWIDTH * 0.7071),
                          dpi=300)
    fig: Figure = _subpl[0]
    ax: Union[NDArray, Axes] = _subpl[1]
    # fig.set_suptitle(f'Genetic Feature: {feature}')

    assert isinstance(ax, Axes)
    xlabels: Optional[str] = kwargs.get('xlabels')

    color = ''.join((char for char in str(module) if not char.isdigit()))
    if logit_module.empty:
        return
    logit_module.replace(np.inf, max_val).replace(-np.inf, min_val)
    bars = ax.bar(logit_module.index,
                  logit_module,
                  color=color,
                  edgecolor='black')
    ax.axhline(0, color='black')  # type: ignore
    for bar in bars:
        x, y = bar.get_xy()
        if y in (max_val, min_val):
            infbars(ax, bar, bar.get_facecolor(), (y == min_val))

    if xlabels is None or xlabels.lower() == 'none':
        ax.set_xticks([], minor=False)  # type: ignore
        ax.set_xticks([], minor=True)  # type: ignore
        return False

    # three ways to resolve xlabels
    # make x-labels vertical
    # stagger x-labels
    # tilt x-labels

    if len(ax.get_xticks()) > 5:
        ax.set_xticklabels(ax.get_xticklabels(),
                           rotation=0.125 * 360.,
                           ha='right')
    fig.add_subplot(111, frameon=False)
    plt.tick_params(labelcolor='none',
                    which='both',
                    top=False,
                    bottom=False,
                    left=False,
                    right=False)
    fig.supylabel('log-odds (bits)')
    plt.savefig(out_dir / f'images/{feature}_{module}_log_odds.png')


def plot_enumerate(logit: pd.DataFrame, out_dir: Path, feature: str):
    """
    Plot horizontal bars enumerating number of skewed features.

    Parameters
    ----------
    logit : pd.DataFrame
        dataframe of features x modules (NaNs are dropped)
    out_dir : Path
        output directory
    feature : str
        feature name
    """
    _subpl = plt.subplots(nrows=1,
                          layout='constrained',
#                          figsize=(COLWIDTH, COLWIDTH * 0.7071),
                          dpi=300)
    fig: Figure = _subpl[0]
    ax: Union[NDArray, Axes] = _subpl[1]
    feature_skews: pd.Series = logit.count().sort_values()
    feature_skews.drop(feature_skews[feature_skews == 0].index, inplace=True)
    assert isinstance(ax, Axes)
    color = [
        ''.join((char for char in str(module) if not char.isdigit()))
        for module in feature_skews.index
    ]
    bar = ax.barh(feature_skews.index,
                  feature_skews,
                  color=color,
                  edgecolor='black')
    ax.bar_label(
        bar,
        fmt='% 2d',
        fontsize=6,
        padding=-12,
        # padding=2,
        color='#7f7f7f',
        weight='bold')
    fig.supxlabel(feature)
    # if any(char.isdigit() for char in str(module)):
    #     bar.set_hatch('oo')
    plt.savefig(out_dir / f'images/{feature}_num.png')
