#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-
#
# Copyright © 2022-2023 Pradyumna Paranjape
# This file is part of WGCNA_Extract.
#
# WGCNA_Extract is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# WGCNA_Extract is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FIT FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with WGCNA_Extract.  If not, see <https://www.gnu.org/licenses/>.
#
"""Map motifs (derived from pwm) to features."""

import json
import re
from argparse import ArgumentParser, RawDescriptionHelpFormatter
from functools import reduce
from multiprocessing import Pool
from os import sched_getaffinity
from pathlib import Path
from time import time
from typing import (Any, Dict, Generator, Iterable, List, Optional, Sequence,
                    Tuple, Union)

import numpy as np
import pandas as pd
from Bio.SeqIO import SeqRecord
from Bio.SeqIO import parse as fasta_parser

from wgcna_extract.gtf_reannotate import parse_feat

NPROCS = (len(sched_getaffinity(0)) - 1) or 1
"""Available parallel processors (spare 1)"""


class GTFeature:
    """
    GTF Feature handle.

    Parsed from GTF Entries.

    Parameters
    ----------
    entry: Iterable[str]
        GTF entry
    unique_tag: str
        feature attribute that can be used as unique handle
    """

    def __init__(self, entry: Iterable[str], unique_tag: str = 'locus_tag'):
        _entry = list(entry)
        self.attributes = parse_feat(_entry[-1])
        """Miscellaneous attributes"""

        self.handle = self.attributes[unique_tag]
        """Unique locus handle"""

        self.source = str(_entry[0])
        """Feature source, refers to FASTA index handle"""

        self.feat_type = str(_entry[2])
        """Type of feature"""

        self.start = int(_entry[3])
        """Begin"""

        self.end = int(_entry[4])
        """End"""

        self.orientation = str(_entry[6])
        """``+``, ``-``, ``.``"""

    def __repr__(self) -> str:
        """Represent instance."""
        return 'GTFeature: ' + ', '.join(
            (f'Locus = {self.handle}', f'start = {self.start}',
             f'end = {self.end}', f'orientation = {self.orientation}'))

    def __gt__(self, other):
        """Handle unique id is greater."""
        return int(self.handle.split('.')[1].split('_')[1]) > int(
            other.locus.split('.')[1].split('_')[1])

    def __contains__(self, mapping: Tuple[int, int, bool, bool]):
        """
        Feature 'contains' mapping.

        Parameters
        ----------
        mapping : tuple
            beg : int
                beginning of mapping
            end : int
                end of mapping (beg + len(motif))
            sense : bool
                orientation is sense
            orientation : bool
                orientation matters (drop counter-sense)
        """
        beg, end, sense, orientation = mapping
        return (((not orientation) or self._correct_orient(sense))
                and (beg >= self.start) and (end <= self.end))

    def _correct_orient(self, sense: bool) -> bool:
        """
        Feature orientation and mapping sense align.

        Always ``True`` when :attr:`orientation` is '.'.

        Parameters
        ----------
        sense : bool
            ``True`` => in direction of sequencing, ``False`` otherwise.

        Returns
        -------
        bool
            ``True`` of orientations align
        """
        return (self.orientation == '.') or ((self.orientation == '-') ^ sense)


def parse_gtfeatures(gtfile: Path) -> Generator[GTFeature, None, None]:
    """
    Parse gtf file to extract :class:`GTFeature`.

    Parameters
    ----------
    gtfile : Path
        File with GTF records

    Yields
    ------
    :class:`GTFeature`
        parsed feature
    """
    for _, feat in pd.DataFrame(
            pd.read_csv(gtfile, sep='\t', header=None,
                        index_col=False)).iterrows():
        yield GTFeature(feat)


class CISMotif:
    """
    Motif handle summarizing pattern, degeneracy, length.

    Parameters
    ----------
    id : str
        Motif ID
    wobble : Sequence[Iterable[str]]
        wobble at position
    replace_u : bool
        with t
    """

    def __init__(self,
                 id: str,
                 wobble: Sequence[Iterable[str]],
                 replace_u: bool = True):
        self.id = id
        """Motif Identifier"""

        self._degen: Optional[int] = None

        self._regexp: Optional[str] = None
        """Regular expression pattern that describes the motif"""

        self._rcregexp: Optional[str] = None
        """Regular expression as reversed complement"""

        self.wobble = [[nuc.replace('U', 'T') for nuc in pos]
                       for pos in wobble] if replace_u else wobble

    def __len__(self):
        """Length of motif (nucleotides)."""
        return len(self.wobble)

    @property
    def rc_wobble(self) -> List[List[str]]:
        """Reverse Complement of wobble."""
        return list(
            reversed(
                list([self._complement(b) for b in bases]
                     for bases in self.wobble)))

    @property
    def regexp(self) -> str:
        """Regular expression pattern."""
        if self._regexp is None:
            _wobble: List[List[str]] = list(
                list(bases) for bases in self.wobble)
            restr = self._gen_restr(_wobble)
            self._regexp = ''.join(restr)
        return self._regexp

    @regexp.setter
    def regexp(self, regexp: Union[str, re.Pattern]):
        self._regexp = str(regexp)

    @regexp.deleter
    def regexp(self):
        self._regexp = None

    @property
    def rcregexp(self) -> str:
        """Reversed complement of regular expression."""
        if self._rcregexp is None:
            restr = self._gen_restr(self.rc_wobble)
            self._rcregexp = ''.join(restr)
        return self._rcregexp

    @property
    def degen(self):
        """Degeneracy associated with the sequence."""
        if self._degen is None:
            self._degen = reduce(lambda x, y: x * (len(tuple(y)) or 4),
                                 self.wobble, 1)
        return self._degen

    @staticmethod
    def _complement(bases: str):
        return ''.join({
            'T': 'A',
            'A': 'T',
            'G': 'C',
            'C': 'G'
        }.get(b, b) for b in bases)

    @staticmethod
    def _gen_restr(_wobble: List[List[str]]):
        restr = []
        for base in _wobble:
            base_degen = len(base) or 4
            if base_degen == 1:
                restr.append(base[0])
            elif base_degen < 4:
                restr.append('[' + ''.join([nuc for nuc in base]) + ']')
            else:
                restr.append('.')
        for idx, term in enumerate(restr):
            if len(term) == 5:
                antiterm = ''.join(anuc
                                   for anuc in (set(['A', 'T', 'G', 'C']) -
                                                set(term[1:4])))
                restr[idx] = f'[^{antiterm}]'
        return restr

    def map_in_seq(self,
                   sequence: str,
                   *args,
                   revcomp: bool = False,
                   **kwargs) -> Tuple[int, ...]:
        """
        Map motif in target using regular expression search.

        Parameters
        ----------
        sequence
            target sequence
        revcomp
            look for reverse complement of motif instead
        *args
            pass to :meth:`re.finditer`
        **kwargs
            pass to :meth:`re.finditer`

        Returns
        -------
        tuple[int, ...]
            All regular expression Matches
        """
        pattern = self.rcregexp if revcomp else self.regexp
        return tuple(
            hit.span()[0]
            for hit in re.finditer(pattern, sequence, *args, **kwargs))

    def p_occur(self,
                counts: Optional[Dict[str, int]] = None,
                revcomp: bool = False) -> float:
        """
        Probability of occurrence of motif given A, T, G, C counts in feature.

        Parameters
        ----------
        counts : Optional[Dict[str, int]]
            Counts of A, T, G, C
        revcomp : bool
            consider reverse complement probability as well

        Returns
        -------
        float
            probability of occurrence of motif
        """
        size = 1 if counts is None else sum(counts.values())
        freq = {
            nuc: count / size
            for nuc, count in counts.items()
        } if counts else {
            'A': 0.25,
            'T': 0.25,
            'G': 0.25,
            'C': 0.25
        }
        prob = reduce(lambda a, b: a * reduce(lambda x, y: x + freq[y], b, 0.),
                      self.wobble, 1.)
        if not revcomp:
            return prob

        revprob = 0.
        freq['A'], freq['T'] = freq['T'], freq['A']
        freq['C'], freq['G'] = freq['G'], freq['C']
        revprob = reduce(
            lambda a, b: a * reduce(lambda x, y: x + freq[y], b, 0.),
            self.rc_wobble, 1.)
        return (prob + revprob) / 2


class ProgressTracker:
    """
    Track, estimate, inform progress.

    Parameters
    ----------
    job_size : int
        Expected number of events.
        If not supplied, each update call must supply completion fraction.
    """

    def __init__(self, job_size: int = 0):
        self.start_time = time()
        """Job initiated at"""

        self.job_size = job_size
        """Total Job size"""

        self.last_printed: str = ''
        """Last printed string"""

        self.last_flushed_perc: int = 0
        """Last flushed percentage"""

    @staticmethod
    def human_time(sec: int) -> str:
        """
        Convert time from seconds to Y yrs, M mth, D day, H hrs, M min, S sec.

        Parameters
        ----------
        sec : int
            time in seconds

        Returns
        -------
        str
            Three most significant parts of time
        """
        part_sizes: Dict[str, Tuple[int, int]] = {
            'yrs': (0x100000000, 31557600),
            'mth': (31557600, 2629800),
            'day': (2629800, 86400),
            'hrs': (86400, 3600),
            'min': (3600, 60),
            'sec': (60, 1),
        }
        time_str = []
        for part, (size_up, size_self) in part_sizes.items():
            num = (sec % size_up) // size_self
            if num:
                time_str.append(f'{num} {part}')
            if len(time_str) > 2:
                break
        return ', '.join(time_str)

    def done(self):
        r"""Erase ETA and print ``\n`` at the end of process tracking."""
        done_str = f'Done in {self.human_time(int(time() - self.start_time))}'
        print('\b' * len(self.last_printed) + done_str +
              (' ' * (len(self.last_printed) - len(done_str))),
              flush=True)

    def update(self, complete: float = -1., item: Optional[str] = None):
        r"""
        Print ETA.

        Parameters
        ----------
        complete : float
            If tracker was initialized with `job_size`,
            *Number* of items completed;
            else, *Fraction* of job completed.
            If not supplied, only elapsed time is printed.
        item : Optional[str]
            current item being processed


        .. warning::
            Updates don't print ``\n`` character.
            Remember to call :meth:`ProgressTracker.done` at the end of loop
        """
        frac = 2.
        if self.job_size:
            frac = complete / self.job_size
        elif 0 <= complete < 1:
            frac = complete
        perc = int(frac * 100)
        if perc <= self.last_flushed_perc:
            return
        print('\b' * len(self.last_printed), end='')
        elapsed = int(time() - self.start_time)
        elapsed_str = self.human_time(elapsed)
        std_out: List[str] = [f'Working {elapsed_str}.']
        if frac < 1.:
            predicted_str = self.human_time(int(elapsed * (1 / frac - 1)))
            std_out.extend((f'Completed {perc}%.', f'ETA: {predicted_str}.'))
        if item:
            std_out.append(f'Processing {item}.')
        out_str = ' '.join(std_out)
        self.last_printed = out_str + (' ' * max(
            (len(self.last_printed) - len(out_str)), 0))
        self.last_flushed_perc = perc
        print(self.last_printed, end='', flush=True)


def parse_motifs(base_dir: Path,
                 threshold: float = 0.2) -> Generator[CISMotif, None, None]:
    """
    Parse motifs from directory.

    Parameters
    ----------
    base_dir : Path
        base directory in which, M_<NNN>.txt files hold pwm values
    threshold : float
        probability below which, nucleotide is ignored

    Yields
    ------
    Motif Object handles
    """
    # We may have stored a parsed cache in the parent directory
    cache_motif_file = base_dir.parent / f'motifs_thres_{threshold}.json'

    if cache_motif_file.is_file():
        print('Loaded motifs from pre-parsed cache.')
        with (cache_motif_file).open('r') as motif_js:
            cached_motifs: Dict[str, List[List[str]]] = json.load(motif_js)
            for motif_id, motif_wobble in cached_motifs.items():
                yield CISMotif(motif_id, motif_wobble)

        return
    cisbp: List[CISMotif] = []
    bases: Sequence[Iterable[str]]
    for motif_file in base_dir.glob('*.txt'):
        try:
            pwm = pd.DataFrame(
                pd.read_csv(motif_file,
                            delimiter='\t',
                            index_col=0,
                            dtype=np.float_)).T
            wobble_vals = pwm.apply(
                lambda row: row[row > threshold].index.to_list(), axis=0)
            if isinstance(wobble_vals, pd.Series):
                bases = wobble_vals.to_list()  # type: ignore [assignment]
            else:
                bases = wobble_vals.iloc[0, :].to_list()
        except pd.errors.EmptyDataError:
            continue
        cisbp.append(CISMotif(motif_file.stem.split('_')[0], bases))

    # Hold a copy
    print('Saving parsed motifs for future reference.')
    with (cache_motif_file).open('w') as motif_js:
        json.dump({motif.id: motif.wobble for motif in cisbp}, motif_js)

    # yield for consistency (we would have returned otherwise)
    for motif in cisbp:
        yield motif


def _arbeiter_suchen(args) -> Tuple[str, int]:
    """
    Worker to parallelly search motif.

    Parameters
    ----------
    args: tuple
        motif : :class:`CISMotif`
            motif handle
        sequence : str
            extracted feature sequence
        orient_matters : bool
            does orientation of match matter?

    Returns
    -------
    tuple[str, int]
        motif-id : number of hits
    """
    motif: CISMotif = args[0]
    sequence: str = args[1]
    orient_matters: bool = args[2]
    hits = len(motif.map_in_seq(sequence, re.IGNORECASE, revcomp=False))
    if not orient_matters:
        hits += len(motif.map_in_seq(sequence, re.IGNORECASE, revcomp=True))
    return motif.id, hits


def extract_seq(
        feat: GTFeature,
        fasta: Dict[str, SeqRecord]) -> Tuple[SeqRecord, Dict[str, int]]:
    """
    Extract sequence given gtf entry and genomic sequences.

    Parameters
    ----------
    feat : :class:`GTFeature`
        Genome Feature
    fasta : SeqRecord
        Genomic FASTA sequences

    Returns
    -------
    tuple[:class:`Bio.SeqIO.SeqRecord`, dict[str, int]]
        Forward sequence (In direction as indicated by feature entry)
    """
    sense = fasta[feat.source][feat.start - 1:feat.end]
    if feat.orientation == '-':
        return sense.reverse_complement()
    return sense


def map_occurrence(features: Iterable[GTFeature],
                   fasta: Dict[str, SeqRecord],
                   cisbp: Iterable[CISMotif],
                   orientation: bool = False,
                   **kwargs) -> pd.DataFrame:
    """
    Map occurrences of any of the wobbles of the motifs.

    Parameters
    ----------
    features : Iterable[:class:`GTFeature`]
        Genome Features
    fasta : Dict[str, SeqRecord]
        corresponding FASTA sequences
    cisbp : Iterable[:class`CISMotif`]
        Motifs to locate and count
    orientation : orientation of mapping is relevant while searching
    **kwargs
        verbose : bool
            Inform at every percentage point completion about ETA
        nproc : int
            number of parallel processes. 0 (default) => available - 1

    Returns
    -------
    :class:`pandas.DataFrame`
        Occurrence : counts, features x motifs
    """
    eta_tracker: Optional[ProgressTracker] = None
    if kwargs.get('verbose', False):
        features = list(features)
        eta_tracker = ProgressTracker(len(features))

    # cisbp is referenced by each feature process
    cisbp = list(cisbp)

    occurrences: Dict[str, Dict[str, int]] = {}
    nprocs = int(kwargs.get('nproc') or NPROCS)
    with Pool(nprocs) as pool:
        if kwargs.get('verbose', False):
            print(f'Counting with {nprocs} processor(s).')
        for feat in features:
            if eta_tracker:
                eta_tracker.update(len(occurrences), feat.handle)
            seq = str(extract_seq(feat, fasta))
            occurrences[feat.handle] = dict(
                pool.map_async(_arbeiter_suchen, ((motif, seq, orientation)
                                                  for motif in cisbp)).get())
        if eta_tracker:
            eta_tracker.done()

    print('Saving...')
    occur_df = pd.DataFrame(occurrences, dtype=np.int_).T
    occur_df.columns.name = 'motif'
    occur_df.index.name = 'feature'
    occur_df.fillna(0, inplace=True)
    return occur_df


def cli() -> Dict[str, Any]:
    """
    Parse command line arguments.

    Returns
    -------
    dict[str, Any]
        command-line args as a dict
    """
    parser = ArgumentParser(description=('\n'.join(
        ('Count occurrence (features x motifs) from', '- Motifs (pwms)',
         '- Genomic sequence (FASTA)', '- Features (gtf)'))),
                            formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument('-v',
                        '--verbose',
                        action='store_true',
                        help='Show progress (slower but supervised)')
    parser.add_argument(
        '-p',
        '--nproc',
        metavar='NUM',
        type=int,
        default=0,
        help=f'Number of parallel processors to use, [default: {NPROCS}]')
    parser.add_argument('-g',
                        '--gtf',
                        type=Path,
                        required=True,
                        help='GTF reference file')
    parser.add_argument('-f',
                        '--fasta',
                        metavar='SEQ',
                        type=Path,
                        required=True,
                        help='Corresponding sequence in FASTA format')
    parser.add_argument(
        '-m',
        '--pwms',
        metavar='DIR',
        type=Path,
        required=True,
        help='Directory containing motif PWMs as separate files')
    parser.add_argument('-o',
                        '--occur',
                        metavar='CSV',
                        type=Path,
                        required=True,
                        help='File to save occurrence table')
    parser.add_argument(
        '-t',
        '--threshold',
        metavar='P',
        type=float,
        default=0.2,
        help='Nucleotides with PWM < P are ignored from search')
    parser.add_argument(
        '-O',
        '--orientation',
        action='store_true',
        help='Orientation of mapping matters (drop counter-sense hits)')
    return vars(parser.parse_args())


def main():
    """Entry Point."""
    cliargs = cli()
    cisbp = parse_motifs(cliargs['pwms'], threshold=cliargs['threshold'])
    features = parse_gtfeatures(cliargs['gtf'])
    genome = {
        record.id: record
        for record in fasta_parser(cliargs['fasta'], 'fasta')
    }
    occurrences = map_occurrence(
        features, genome, cisbp,
        **{key: cliargs[key]
           for key in ('orientation', 'verbose', 'nproc')})
    occurrences.to_csv(cliargs['occur'])


if __name__ == '__main__':
    main()
