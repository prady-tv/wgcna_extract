#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-
# Copyright © 2022 Pradyumna Paranjape
#
# This file is part of wgcna_extract.
#
# wgcna_extract is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# wgcna_extract is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with wgcna_extract. If not, see <https://www.gnu.org/licenses/>.
#
"""Palette color functions"""

from typing import List, Optional, Sequence, Tuple, Union

import numpy as np
from matplotlib import colormaps
from matplotlib.colors import Colormap
from numpy.typing import NDArray

from wgcna_extract.errors import WgcnaExtractError


class BioModulePaintError(WgcnaExtractError):
    """Module color assignment error"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class GenericPalette():
    """
    Generic color map
    Copied under LGPL from self-owned project
    https://gitlab.com/pradyparanjpe/presentps.git

    Colormap need not be linear. It may be defined by a sequence values which
    are equally spaced in the color-space.
    Color-space being the number of defined nodes.
    Between adjacent nodes, the map is assumed to be linear.

    Args:
        cpal: sequence of defined nodes (minimum two bounds are essential)
        name: name of palette
    """

    def __init__(self,
                 cpal: Sequence[Sequence[float]],
                 *,
                 name: Optional[str] = None):
        self.name = name
        """Palette name"""
        self._palette: NDArray[np.float_] = np.array(cpal, dtype=np.float_)
        color_sz = self._palette.shape[-1]
        if not 2 < color_sz < 5:
            raise ValueError(
                f'Colors must be of the form RGB[A] found size {color_sz}')
        if color_sz == 3:
            self._palette = np.concatenate(
                (self._palette, np.ones((self._palette.shape[0], 1))), axis=1)
        if self.N < 2:
            raise ValueError(f'At least two nodes are essential. Got {self.N}')

    @property
    def N(self):
        """
        Total defined color_space.
        Undefined colors are inferred
        as weighted average of its defined bounds.

        for compatibility with :py:class:`matplotlib.colors.Colormap`
        """
        return self._palette.shape[0]

    def __call__(self,
                 X: Union[int, float, Sequence[Union[float, int]],
                          NDArray[Union[np.float_, np.int_]]],
                 alpha: Union[float, Sequence[float], NDArray[np.float_]] = 1.,
                 bytes: bool = False) -> NDArray[Union[np.float_, np.int_]]:
        """
        Values for 'X' th color

        Args:
            X: 0. < X < 1., color fraction of color_space
            alpha: 0 < alpha < 1, corresponding alpha values or None.
            bytes: bool: convert from color space of ``[0, 1]`` to ``[0, 255]``

        Returns:

        """
        # arrayfy X
        if isinstance(X, Sequence):
            _X: NDArray[np.float_] = np.array(X, dtype=np.float_)
        elif isinstance(X, (float, int)):
            _X = np.array((X, ), dtype=np.float_)
        else:
            _X = np.array(X, dtype=np.float_)

        # arrayfy alpha
        _alpha = np.ones((*_X.shape, 4), dtype=np.float_)
        if isinstance(alpha, (int, float)):
            alpha = min(alpha, 1)
            _alpha[:, -1] = alpha
        else:
            _alpha[-1] = np.array(alpha)
        if np.all(_X > 1) and np.all(_X % 1 == 0):
            _X /= self.N
        _X %= 1
        cols = np.ones(_alpha.shape)
        for idx, point in np.ndenumerate(_X):
            num_i = int(point * self.N)
            if point * self.N == num_i:
                # All items are exact
                cols[idx, :] = self._palette[num_i, :]
            else:
                # scaled intrapolation
                num_h = (num_i + 1) % self.N
                scale = point * self.N - num_i
                cols[idx, :] = (self._palette[num_h, :] - self._palette[
                    num_i, :]) * scale + self._palette[num_i, :]
        cols *= _alpha
        if bytes:
            cols = np.array(np.floor(cols * 255), dtype=np.int_)
        return cols


def _drop_dups(a_2d: NDArray[np.float_], ambiguity: float = 1 / 0x100):
    """Drop duplicate colors within ambiguity"""
    try:
        _a_2d = a_2d[:, :3]
    except IndexError as err:
        raise BioModulePaintError('a_2d should be of the form RGB[A]') from err
    order = np.lexsort(_a_2d.T)
    _a_2d = _a_2d[order]
    diff = np.diff(_a_2d, axis=0)
    uid = np.ones(len(_a_2d), dtype=np.bool_)
    uid[1:] = (diff > ambiguity).any(axis=1)
    return a_2d[uid]


def _drop_avoid(a_2d: NDArray[np.float_],
                avoid: Sequence[Tuple[float, ...]],
                ambiguity: float = 1 / 0x100):
    """
    Drop colors from a_2d

    that are supplied in avoid

    if they are within ambiguity away

    Return dropped
    """
    if not avoid:
        return a_2d
    try:
        _avoid = np.array(list(pig[:3] for pig in avoid), dtype=np.float_)
        _a_2d = a_2d[:, :3]
    except IndexError as err:
        raise BioModulePaintError(
            'avoid and a_2d should be of the form RGB[A]') from err
    _a_diff = _a_2d[:, None, :3] - _avoid[None, :, :3]
    _a_uniq = np.any(np.all(_a_diff > ambiguity, axis=-1), axis=-1)
    return a_2d[_a_uniq, :]


def paint_module(module_id: Union[float, Sequence[float], NDArray[np.float_]],
                 module_space: int = 1,
                 palette: Optional[Union[GenericPalette, Colormap]] = None,
                 avoid: Sequence[Tuple[float, ...]] = (),
                 ambiguity: float = 1 / 0x100):
    """
    Color for the module in context of all modules

    Args:
        module_id: id(s) of module (may be pre-processed as fraction of all)
        module_space: number of discovered modules other than grey
        palette: palette from which, colors are derived.
        avoid: avoid returning these colors (RGBA)
            if module_space is not supplied, this isn't checked.
        ambiguity: pigment fraction [0, 1] that is indistinguishable.

    Returns:
        Depending upon number of inputs (module_id), A 1D NDArray
        of color (RGBA) or a 2D array of corresponding colors.

    If module_space is not supplied (module_ids are fractional,
    we cannot ascertain increment of color_ids of unsupplied modules. Hence,
    'Avoid' subroutine, which extends module-space to avoid colors, is skipped.
    """
    if module_space < 1 or isinstance(module_space, float):
        raise BioModulePaintError(
            f"module_space should be an integer or 1, is {module_space}")

    palette = palette or next(iter(colormaps.values()))
    if palette is None:
        raise BioModulePaintError('No palette provided. Could not guess.')

    # ufunc divisible
    if isinstance(module_id, Sequence):
        _module_id: NDArray[np.float_] = np.array(module_id, dtype=np.float_)
    elif isinstance(module_id, (float, int)):
        _module_id = np.array((module_id, ), np.float_)
    else:
        _module_id = np.array(module_id, dtype=np.float_)
    if module_space == 1:
        return palette(_module_id)

    _module_idp = np.array(np.floor(_module_id), dtype=np.intp)
    module_farben = np.zeros((*_module_id.shape, 4))
    for buff_mod in range(100):
        _module_space = module_space + buff_mod
        space_farb = np.array(palette(
            [node / _module_space for node in range(_module_space)]),
                              dtype=np.float_)
        space_farb = _drop_dups(space_farb, ambiguity)
        space_farb = _drop_avoid(space_farb, avoid, ambiguity)
        if module_space <= space_farb.shape[0]:
            for mod_id, point in np.ndenumerate(_module_idp):
                module_farben[(*mod_id, slice(4))] = space_farb[(point,
                                                                 slice(4))]
            return module_farben
    raise BioModulePaintError(
        "Can't determine a color-scheme, try reducing ambiguity")


if __name__ == '__main__':
    size = 32

    null_point = 0
    single_point = 3
    multi_point = (0, 4, 8, 12, 16, 20)

    avoid = ((0, 0, 0, 0), (1, 1, 1, 1))

    ambig = 0.075

    test_palette = GenericPalette((
        (0, 0, 0, 1),
        (1, 0, 0, 1),
        (0, 1, 0, 1),
        (1, 1, 0, 1),
        (1, 1, 1, 1),
        (0, 0, 1, 1),
        (1, 0, 1, 1),
        (0, 1, 1, 1),
    ))

    print('null')
    print(paint_module(null_point, size))
    print('null avoid')
    print(paint_module(null_point, size, avoid=avoid))
    print('null avoid ambig')
    print(paint_module(null_point, size, avoid=avoid, ambiguity=ambig))
    print('null avoid custom')
    print(paint_module(null_point, size, palette=test_palette, avoid=avoid))
    print('single')
    print(paint_module(single_point, size))
    print('single avoid')
    print(paint_module(single_point, size, avoid=avoid))
    print('single avoid ambig')
    print(paint_module(single_point, size, avoid=avoid, ambiguity=ambig))
    print('single avoid ambig custom')
    print(paint_module(single_point, size, palette=test_palette, avoid=avoid))
    print('multi')
    print(paint_module(multi_point, size))
    print('multi avoid')
    print(paint_module(multi_point, size, avoid=avoid))
    print('multi avoid ambig')
    print(paint_module(multi_point, size, avoid=avoid, ambiguity=ambig))
    print('multi avoid ambig custom')
    print(
        paint_module(multi_point,
                     size,
                     palette=test_palette,
                     avoid=avoid,
                     ambiguity=ambig))
    print('all ambig custom')
    print(paint_module(range(size), size, palette=test_palette))
    print('all avoid ambig custom')
    print(
        paint_module(range(size),
                     size,
                     palette=test_palette,
                     avoid=avoid,
                     ambiguity=ambig))
