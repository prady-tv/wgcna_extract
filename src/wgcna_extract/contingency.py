#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-
#
# Copyright © 2022-2023 Pradyumna Paranjape
# This file is part of WGCNA_Extract.
#
# WGCNA_Extract is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# WGCNA_Extract is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FIT FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with WGCNA_Extract.  If not, see <https://www.gnu.org/licenses/>.
#
"""
Generate contingency for feature / module
"""

from typing import Dict, Union

import numpy as np
import pandas as pd


def joint_labels(cat_1: Dict[str, Union[str, str]],
                 cat_2: Dict[str, str],
                 cat_1_name: str = 'cat_1',
                 cat_2_name: str = 'cat_2'):
    """
    Generate joint labels given Classification Categories

    Args:
        cat_1: Category 1 of classification. (key: object, value: label(s))
        cat_2: Category 2 of classification. (key: object, value: label)
        cat_1_name: name of catgory 1
        cat_2_name: name of catgory 2

    Returns:
        Joint labels table.
        Pandas dataframe with object index and cat_1, cat_2 classes as columns
        'unclassified' class is addeded to each category if necessary
    """
    joint_df = pd.DataFrame('unclassified',
                            index=list(set((*cat_1.keys(), *cat_2.keys()))),
                            columns=[cat_1_name, cat_2_name])
    for cat_gid, cat_lab in cat_1.items():
        joint_df.loc[cat_gid, cat_1_name] = cat_lab

    for cat_gid, cat_lab in cat_2.items():
        joint_df.loc[cat_gid, cat_2_name] = cat_lab
    return joint_df


def contingency_mxn_qual(qual_1: pd.DataFrame,
                         qual_2: pd.DataFrame,
                         qual_1_name: str = 'qual_1',
                         qual_2_name: str = 'qual_2') -> pd.DataFrame:
    """
    Generate contingency table given qualifiers (boolean)

    Qualifiers for objects (pandas rows) take ``bool`` (future: ``int``)
    values for qualifying terms (pandas columns)

    Args:
        qual_1: qualifier 1.
        qual_2: qualifier 2.
        qual_1_name: name of qual 1
        qual_2_name: name of qual 2

    Returns:
        Joint contingency table without marginals.
        Pandas dataframe with:
            - qual_1 columns as index
            - qual_2 columns as column

        'unclassified' class is addeded to each qualifier if necessary
    """
    res = pd.DataFrame(0, index=qual_1.columns, columns=qual_2.columns)
    res.index.name = qual_1_name
    res.columns.name = qual_2_name
    for col_1 in res.index:
        for col_2 in res.columns:
            res.loc[col_1, col_2] = (qual_1[col_1] & qual_2[col_2]).astype(
                np.int_).sum()

    # Drop zeros
    res.fillna(False, inplace=True)
    res = res.loc[res.any(axis=1), res.any(axis=0)]
    return res


def contingency_mxn_unique(cat_1: Dict[str, str],
                           cat_2: Dict[str, str],
                           cat_1_name: str = 'cat_1',
                           cat_2_name: str = 'cat_2') -> pd.DataFrame:
    """
    Generate contingency table given Classification Categories

    Args:
        cat_1: Category 1 of classification. (key: object, value: label(s))
        cat_2: Category 2 of classification. (key: object, value: label)
        cat_1_name: name of category 1
        cat_2_name: name of category 2

    Returns:
        Joint contingency table without marginals.
        Pandas dataframe with cat_1 classes as index and cat_2 class as column
        'unclassified' class is addeded to each category if necessary
    """
    # Initiate contingency table
    joint_df = joint_labels(cat_1, cat_2, cat_1_name, cat_2_name)
    contingency = joint_df.groupby([cat_1_name,
                                    cat_2_name]).value_counts().unstack()
    contingency.index.name = cat_1_name
    contingency.columns.name = cat_2_name
    # drop '0' rows and columns
    contingency.fillna(0, inplace=True)
    contingency = contingency.loc[(contingency != 0).any(axis=1),
                                  (contingency != 0).any(axis=0)]
    return contingency


def contingency_2x2(contingency: pd.DataFrame, term_ax0: str,
                    term_ax1: str) -> pd.DataFrame:
    """
    Convert an m x n contingency table to a 2x2 contingency table.

    TODO: Make this a ufunc

    Args:
        term_ax0: target term along axis - 0 (index)
        term_ax1: target term along axis - 1 (columns)

    Returns:
        a 2 x 2 contingency pd.DataFrame:
            index: term_ax0, NOT term_ax0
            columns: term_ax1, NOT term_ax1
    """
    if (contingency.shape[0] < 2) or (contingency.shape[1] < 2):
        raise ValueError(
            f"Bad shape {contingency.shape} of input contingency table")
    if term_ax0 not in contingency.index:
        raise KeyError(f"{term_ax0} not in index of input contingency table")
    if term_ax1 not in contingency.columns:
        raise KeyError(f"{term_ax1} not in columns of input contingency table")

    contingency = contingency.astype(np.int_)

    # Initialize
    cont_2x2 = pd.DataFrame(0,
                            index=[term_ax0, 'not ' + term_ax0],
                            columns=[term_ax1, 'not ' + term_ax1],
                            dtype=np.int_)

    # O | X
    # X | X
    cont_2x2.loc[term_ax0, term_ax1] = contingency.loc[term_ax0, term_ax1]

    # X | O
    # X | X
    cont_2x2.loc[term_ax0,
                 'not ' + term_ax1] = contingency.loc[term_ax0, :].sum()
    cont_2x2.loc[term_ax0, 'not ' + term_ax1] = cont_2x2.loc[  # type: ignore
        term_ax0, 'not ' + term_ax1] - cont_2x2.loc[term_ax0,
                                                    term_ax1]  # type: ignore

    # X | X
    # O | X
    cont_2x2.loc['not ' + term_ax0,
                 term_ax1] = contingency.loc[:, term_ax1].sum()
    cont_2x2.loc['not ' + term_ax0, term_ax1] = cont_2x2.loc[  # type: ignore
        'not ' + term_ax0, term_ax1] - cont_2x2.loc[term_ax0,
                                                    term_ax1]  # type: ignore

    # X | X
    # X | O
    cont_2x2.loc['not ' + term_ax0, 'not ' +
                 term_ax1] = contingency.sum().sum() - cont_2x2.sum().sum()
    return cont_2x2
