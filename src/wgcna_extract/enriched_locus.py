#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-

import sys
from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
from functools import reduce
from itertools import product
from pathlib import Path
from typing import Any, Dict, Iterable, List, Optional, Set

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from scipy.stats import binom, binom_test, poisson

from wgcna_extract.binom_enrich import binom_test_array, plot_excesses


# Helpers
def _fp_to_farb(fp: Path) -> str:
    return str(fp).split('-')[-1].split('.')[0]


# Inputs
def module_terms(base_dir: Path) -> Dict[str, pd.DataFrame]:
    """
    Create a Module: gene_ids dict

    Args:
        base_dir: directory containing *-nodes-*.tsv records

    Returns:
        Dict with list of corresponding gene_ids for each module
        sorted ascending by number of features
    """
    terms_dict: Dict[str, pd.DataFrame] = {}
    for nodefile in base_dir.glob('*-nodes-*.tsv'):
        nodes_records = pd.DataFrame(pd.read_csv(nodefile,
                                                 delimiter='\t',
                                                 index_col=0),
                                     dtype=str)
        farb = _fp_to_farb(nodefile)
        terms_dict[farb] = pd.DataFrame(
            [tid.split('.') for tid in nodes_records.index],
            index=nodes_records.index,
            columns=['chromosome', 't_num'])
        terms_dict[farb]['chromosome'] = terms_dict[farb]['chromosome'].apply(
            lambda x: int(x.split('_')[-1]))
        terms_dict[farb]['t_num'] = terms_dict[farb]['t_num'].apply(
            lambda x: int(x.replace('T', '')))

    return dict(
        sorted([td for td in terms_dict.items()],
               key=lambda x: len(x),
               reverse=True))


# Object Structures
class LocusCounter():
    """
    LOCUS Qualifiers Counter, inspired by `:py:class:collections.Counter`

    Args:
        feature_vals: index feature id, Value: locus qualifier names
        uninformative: Iterable of terms that are not informative
        t_sep: terms are delimited in LOCUS Qualifier name by these separators
    """

    def __init__(self,
                 feature_vals: Iterable[pd.DataFrame],
                 t_sep: Iterable[str] = (),
                 uninformative: Iterable[str] = ()):
        self.feature_vals = pd.DataFrame(feature_vals, dtype=str)
        """Locus qualifiers DataFrame"""

        self.t_sep = t_sep
        """Term-Separators, if ``None``, terms are not separated"""

        self.uninformative = uninformative
        """Uninformative terms, ignored while reporting"""

        self._feature_qualifiers: Optional[pd.DataFrame] = None
        self._locus_qualifier_feats = None
        self._all_terms: Optional[pd.Series] = None
        self._term_feat_freq: Optional[pd.Series] = None

    @property
    def feature_qualifiers(self) -> pd.DataFrame:
        """
        DataFrame that lists qualifier values (Tuple) for each feature.

        Rows: feature indices
        Columns: qualifiers
        Values: Tuple of individual terms
        """
        if self._feature_qualifiers is None:
            self._feature_qualifiers = self.feature_vals.applymap(
                lambda x: (x.lower(), ) if isinstance(x, str) else x,
                na_action='ignore')
            for sep in self.t_sep:
                self._feature_qualifiers = self._feature_qualifiers.applymap(
                    lambda x: tuple(
                        (s_t for o_t in x for s_t in o_t.split(sep))),
                    na_action='ignore')
        return self._feature_qualifiers

    @property
    def all_terms(self) -> pd.Series:
        """
        All terms that occur in the complete feature set

        Pandas series of all terms
        index: qualifiers
        values: Combined set of qualifier's term-space
        """
        if self._all_terms is None:
            self._all_terms = pd.Series(
                self.feature_qualifiers.apply(
                    lambda t: reduce(lambda r, n: set(r) | set(n), t)))
        return self._all_terms

    @property
    def qualifiers(self) -> Set[str]:
        """Known qualifiers"""
        return set(self.all_terms.index)

    @property
    def num_feat(self) -> int:
        """Number of features"""
        return self.feature_vals.shape[0]

    @property
    def term_feat_freq(self) -> pd.Series:
        """
        Term feature frequencies as counts and fraction of total.

        Structure:
        multi-index:
            qualifiers: qual, ...
            terms: term, ...
        values: number of features containing the term (name: counts)
        """
        if self._term_feat_freq is None:
            term_idx = pd.MultiIndex.from_tuples(
                tuple(tup) for qual, terms in self.all_terms.to_dict().items()
                for tup in product((qual, ), terms))
            term_idx.names = ['qualifier', 'term']
            self._term_feat_freq = pd.Series(index=term_idx,
                                             name='counts',
                                             dtype=int)
            for idx in term_idx:
                self._term_feat_freq[idx[0], idx[1]] = self.feature_qualifiers[
                    idx[0]].apply(lambda y: idx[1] in y).value_counts()[True]
        return self._term_feat_freq


def cli() -> Dict[str, Any]:
    """Parse command line"""
    parser = ArgumentParser(
        formatter_class=ArgumentDefaultsHelpFormatter,
        description='''Count enriched LOCUS terms from modules.''')
    parser.add_argument('base-dir',
                        default='.',
                        type=Path,
                        help='Analyse files in this directory.')
    parser.add_argument('--out-dir',
                        default='locus_frequencies',
                        type=Path,
                        help='Directory created inside BASE-DIR')
    parser.add_argument('--p-value',
                        type=float,
                        help='Enrichment p-value cut off',
                        default=1e-3)
    return vars(parser.parse_args())


def main() -> int:
    """Main routine"""
    cliargs = cli()
    out_dir = cliargs['base-dir'] / cliargs['out_dir']
    out_dir.mkdir(parents=True, exist_ok=True)
    (out_dir / 'sheets').mkdir(parents=True, exist_ok=True)
    (out_dir / 'images').mkdir(parents=True, exist_ok=True)
    modules: Dict[str, pd.DataFrame] = module_terms(cliargs['base-dir'])
    genome_counter = LocusCounter(pd.concat(modules.values()))
    prob: pd.Series = genome_counter.term_feat_freq / genome_counter.num_feat
    prob.name = 'prob'
    for qual, freq in prob.unstack().items():
        sorted_freq = freq.dropna().sort_values(ascending=False)
        assert sorted_freq is not None
        sorted_freq.index.name = 'term'
        sorted_freq.to_csv(
            (out_dir / 'sheets' / str(qual)).with_suffix('.csv'))

    for mod, feature_vals in modules.items():
        print(f'Calculating excess for module {mod}')
        mod_counter = LocusCounter(feature_vals, cliargs.get('t_sep', ()),
                                   cliargs.get('ignore', ()))
        occur: pd.Series = mod_counter.term_feat_freq.reindex(prob.index)
        occur.fillna(0, inplace=True)
        expect = prob * mod_counter.num_feat
        # p_val = poisson_p_value(occur, expect)
        # p_val = binom_p_value(occur, mod_counter.num_feat, prob)
        p_val = binom_test_array(occur, mod_counter.num_feat, prob)
        p_val = p_val[p_val < cliargs['p_value']]
        if len(p_val) < 1:
            continue
        (out_dir / 'sheets' / mod).mkdir(parents=True, exist_ok=True)
        e_ser: pd.Series = -np.log10(p_val[occur > expect])
        e_ser: pd.Series = pd.concat((e_ser, np.log10(p_val[occur < expect])))
        e_ser.sort_values(inplace=True, ascending=False)
        e_ser.name = f'-log(p-value)'
        e_ser.to_csv(
            (out_dir / 'sheets' / mod / 'p-values').with_suffix('.csv'))

        e_val = pd.DataFrame(e_ser)
        e_val['color'] = 'grey' if mod == 'grey60' else mod
        (out_dir / 'images' / mod).mkdir(parents=True, exist_ok=True)
    plot_excesses(e_val, mod, out_dir / 'images', cliargs['p_value'])
    return 0


if __name__ == '__main__':
    sys.exit(main())
