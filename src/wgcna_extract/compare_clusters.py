#!/usr/bin/env python
# -*- coding: utf-8; mode: python; -*-
"""Ad-Hoc script to find overlaps between various Modules"""

import sys
from itertools import permutations
from pathlib import Path

import numpy as np
import pandas as pd
from psprint import print

UNI_INTERSECT = '\u2229'
"""Unicode intersect symbol"""

UNI_UNION = '\u222A'
"""Unicode union symbol"""

UNI_DIV = '\u2014'
"""Unicode em-dash symbol"""


def _fp_to_farb(fp: Path) -> str:
    return str(fp).split('-')[-1].split('.')[0]


def _farb_to_fp(farb: str, base: Path) -> Path:
    return base / f'CytoscapeInput-nodes-{farb}.tsv'


def calc_ol(i_path: Path, j_path: Path) -> float:
    """Calculate overlap"""
    i_idx = pd.DataFrame(pd.read_csv(i_path, index_col=0,
                                     delimiter='\t')).index
    j_idx = pd.DataFrame(pd.read_csv(j_path, index_col=0,
                                     delimiter='\t')).index
    try:
        return (len(i_idx.intersection(j_idx)) / len(i_idx))
    except ZeroDivisionError:
        return np.nan


def compare_clustering_pat(anal1: Path, anal2: Path) -> pd.DataFrame:
    """Create a matrix overlap between clusters derived from anal1 and anal2"""

    # Header descriptor for results
    numer = f'({anal1}) {UNI_INTERSECT} ({anal2})'
    div_line = len(numer) * UNI_DIV
    denom = str(anal1)
    denom = ' ' * ((len(div_line) - len(denom)) // 2) + denom
    print()
    for frac_part in (numer, div_line, denom):
        print(frac_part)
    print()

    # build
    idx = [_fp_to_farb(fp) for fp in anal1.glob("*-nodes-*.tsv")]
    cols = [_fp_to_farb(fp) for fp in anal2.glob("*-nodes-*.tsv")]
    ol_frame = pd.DataFrame(np.zeros((len(idx), len(cols))),
                            index=idx,
                            columns=cols)

    # populate
    for i_farb in ol_frame.index:
        for j_farb in ol_frame.columns:
            ol_frame.loc[i_farb, j_farb] = calc_ol(_farb_to_fp(i_farb, anal1),
                                                   _farb_to_fp(j_farb, anal2))
    # report
    max_ol = pd.DataFrame({'overlap': ol_frame.max(axis=1)})
    max_ol['counterpart'] = ol_frame.idxmax(axis=1)
    return pd.DataFrame(max_ol.dropna())


def main(*argv) -> int:
    """Main Routine"""
    analyses = [Path(p) for p in argv[1:]]
    assert all(a.is_dir() for a in analyses)
    for combi_pair in permutations(analyses, 2):
        print(compare_clustering_pat(combi_pair[0], combi_pair[1]),
              disabled=True)
        print('\n', disabled=True)
    print("\n", disabled=True)
    return 0


if __name__ == "__main__":
    sys.exit(main(*sys.argv))
