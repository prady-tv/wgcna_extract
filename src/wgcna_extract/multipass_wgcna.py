#!/usr/bin/env python
# -*- coding:utf-8; mode: python -*-
"""Wrapper for WGCNA"""

from argparse import (ArgumentDefaultsHelpFormatter, ArgumentParser,
                      ArgumentTypeError, Namespace)
from pathlib import Path
from typing import Dict, List, Mapping, Optional, Tuple, Union

import numpy as np
import pandas as pd
from numpy.typing import NDArray
from psprint import print
from rpy2.robjects import NULL, default_converter, numpy2ri, pandas2ri
from rpy2.robjects.conversion import Converter, localconverter
from rpy2.robjects.packages import Package, PackageNotInstalledError, importr

RLIB = str(Path(__file__).parent / 'Rlib')


class NoneConverter(Converter):

    def __init__(self, *args, **kwargs):
        super().__init__(name='NoneConverter', *args, **kwargs)
        self.py2rpy.register(type(None), self._none2null)
        self.rpy2py.register(type(None), self._null2none)

    @staticmethod
    def _none2null(obj):
        if obj is None:
            return NULL
        else:
            raise NotImplementedError

    @staticmethod
    def _null2none(obj):
        if obj is NULL:
            return None
        else:
            raise NotImplementedError


NONE_CONVERTER = NoneConverter()


def init_r(
        libraries: Optional[Dict[str, Optional[str]]] = None,
        gh_libs: Optional[Dict[str, Optional[str]]] = None,
        bc_libs: Optional[Dict[str, Optional[str]]] = None) -> List[Package]:
    """
    Initialize R environment wrap with rpy2 and return handles for functions.

    Args:
        libraries: namespaces to be imported
        gh_libs: github installations and imports
        bc_libs: bio-conductor installations and imports

    Returns:
        handles in order: *libraries_handles, *gh_libs_handles
    """
    # init R

    utils = importr('utils')
    utils.chooseCRANmirror(ind=1)
    pandas2ri.activate()

    handles: List[Package] = []
    libraries = libraries or {}

    # import/install R libraries
    for lib, lib_name in libraries.items():
        try:
            handles.append(importr(lib_name or lib, lib_loc=RLIB))
        except PackageNotInstalledError:
            utils.install_packages(lib, lib=RLIB)
            handles.append(importr(libraries[lib] or lib, lib_loc=RLIB))

    # import/install GitHub libraries
    if gh_libs:
        devtools = importr('devtools')
        for lib, lib_name in gh_libs.items():
            try:
                handles.append(importr(lib_name or lib, lib_loc=RLIB))
            except PackageNotInstalledError:
                devtools.install_github(lib, lib=RLIB)
                handles.append(importr(gh_libs[lib] or lib, lib_loc=RLIB))

    if bc_libs:
        bioc_manager = importr('BiocManager')
        for lib, lib_name in bc_libs.items():
            try:
                handles.append(importr(lib_name or lib, lib_loc=RLIB))
            except PackageNotInstalledError:
                bioc_manager.install(lib, lib=RLIB)
                handles.append(importr(bc_libs[lib] or lib, lib_loc=RLIB))
    return handles


GRDEVICES = importr('grDevices')

WGCNA, = init_r({'WGCNA': 'WGCNA'})

WGCNA.enableWGCNAThreads()


class RWGCNA():
    """
    R::WGCNA::blockwiseModules

    Args:
        **kwargs: arguments that are passed to blockwiseModules
    """

    def __init__(self, dat_expr: pd.DataFrame, **kwargs):
        self._TOM = None
        self._adjacency = None
        self._dendrograms = None
        self._module_eigengenes = None
        self._module_colors = None
        self.dat_expr = dat_expr
        self.call_kwargs = {
            'numericLabels': True,
            'pamRespectsDendro': True,
            'sft': 6,
            'TOMType': 'signed',
            'networkType': 'signed',
            # 'loadTOM': True,
            # 'saveTOMs': True,
            'verbose': 0,
            'maxBlockSize': 15000
        }
        self.call_kwargs.update(kwargs)

        self.rval: Mapping = NotImplemented

    @property
    def colors(self) -> NDArray[Union[np.int_, np.str_]]:
        """A vector of color or numeric module labels for all genes"""
        return np.array(self.rval['colors'])

    @property
    def unmergedColors(self) -> NDArray[Union[np.int_, np.str_]]:
        """
        A vector of color or numeric module labels
        for all genes before module merging.
        """
        return np.array(self.rval['unmergedColors'])

    @property
    def goodSamples(self) -> NDArray[np.int_]:
        """
        Numeric vector giving indices of good samples.

        `Good samples` := do not have too many missing entries.
        """
        return np.array(self.rval['goodSamples'], dtype=np.int_)

    @property
    def goodGenes(self) -> NDArray[np.int_]:
        """
        Numeric vector giving indices of good genes,

        `Good genes` := do not have too many missing entries.
        """
        return np.array(self.rval['goodGenes'], dtype=np.int_)

    @property
    def TOMFiles(self) -> Optional[NDArray[np.str_]]:
        """
        If `saveTOMs`==``TRUE``, a vector of character strings,
        one string per block, giving the file names of
        files (relative to current directory) in which
        blockwise topological overlaps were saved.
        """
        return np.array(self.rval['TOMFiles'],
                        dtype=np.str_) if self.rval['TOMFiles'] else None

    @property
    def blockGenes(self):
        """
        A list[R] [pydict] whose components give the indices of genes in each block.

        This is not yet wrapped and translated to its python equivalent.
        """
        return self.rval['blockGenes']

    @property
    def dendrograms(self):
        """
        A list[R] [pydict] whose components conatain hierarchical clustering
        dendrograms of genes in each block.

        This is not yet wrapped and translated into its python equivalent
        """
        return self.rval['dendrograms']

    @property
    def blocks(self):
        """
        If input blocks was given, its copy,

        Otherwise a vector of length equal number of genes giving
        the block label for each gene.

        Note that block labels are not necessarily sorted
        in the order in which the blocks were processed
        (since we do not require this for the input blocks).

        See blockOrder below.

        This is not yet wrapped and translated into its python equivalent
        """
        return self.rval['blocks']

    @property
    def blockOrder(self) -> NDArray[np.str_]:
        """
        A vector giving the order in which blocks were processed
        and in which blockGenes above is returned.

        For example,
        ``blockOrder[1]`` (``0`` for python)
        contains the label of the first-processed block.
        """

        return np.array(self.rval['blockOrder'], dtype=np.str_)

    @property
    def MEsOK(self) -> bool:
        """
        Logical[R] [py:bool] indicating whether the module eigengenes were
        calculated without errors.
        """
        return bool(self.rval['MEsOK'])

    @property
    def module_eigengenes(self) -> pd.DataFrame:
        """
        [MEs]
        A data frame containing module eigengenes of the found modules
        (given by colors).
        """
        if not self._module_eigengenes:
            if self.rval is NotImplemented:
                raise NotImplementedError
            with localconverter(default_converter + pandas2ri.converter +
                                numpy2ri.converter + NONE_CONVERTER):
                me_dict = WGCNA.moduleEigengenes(
                    self.dat_expr,
                    softPower=self.call_kwargs['sft'],
                    colors=self.module_colors)
            self._module_eigengenes = pd.DataFrame(me_dict['averageExpr'],
                                                   index=self.dat_expr.index)
        return self._module_eigengenes

    @property
    def module_colors(self):
        if self._module_colors is None:
            with localconverter(default_converter + pandas2ri.converter +
                                numpy2ri.converter + NONE_CONVERTER):
                self._module_colors = np.array(
                    WGCNA.labels2colors(self.rval['colors']))
        return self._module_colors

    @property
    def topology_overlap_matrix(self) -> pd.DataFrame:
        """
        Topology overlap matrix, either calculated from dat_expr

        [or NOT IMPLEMENTED: loaded from TOMFiles]
        """
        if self._TOM is None:
            with localconverter(default_converter + pandas2ri.converter +
                                numpy2ri.converter + NONE_CONVERTER):
                tom_matrix = WGCNA.TOMsimilarity(
                    self.adjacency.to_numpy(),
                    TOMType=self.call_kwargs['TOMType'])
                self._TOM = pd.DataFrame(tom_matrix,
                                         index=self.dat_expr.columns,
                                         columns=self.dat_expr.columns)
        return self._TOM

    @topology_overlap_matrix.deleter
    def topology_overlap_matrix(self):
        self._TOM = None

    @property
    def adjacency(self) -> pd.DataFrame:
        """Adjacency data frame calculated from dat_expr"""
        if self._adjacency is None:
            with localconverter(default_converter + pandas2ri.converter +
                                numpy2ri.converter + NONE_CONVERTER):
                adj_matrix = WGCNA.adjacency(
                    self.dat_expr,
                    power=self.call_kwargs['sft'],
                    type=self.call_kwargs['networkType'])
                self._adjacency = pd.DataFrame(adj_matrix,
                                               index=self.dat_expr.columns,
                                               columns=self.dat_expr.columns)
        return self._adjacency

    @adjacency.deleter
    def adjacency(self):
        self._adjacency = None

    def __repr__(self):
        return str(self.dat_expr) + '\n\n' + f'{self.call_kwargs}'

    def analyse(self) -> None:
        if self.rval is NotImplemented:
            with localconverter(default_converter + pandas2ri.converter +
                                numpy2ri.converter + NONE_CONVERTER):
                self.rval = WGCNA.blockwiseModules(self.dat_expr,
                                                   **self.call_kwargs)


def _domain_float(arg: str,
                  left: float = 0.,
                  right: float = 1.,
                  left_closed: bool = True,
                  right_closed: bool = True) -> float:
    """
    Restrict float within interval.

    Args:
        arg: input arg
        min: lower bound
        max: upper bound

    Returns:
        float converted form

    Raises:
        ArgumentTypeError
    """
    try:
        frac = float(arg)
    except:
        raise ArgumentTypeError(f'{arg} is not a fraction literal')
    interval: List[str] = ['(', ')']
    if left < frac < right:
        return frac
    elif left_closed and frac == left:
        interval[0] = '['
        return frac
    elif right_closed and frac == right:
        interval[1] = ']'
        return frac
    raise ArgumentTypeError(
        f'{arg} violates domain {interval[0]}{left}, {right}{interval[1]}')


def command_line() -> Namespace:
    """Parse command line arguments"""
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('--cwd',
                        help='working directory',
                        dest='dirbase',
                        type=Path,
                        default='.')
    parser.add_argument(
        '--counts',
        help='Path Name of file that contains counts Row: genes [counts.csv]',
        type=Path,
        default='counts.csv',
        dest='counts')
    parser.add_argument(
        '--meta',
        help='Path Name of file that contains metadata [meta.csv]',
        type=Path,
        default='meta.csv',
        dest='meta')
    parser.add_argument(
        '--annot',
        help='Path Name of file that contains annotations [annot.csv]',
        type=Path,
        default='annot.csv',
        dest='annot')
    parser.add_argument('--min-module-size',
                        help='Minimum number of Genes in a Module [30]',
                        default=30,
                        dest='minModuleSize',
                        type=int)
    parser.add_argument('--merge-cut-height',
                        help='Height at which modules are merged [0.]',
                        default=0.,
                        dest='mergeCutHeight',
                        type=_domain_float)
    parser.add_argument('--tom-type',
                        help='TOMType',
                        default='signed',
                        dest='TOMType',
                        choices=['signed', 'unsigned'])
    parser.add_argument('--net-type',
                        help='Network type',
                        default='signed',
                        dest='net_type',
                        choices=['signed', 'unsigned'])
    parser.add_argument(
        '--max-pass',
        type=float,
        help='maximum number of WGCNA passes. (-1 => No restriction )',
        default=-1)
    parser.add_argument(
        '--mad',
        help=
        'Use best (+ve) or worst (-ve) Qth quantile genes ranked by MAD [1.]',
        type=(lambda x: _domain_float(x, -1, 1)),
        default=1.,
        metavar='Q',
    )
    parser.add_argument('--target',
                        help='gene_id of target gene',
                        nargs='*',
                        type=str)
    return parser.parse_args()


def load_data(counts_file: Path, annot_file: Path,
              meta_file: Path) -> Dict[str, pd.DataFrame]:
    """Load data"""
    counts = pd.DataFrame(pd.read_csv(counts_file, index_col=0))
    annot = pd.DataFrame(pd.read_csv(annot_file, index_col=0))
    meta = pd.DataFrame(pd.read_csv(meta_file, index_col=0))
    return {'counts': counts, 'annot': annot, 'meta': meta}


def filter_genes(counts: pd.DataFrame, mad: float = 1.) -> pd.DataFrame:
    """
    Filter genes by MAD
    """
    if mad == 1.:
        return counts
    gene_mad = counts.mad(axis=1)
    assert isinstance(gene_mad, pd.Series)
    if mad > 0:
        filt_counts = counts.loc[gene_mad > gene_mad.quantile(1 - mad), :]
        print(
            f'Using {mad} MAD quantile, {filt_counts.shape[0]} best ranked genes left',
            mark='info')
        return filt_counts
    else:
        filt_counts = counts.loc[gene_mad < gene_mad.quantile(-mad), :]
        print(
            f'Using {mad} MAD quantile, {filt_counts.shape[0]} worst ranked genes left',
            mark='info')
        return filt_counts


def guess_sft(dat_expr: pd.DataFrame, net_type: str = 'signed') -> int:
    # powers = [*range(1, 11), *range(12, 22, 2)]
    # print(powers, flush=True, mark='bug')
    with localconverter(default_converter + pandas2ri.converter +
                        numpy2ri.converter + NONE_CONVERTER):
        sft = WGCNA.pickSoftThreshold(
            dat_expr,
            # powerVector=powers,
            networkType=net_type)
    power = sft['powerEstimate']
    return 30 if np.isnan(power) else power


def net_data(dat_expr: pd.DataFrame, sft: int, cli_args: Namespace):
    """WGCNA Wrapper that returns network object"""
    wgcna = RWGCNA(
        dat_expr=dat_expr,
        power=sft,
        TOMType=cli_args.TOMType,
        minModuleSize=cli_args.minModuleSize,
        networkType=cli_args.net_type,
        reassignThreshold=0,
        mergeCutHeight=cli_args.mergeCutHeight,
        deepSplit=4,
    )
    wgcna.analyse()
    return wgcna


def fish_target_cluster(
        dat_expr: pd.DataFrame,
        targets: List[str],
        net: Optional[RWGCNA] = None) -> Tuple[pd.DataFrame, int]:
    """Fish clusters that contain target genes"""
    if net is None:
        return dat_expr, dat_expr.shape[1]
    modules = []
    for goi in targets:
        goi_idx = dat_expr.columns.get_loc(goi)
        modules.append(net.colors[goi_idx])
    sub_bool = np.isin(net.colors, modules)
    sub_dat = dat_expr.loc[:, sub_bool]
    return sub_dat, sub_bool.sum()


def cyto_exporter(wgcna: RWGCNA, annot: pd.DataFrame, mod: str, dirbase: Path):
    print("Exporting modules colored", mod)
    module_idx = np.where(wgcna.module_colors == mod)[0]
    mod_probes = wgcna.dat_expr.columns[module_idx]
    mod_genes = annot.loc[mod_probes, 'gene_symbol']
    # Select the corresponding Topological Overlap
    modtom = pd.DataFrame(wgcna.topology_overlap_matrix.iloc[module_idx,
                                                             module_idx])
    del wgcna.adjacency  # holds unnecessary memory. May be recalculated later.
    # Export the network into edge and node list files Cytoscape can read
    edge_path_str = str(dirbase / ("CytoscapeInput-edges-" + mod + '.tsv'))
    node_path_str = str(dirbase / ("CytoscapeInput-nodes-" + mod + '.tsv'))
    with localconverter(default_converter + pandas2ri.converter +
                        numpy2ri.converter + NONE_CONVERTER):
        WGCNA.exportNetworkToCytoscape(
            modtom,
            edgeFile=edge_path_str,
            nodeFile=node_path_str,
            weighted=True,
            threshold=0.02,
            nodeNames=mod_probes,
            altNodeNames=mod_genes,
            nodeAttr=wgcna.module_colors[module_idx])


def main():
    cli_args = command_line()
    cli_args.dirbase.mkdir(parents=True, exist_ok=True)
    data = load_data(cli_args.counts, cli_args.annot, cli_args.meta)
    clust_size = 3
    old_clust_size = 4
    dat_expr = data['counts'].T
    wgcna = None
    sft = 6
    num_passes = 0
    while ((num_passes != cli_args.max_pass) and (clust_size > 2)
           and (clust_size != old_clust_size)):
        num_passes += 1
        old_clust_size = clust_size
        dat_expr, clust_size = fish_target_cluster(dat_expr, cli_args.target,
                                                   wgcna)
        print(f'Clust size: {clust_size}', mark='info')
        sub_dat = filter_genes(dat_expr.T, cli_args.mad).T
        for goi in cli_args.target:
            if goi not in sub_dat.columns:
                print(f'{goi} not found in counts', mark='warn')
                print('may have been filtered out.')
                break
            else:
                dat_expr = sub_dat
        sft = guess_sft(dat_expr, cli_args.net_type)
        print(f'Using soft thresholding power {sft}', mark='info')
        wgcna = net_data(dat_expr, sft, cli_args)
    # last network is in net
    if wgcna is None:
        return

    print('Saving module-eigengenes', mark='info')
    wgcna.module_eigengenes.to_csv(cli_args.dirbase / 'eigengenes.csv')
    print('Saving the adjacency matrix', mark='info')
    wgcna.adjacency.to_csv(cli_args.dirbase / 'adjacency.csv')
    for mod in np.unique(wgcna.module_colors):
        cyto_exporter(wgcna, data['annot'], mod, cli_args.dirbase)


if __name__ == '__main__':
    main()
