#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-
#
# Copyright © 2022-2023 Pradyumna Paranjape
# This file is part of WGCNA_Extract.
#
# WGCNA_Extract is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# WGCNA_Extract is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FIT FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with WGCNA_Extract.  If not, see <https://www.gnu.org/licenses/>.
#
"""
For each feature, count A, T, G, C
"""

import sys
from multiprocessing import Pool
from os import sched_getaffinity
from pathlib import Path
from typing import Optional, Tuple

import pandas as pd
from Bio.SeqIO import parse as fasta_parser

from wgcna_extract.gtf_reannotate import parse_feat

NPROCS = (len(sched_getaffinity(0)) - 1) or 1


def _count_arbeiter(args) -> Optional[Tuple[str, Tuple[int, int, int, int]]]:
    """
    From genome, fish sequence corresponding to entry to count nucleotides

    Args:
        genome: genome seqence
        entry: entry string in gtf format
        handle: unique feature for entry
        entry_sep: separator for columns
        *featargs: passed to :py:meth:`parse_feat` in order

    Returns:
        handle, count_of(A, T, G, C)
    """
    genome, entry, handle, entry_sep, *featargs = args
    _entry = entry.split(entry_sep)
    if len(_entry) != 9:
        return None
    features = parse_feat(_entry[-1], *featargs)
    locus_tag = features.get(handle)
    if locus_tag is None:
        return None
    sequence = genome[_entry[0]][int(_entry[3]):int(_entry[4]) + 1].upper()
    return locus_tag, tuple(
        sequence.count(nuc) for nuc in ('A', 'T', 'G', 'C'))


def main():
    args = sys.argv
    if len(sys.argv) != 4:
        print(
            f'Usage: {args[0]} IN_FILE_PATH.gtf genome.fna OUT_FILE_PATH.csv')
        return 1
    args.pop(0)
    gtf, fasta, outfile = [Path(args[n]) for n in range(3)]
    genome = {
        str(record.id): record.seq
        for record in fasta_parser(fasta, 'fasta')
    }
    gtf_entries = gtf.read_text().split('\n')
    with Pool(NPROCS) as pool:
        promise = pool.map_async(_count_arbeiter,
                                 ((genome, entry, 'locus_tag', '\t', ';', ' ')
                                  for entry in gtf_entries))
        counts = pd.DataFrame(
            {
                res[0]: [count[0] for count in zip(*res[1:])]
                for res in promise.get() if res is not None
            },
            index=['A', 'T', 'G', 'C']).T
    counts.to_csv(outfile)


if __name__ == '__main__':
    main()
