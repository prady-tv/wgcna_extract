#!/usr/bin/env python3
# -*- coding: utf-8; mode: python -*-

from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
from pathlib import Path
from typing import Any, Dict, Optional

import numpy as np
import pandas as pd
from psprint import print
from scipy.spatial.distance import pdist, squareform
from sklearn import decomposition


def _fp_to_farb(fp: Path) -> str:
    return str(fp).split('-')[-1].split('.')[0]


def load_cytoscape(base_dir: Path):
    old_clusters: Dict[str, Dict[str, pd.DataFrame]] = {}
    for nodefile in base_dir.glob('*-nodes-*.tsv'):
        nodes_records = pd.DataFrame(pd.read_csv(nodefile,
                                                 delimiter='\t',
                                                 index_col=0),
                                     dtype=str)
        old_clusters[_fp_to_farb(nodefile)] = {'nodes': nodes_records}
    for edgefile in base_dir.glob('*-edges-*.tsv'):
        edge_records = pd.DataFrame(pd.read_csv(edgefile,
                                                delimiter='\t',
                                                index_col=0),
                                    dtype=str)
        old_clusters[_fp_to_farb(edgefile)]['edges'] = edge_records
    return old_clusters


def load_eigengenes(base_dir: Optional[Path] = None,
                    fname='eigengenes.csv',
                    out_dir: Optional[Path] = None,
                    eigendf: Optional[pd.DataFrame] = None) -> Dict[str, str]:
    """
    Load corresponding eigengenes to define cluster ids

    000 is reserved for grey.

    Ids for true clusters are ranks their along first Principal axis.

    Args:
        base_dir: parent directory for auto-search. Masked if it is ``None``
        fname: filename of eigengenes file.
        out_dir: write eigenvalues to new_copy as parent directory
        eigendf: supply eigendf manually, ignore all other input kwargs
    """
    fname = (base_dir / fname) if base_dir is not None else Path(fname)
    eigendf = pd.DataFrame(pd.read_csv(fname, index_col=0), dtype=np.float_)
    eigendf.rename(columns={col: col[2:]
                            for col in eigendf.columns},
                   inplace=True)
    eigendist = pd.DataFrame(squareform(
        pdist(eigendf.T.values, metric='euclidean')),
                             columns=eigendf.columns,
                             index=eigendf.columns)
    eigendist /= eigendist.max()

    dim = min(eigendist.shape)
    dim_reduce = decomposition.PCA(dim)
    dim_reduce.fit(eigendist)
    eigen_pc = pd.DataFrame(dim_reduce.components_.T,
                            index=eigendist.columns,
                            columns=[f'PC_{num}' for num in range(dim)])
    cid = {
        color: f'cluster_{int(rankid):03d}'
        for color, rankid in eigen_pc.mean(axis=1).rank(
            method='first').to_dict().items()
    }
    eigendf.rename(columns=cid, inplace=True)
    if out_dir is not None:
        eigendf.to_csv(out_dir / fname.name)
    return cid


def write_cscape(out_dir: Path, dat: Dict[str, Dict[str, pd.DataFrame]]):
    out_dir.mkdir(exist_ok=True, parents=True)
    for cluster, dattype in dat.items():
        for ftype, dframe in dattype.items():
            dframe.to_csv((out_dir / f'{cluster}-{ftype}').with_suffix('.tsv'),
                          sep='\t')


def cli() -> Dict[str, Any]:
    parser = ArgumentParser(
        description=
        '''Rename WGCNA clusters sorting them by number of elements''',
        formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        'base_dir',
        type=Path,
        default='.',
        help='Cytoscape_exports are present in this directory.')
    parser.add_argument('--new-copy',
                        type=Path,
                        default='new-cluster-names',
                        help='directory to backup')
    return vars(parser.parse_args())


def main():
    cliargs = cli()
    renamer = load_eigengenes(cliargs['base_dir'], out_dir=cliargs['new_copy'])
    renamer['gray'] = 'cluster_0'
    renamer['grey'] = 'cluster_0'
    pd.Series(renamer, ).sort_values().to_csv(cliargs['new_copy'] /
                                              'renamer.csv')
    old_clusters = load_cytoscape(cliargs['base_dir'])
    new_clusters = {renamer[name]: val for name, val in old_clusters.items()}

    for name in new_clusters:
        new_clusters[name]['nodes'].iloc[:, -1] = name

    write_cscape(cliargs['new_copy'], new_clusters)


if __name__ == '__main__':
    main()

#  LocalWords:  eigengenes LocalWords usr gray
