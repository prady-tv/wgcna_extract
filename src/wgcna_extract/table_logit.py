#!/usr/bin/env python3
# -*- coding: utf-8; mode:python; -*-
#
# Copyright © 2022-2023 Pradyumna Paranjape
# This file is part of WGCNA_Extract.
#
# WGCNA_Extract is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# WGCNA_Extract is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FIT FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with WGCNA_Extract.  If not, see <https://www.gnu.org/licenses/>.
#
"""
Represent features with long descriptions as csv tables

FUTURE: Optionally export in org-formatted
"""

import sys
from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
from pathlib import Path
from typing import Mapping, Optional

import pandas as pd
from tabulate import tabulate

from wgcna_extract.parsers import obo_equiv


def logit_table(enrichment: Path,
                translator: Mapping[str, str],
                score_name: str = 'log-odds (bits)',
                index_name: Optional[str] = None,
                col_name: str = 'Module',
                groupby: str = 'column') -> pd.DataFrame:
    """
    Convert csv table to a Nan-filtered, sorted table.

    Args:
        enrichment: csv file containing enrichment scores (logit)
        translator: translate index values using mapping
        score_name: name of score-column
        index_name: name of index of table loaded from `enrichment`
            [default: stem of `enrichment`]
        col_name: name of column of table loaded from `enrichment`
        groupby: {'index', 'column'} Group table by...
            The other is sorted in descending order

    Returns:
        DataFrame ready to be formatted as a Table
    """
    index_name = index_name or enrichment.name
    csv_table = pd.DataFrame(pd.read_csv(enrichment, index_col=0))
    if translator:
        csv_table.rename(index=translator, inplace=True)
    csv_table.index.name = index_name
    csv_table.columns.name = col_name
    en_ser = csv_table.loc[(csv_table.index != 'unclassified'),
                           (csv_table.columns != 'unclassified')].stack()
    en_ser.name = score_name
    sorter, grouper = index_name, col_name
    if groupby == 'index':
        grouper, sorter = sorter, grouper
    sorted_table = en_ser.reset_index().sort_values(
        [grouper, score_name], ascending=[True,
                                          False]).set_index([grouper, sorter])
    group_sort_idx = sorted_table.groupby(grouper).count().sort_values(
        score_name, ascending=False).index
    return sorted_table.loc[group_sort_idx]


def cli():
    """
    Accept command-line arguments
    """
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter,
                            description='''
    Represent features with long descriptions as csv tables
    ''')
    parser.add_argument('filename',
                        help='CSV file containing enrichments',
                        metavar='CSV',
                        type=Path)
    parser.add_argument(
        '-o',
        '--out-file',
        type=Path,
        help=
        'CSV output file with sorted enrichments table (default: CSV_table.csv)'
    )
    parser.add_argument(
        '-O',
        '--org',
        action='store_true',
        help=
        'output file in org-babel-table format, output extension changed to .org'
    )
    parser.add_argument('-G',
                        '--gobo',
                        type=Path,
                        help="path to GO's obo file to get GO-equivalence")
    parser.add_argument(
        '-s',
        '--score-name',
        type=str,
        default='log-odds (bits)',
        help='Name of enrichment score, used as table values header')
    parser.add_argument('-i',
                        '--index-name',
                        type=str,
                        default=None,
                        help='Name of CSV index, used as a table header')
    parser.add_argument('-c',
                        '--col-name',
                        type=str,
                        default='Modules',
                        help='Name of CSV columns, used as a table header')
    parser.add_argument('-g',
                        '--groupby',
                        type=str,
                        default='column',
                        help='Table is grouped by this axis of CSV.')
    return vars(parser.parse_args())


def main():
    """
    Main routine
    """
    cliargs = cli()
    term_translate = {}
    if cliargs.get('gobo'):
        term_translate = obo_equiv(cliargs['gobo'])
    table = logit_table(
        cliargs['filename'],
        translator=term_translate,
        **{
            key: val
            for key, val in cliargs.items()
            if key not in ['filename', 'org', 'out_file', 'gobo']
        })
    out_file = cliargs.get(
        'out_file',
        cliargs['filename'].parent / (cliargs['filename'].stem + '_table.csv'))
    if cliargs['org']:
        out_file = out_file.with_suffix('.org')
        org_table = table.reset_index()
        org_table.columns = [*table.index.names, cliargs['score_name']]
        org_fmt = tabulate(org_table,
                           tablefmt='orgtbl',
                           showindex=False,
                           headers='keys')
        out_file.write_text(org_fmt)
    else:
        table.to_csv(out_file)
    return 0


if __name__ == '__main__':
    sys.exit(main())
