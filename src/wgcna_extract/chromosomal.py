#!/usr/bin/env python3
# -*- coding: utf-8; mode:python; -*-
#
# Copyright © 2022-2023 Pradyumna Paranjape
# This file is part of WGCNA_Extract.
#
# WGCNA_Extract is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# WGCNA_Extract is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FIT FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with WGCNA_Extract.  If not, see <https://www.gnu.org/licenses/>.
#
"""
Convert generic features into contingency tables.

- Features:
    - Chromosome: Modules, Chromosome Number
    - Poly-Cisrton: Modules, Poly-Cistron (direction of genome read)
    - i-ord: Modules, Ord in order starting from 'i+1'
    - j-ord: Modules, Ord in order starting from 'j-1'
    - i-start: start(gene) - start(poly-cistron)
      - i-start is continuous random variable, globally viewed
                   # poly-cistrons: len(poly-cistron) > i
        Pr(i) = --------------------------------------------
                      # (poly-cistronic nucleotides)

      (Future:) Calculate ks statistic for observation of 'i' for each module
      discretize i-start in 2000 bp bins and use fisher-exact

- Determine mutual information between modules and:
    - chromosome
    - poly-cistron
    - i-ord
    - j-ord

- From the contingency table, use hypergeometric distribution
  to qualify each contingency-column using `p-value`.

- Filter features that pass `p-value` two-tailed.
  This states that the feature is `enriched` or `depleted` significantly.
"""

from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
from os import sched_getaffinity
from pathlib import Path
from typing import Dict

import numpy as np
import pandas as pd

from wgcna_extract.contingency import contingency_mxn_unique
from wgcna_extract.feature_stats import analyse_skew
from wgcna_extract.parsers import (GffRecord, GtfRecord, GxfRecord,
                                   parse_modules)
from wgcna_extract.plots import plot_enumerate, plot_logit

NPROC = (len(sched_getaffinity(0)) - 1) or 1
"""Available processors - 1"""


def cli():
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter,
                            description='''
    Description:

    Binomial p-values for:
    k: Occurrence of motif in RNA-sequences
    ''')
    parser.add_argument('-g',
                        '--gxf',
                        type=Path,
                        default=Path('./features.gff'),
                        help='Gene (or transcripts) features file')
    parser.add_argument('-p',
                        '--p-value',
                        type=float,
                        default=1e-3,
                        help='p-value cut-off for binomial test.')
    parser.add_argument('-o',
                        '--out-dir',
                        type=Path,
                        default=Path('enrichments'),
                        nargs='?',
                        help='base directory for output files.')
    parser.add_argument('base-dir',
                        type=Path,
                        default=Path('.'),
                        nargs='?',
                        help='base directory containing clusters (Cytoscape).')

    return vars(parser.parse_args())


def legend_unique(ax, *args, **kwargs):
    """Place unique legends"""
    handles, labels = ax.get_legend_handles_labels()
    unique = [(hand, lab) for i, (hand, lab) in enumerate(zip(handles, labels))
              if lab not in labels[:i]]
    ax.legend(*zip(*unique), *args, **kwargs)


def extract_genetic_info(gxf: GxfRecord) -> pd.DataFrame:
    """
    Extract following information from records:
        - Chromosome
        - Poly-Cistron
        - i-ord: transcript-ord on Poly-Cistron from
            the side of transcription initiation
        - j-ord: transcript-ord on Poly-Cistron from
            the side of `J` base nucleotide
        - i-rel-loc: relative location from initialization of transcription

    Args:
        gxf: parsed gxf record
    """
    attr = gxf.attributes
    gxfinfo = pd.DataFrame(index=attr.index,
                           columns=pd.Index(('chromosome', 'poly-cistron',
                                             'i-ord', 'j-ord', 'i-start')))

    # initialize
    cis_num: int = 0
    i_ord: int = 0
    current_ori: str = '.'
    current_chr: str = ''
    cis_start: int = 0

    # determine poly-cistron, i-ord (signed)
    # Following is a `sequential` mapping function
    for _idx, orient in attr['strand'].items():
        idx = str(_idx)  # affirm string
        if attr.loc[idx, 'seqid'] != current_chr:
            # Chromosome changed
            cis_num = 0
            cis_start = int(attr.loc[idx, 'start'])  # type: ignore [arg-type]
            current_ori = orient
            current_chr = str(attr.loc[idx, 'seqid'])
            i_ord = {'-': -1, '+': 0}[orient]
            i_start = 0
        elif orient != current_ori:
            # Orientation has changed
            cis_num += 1
            cis_start = int(attr.loc[idx, 'start'])  # type: ignore [arg-type]
            current_ori = orient
            i_ord = {'-': -1, '+': 0}[orient]
            i_start = 0
        else:
            # Simply, the next gene in same poly-cistron
            i_ord += {'-': -1, '+': 1}[orient]
            i_start = {
                '-': -1,
                '+': 1
            }[orient] * int(
                attr.loc[idx, 'start']) - cis_start  # type: ignore [arg-type]
        gxfinfo.loc[idx, 'poly-cistron'] = cis_num
        gxfinfo.loc[idx, 'i-ord'] = i_ord
        gxfinfo.loc[idx, 'i-start'] = i_start
        gxfinfo.loc[idx, 'chromosome'] = str(
            int(str(idx).split('.')[0].split('_')[-1]))

    # Elaborate poly-cistron name
    gxfinfo['poly-cistron'] += 1
    gxfinfo['poly-cistron'] = gxfinfo['poly-cistron'].apply(
        lambda x: f'.{int(x):0>2}')
    gxfinfo['poly-cistron'] = gxfinfo['chromosome'] + gxfinfo['poly-cistron']

    len_cis: pd.Series = pd.Series(index=gxfinfo.index, dtype=np.int_)
    true_init: pd.Series = pd.Series(index=gxfinfo.index, dtype=np.int_)

    for cis_id in gxfinfo['poly-cistron'].unique():
        cis_idx = gxfinfo.index[gxfinfo['poly-cistron'] == cis_id]
        len_cis[cis_idx] = len(cis_idx)
        true_init[cis_idx] = np.min(gxfinfo.loc[cis_idx, 'i-start'])

    # rectify i-ord, rel-loc, derive j-ord
    gxfinfo['i-ord'] %= len_cis
    gxfinfo['j-ord'] = gxfinfo['i-ord'] - len_cis

    gxfinfo['i-ord'] += 1  # human indexing
    gxfinfo['i-ord'] = pd.to_numeric(gxfinfo['i-ord'], downcast='integer')
    gxfinfo['j-ord'] = pd.to_numeric(gxfinfo['j-ord'], downcast='integer')
    gxfinfo['i-start'] = pd.to_numeric(gxfinfo['i-start'], downcast='integer')
    # gxfinfo['i-ord'] = gxfinfo['i-ord'].apply(lambda x: int(x))
    # gxfinfo['j-ord'] = gxfinfo['j-ord'].apply(lambda x: int(x))
    gxfinfo['i-start'] -= true_init
    gxfinfo['i-start'] = gxfinfo['i-start'].apply(lambda x: int(
        (x // 2000) + 1))

    # Filter index for transcripts only
    gxfinfo = gxfinfo.loc[gxfinfo.index.str.contains(r'\w+_\d+\.T?\d+',
                                                     regex=True)]
    return gxfinfo.map(lambda x: str(x))  # type: ignore [operator]


def analyse(gxf_info: pd.DataFrame, module_labels: Dict[str, str],
            feature: str, alpha: float, out_dir: Path, **kwargs):
    print(f'{feature}:')
    cat1 = gxf_info[feature].to_dict()
    cont = contingency_mxn_unique(cat_1=cat1,
                                  cat_2=module_labels,
                                  cat_1_name=feature,
                                  cat_2_name='module')
    cont.to_csv(out_dir / f'sheets/{feature}_contingency.csv')
    chi2_res = analyse_skew(cont, alpha)
    chi2_score, pfish, logit = chi2_res['chi2_score'], chi2_res[
        'pfish'], chi2_res['logit']

    if kwargs.get('sort', False):
        logit = logit.reindex(
            pd.Index(sorted(logit.index.to_list(), key=lambda x: float(x))))

    if not pfish.empty:
        pfish.to_csv(out_dir / f'sheets/{feature}_pfisher.csv')
    if not logit.empty:
        logit.to_csv(out_dir / f'sheets/{feature}_logit.csv')
        kwargs['force_resolution'] = kwargs.get('force_resolution', True)
        kwargs['xlabels'] = kwargs.get('xlabels', 'feature')
        plot_logit(logit, feature, out_dir, **kwargs)
        plot_enumerate(logit, out_dir, f'No. of {feature} skewed')
    return {'statistic': chi2_score.statistic, 'p-value': chi2_score.pvalue}


def main():
    """Main routine wrap"""
    cliargs = cli()
    out_dir = cliargs['base-dir'] / cliargs['out_dir']
    p_value = cliargs['p_value']
    out_dir.mkdir(parents=True, exist_ok=True)
    (out_dir / 'sheets').mkdir(parents=True, exist_ok=True)
    (out_dir / 'images').mkdir(parents=True, exist_ok=True)

    feature_file: Path = cliargs['gxf']
    module_labels = parse_modules(cliargs['base-dir'])
    feature_parser = {'.gtf': GtfRecord}.get(feature_file.suffix, GffRecord)
    gxf: GxfRecord = feature_parser(feature_file)

    gxf_info = extract_genetic_info(gxf)

    _dependency = {}
    # calculate information content
    observables = {
        'chromosome': ('No.', None),
        'i-ord': ('No.', 0.0833),
        'j-ord': ('No.', 0.0833),
        'poly-cistron': ('No.', 0.25),
        'i-start': ('x 2000', 0.0833)
    }
    for feature, (units, resolution) in observables.items():
        _dependency[feature] = analyse(gxf_info,
                                       module_labels,
                                       feature,
                                       p_value,
                                       out_dir,
                                       units=units,
                                       sort=True,
                                       resolution=resolution)

    dependency = pd.DataFrame(_dependency).T
    dependency.to_csv(out_dir / 'sheets/dependence.csv')


if __name__ == "__main__":
    main()
