#######
USAGE
#######

**********
SYNOPSIS
**********

.. argparse::
   :ref: wgcna_extract.command_line._cli
   :prog: wgcna_extract

**************
Instructions
**************

User configuration
====================

- Create a configuration file as directed `here <configure.html>`__.
