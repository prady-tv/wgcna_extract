#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-

# Copyright (c) Pradyumna Swanand Paranjape <pradyparanjpe@rediffmail.com>
#
# This file is part of wgcna_extract.
#
# wgcna_extract is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# wgcna_extract is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with wgcna_extract. If not, see <https://www.gnu.org/licenses/>.
#
"""
Calculate Silhouette score and coefficients for clustering

Calculate silhoulette score, calinski harabasz score, davies bouldin score,
silhouette coefficients for WGCNA modules in given directory.

Plot silhouette sample coefficients.
"""

import sys
from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser, Namespace
from pathlib import Path
from typing import Iterable, Optional

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.metrics import (calinski_harabasz_score, davies_bouldin_score,
                             silhouette_samples, silhouette_score)


def _r_path(path: str) -> Path:
    "Resolved Path corresponding to path"
    return Path(path).resolve()


def cli() -> Namespace:
    """Command line"""
    description = '''
    Silhouette - Calculate and plot cluster silhouette coefficients and score
    '''
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter,
                            description=description)
    parser.add_argument('-v',
                        '--verbose',
                        action='count',
                        help='output verbosity')
    parser.add_argument('-d',
                        '--import-dir',
                        type=_r_path,
                        default='.',
                        help='directory from where, node-files are imported')
    parser.add_argument('-b',
                        '--base-name',
                        type=str,
                        default='CytoscapeInput-nodes-*.tsv',
                        help='Base name glob of imports. "*" = labels')
    parser.add_argument('--label-delim',
                        type=str,
                        default='\t',
                        help='delimiter for cytoscape-imports (default: tab)')
    parser.add_argument('--adj-delim',
                        type=str,
                        default=',',
                        help='delimiter for adjacency. !pass tab as \'\\t\'')
    parser.add_argument('-x',
                        '--exclude',
                        type=str,
                        nargs='*',
                        default=['gray', 'grey'],
                        help='exclude labels')
    parser.add_argument('--metric',
                        type=str,
                        default='euclidean',
                        help='silhouette score metric')
    parser.add_argument('--is-dist',
                        action='store_true',
                        help='''
                        Given adjacency is actually distance.
                        Overwrites metric -> precomputed
                        ''')
    parser.add_argument('--adj-labels',
                        type=_r_path,
                        help='Path to martix whose labels should be used.')

    parser.add_argument('adjacency',
                        type=_r_path,
                        help='Path to adjacency matrix.')
    args = parser.parse_args()
    if args.is_dist:
        args.metric = 'precomputed'
    return args


def extract_labels(import_dir: Path,
                   baseglob: str = 'CytoscapeInput-edges-*.tsv',
                   delim: str = ',',
                   exclude: Optional[Iterable[str]] = None,
                   verbose: bool = False) -> pd.Series:
    """
    Locate files and extract labels for handles

    Args:
        import_dir: directory in which, all cytoscape imports are located.
        baseglob: base name glob of imported `*nodes*` files.
            [ 'base-name-*-structure.ext', where, * discovers labels ]
        delim: delimiter
        exclude: exclude samples classified in these labels
        verbose: print verbose story

    Returns:
        Seires of transcript-handles and corresponding labels.
    """
    labels = pd.Series(dtype="string")
    if verbose:
        print(f'Reading {baseglob} from directory {str(import_dir)}')
    for labfile in import_dir.glob(baseglob):
        # the third column is expected to be the label
        if verbose:
            print(f'Reading {labfile.name}')
        labels = pd.concat(
            (labels, pd.read_csv(labfile, index_col=0,
                                 delimiter=delim).iloc[:, 1]))
    if verbose:
        print(f'Dropping exclusions')
    if exclude:
        labels = labels[~labels.isin(exclude)]
    return labels


def silhouette(adjacency: pd.DataFrame,
               labels: pd.Series,
               s_avg: Optional[float] = None,
               metric: str = 'euclidean',
               verbose: bool = False):
    """
    Calculate and plot silhouette coefficients and scores


    Args:
        adjacency: adjacency matrix
        labels: node labels
        s_avg: calculated silhouette score
        metric: metric for scoring
        verbose: verbosity

    Adapted from:
        `sklearn <https://scikit-learn.org/stable/auto_examples/cluster/\
plot_kmeans_silhouette_analysis.html>`__
    """
    samp_sil_val = silhouette_samples(adjacency, labels, metric=metric)
    if verbose:
        print(f'type of samp_sil_val: {type(samp_sil_val)}')
        print(f'sample_values: {samp_sil_val.size}')

    y_lower = 10
    n_clusters = labels.nunique()
    if verbose:
        print(f'{n_clusters=}')

    clust_num = 0
    _, ax = plt.subplots()
    for lab in labels.unique():
        clust_num += 1
        lab_sil_val = samp_sil_val[labels == lab]

        lab_sil_val.sort()

        lab_size = lab_sil_val.shape[0]
        y_upper = y_lower + lab_size

        color = lab.replace("grey60", "grey")
        ax.fill_betweenx(
            np.arange(y_lower, y_upper),
            0,
            lab_sil_val,
            facecolor=color,
            edgecolor=color,
            alpha=0.7,
        )

        # Label the silhouette plots with their cluster numbers at the middle
        # ax.text(-0.05, y_lower + 0.5 * lab_size, lab)

        # Compute the new y_lower for next plot
        y_lower = y_upper + 10  # 10 for the 0 samples

    ax.set_title('Silhouette plot for modules')
    ax.set_xlabel('Silhouette coefficient')
    ax.set_ylabel('Clusters (no order)')

    # The vertical line for average silhouette score of all the values
    if s_avg is None:
        s_avg = silhouette_score(
            silhouette_score(adjacency, labels, metric=metric))

    ax.axvline(x=s_avg, color='gray', linestyle='--')
    ax.axvline(x=0, color='gray', linestyle=':')

    ax.set_yticks([])  # Clear the yaxis labels / ticks

    plt.savefig("sample_silhouettes.png")


def main():
    """main routine"""
    cliargs = cli()
    if cliargs.verbose:
        print('Command arguments:')
        print(vars(cliargs))

    if cliargs.verbose:
        print('reading adjacency')
    adjacency = pd.read_csv(cliargs.adjacency,
                            index_col=0,
                            delimiter=cliargs.adj_delim)
    """Adjacency matrix"""

    # label adjacency
    if cliargs.adj_labels:
        adj_labels_df = pd.read_csv(cliargs.adj_labels,
                                    index_col=0,
                                    delimiter=cliargs.adj_delim)
        adjacency.index = adj_labels_df.index
        adjacency.columns = adj_labels_df.columns

    if cliargs.verbose:
        print('Adjacency:')
        print(adjacency.head())
        print(adjacency.tail())
        print(adjacency.describe())

    labels = extract_labels(cliargs.import_dir,
                            baseglob=cliargs.base_name,
                            delim=cliargs.label_delim,
                            exclude=cliargs.exclude,
                            verbose=cliargs.verbose)
    """Node cluster labels"""
    if cliargs.verbose:
        print('Labels:')
        print(labels.head())
        print(labels.tail())
        print(labels.describe())

    # drop missing
    common_idx = adjacency.index.intersection(labels.index)
    adj_drop = adjacency.index.difference(common_idx)

    if cliargs.verbose:
        print('Dropping genes:')
        print(adj_drop)
        print('Common index:')
        print(common_idx)

    labels = labels.drop(labels.index.difference(common_idx))
    adjacency = adjacency.drop(adj_drop, axis=0).drop(adj_drop, axis=1)

    # calculate scores
    if cliargs.verbose:
        print('Calculating clustering quality scores:')
    if not cliargs.is_dist:
        db_score = davies_bouldin_score(adjacency, labels)
        """davies bouldin score"""

        ch_score = calinski_harabasz_score(adjacency, labels)
        """Calinski Harabasz score"""

        print('Scores:')
        print(f'Calinski Harabasz: {ch_score}, Davies Bouldin: {db_score}')

    s_score = silhouette_score(adjacency, labels, metric=cliargs.metric)
    """Silhouette score"""
    print(f'Silhouette: {s_score}')
    silhouette(adjacency,
               labels,
               s_avg=s_score,
               metric=cliargs.metric,
               verbose=cliargs.verbose)
    return 0


if __name__ == '__main__':
    sys.exit(main())
