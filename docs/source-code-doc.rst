###############
SOURCE CODE DOC
###############

*************
Entry Points
*************

Motifs
=============

Known `CISBP <http://cisbp.ccbr.utoronto.ca/>`__ motifs

Map motifs to features
-----------------------

.. automodule:: wgcna_extract.motif_map
   :members:

Calculate statistics for mapped motifs
---------------------------------------

.. automodule:: wgcna_extract.motif_stats
   :members:

=============================================================================

Genetic Analysis
====================

Are any of the genetic features associated with regulatory modules?

Reannotate GTF
-----------------

.. automodule:: wgcna_extract.gtf_reannotate
   :members:

=============================================================================


**************
Structure
**************

Output modules
==============

plots
---------------------------------

.. automodule:: wgcna_extract.plots
   :members:

Statistical Calculations
========================

.. automodule:: wgcna_extract.feature_stats
   :members:

Common modifications
======================

Shared files
