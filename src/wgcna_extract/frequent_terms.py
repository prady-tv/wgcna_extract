#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-

from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
from pathlib import Path
from typing import Any, Dict, Iterable, Optional, Set, Tuple

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

UNINFORMATIVE = [
    '',
    '.',
    'a.k.a.',
    'and',
    'associated',
    'beta',
    'binding',
    'complex',
    'conserved',
    'containing',
    'domain',
    'factor',
    'family',
    'function',
    'hypothetical',
    # 'hydrolase',
    'in',
    # 'kinase',
    'like',
    'member',
    'nan',
    'of',
    'or',
    # 'peptidase',
    # 'phosphatase',
    'protein',
    'putative',
    'related',
    'repeat',
    'repeats',
    'rich',
    'subfamily',
    'subunit',
    'superfamily',
    'type',
    'uncharacterised',
    'uncharacterized',
    'unknown',
]


def _fp_to_farb(fp: Path) -> str:
    return str(fp).split('-')[-1].split('.')[0]


def module_terms(base_dir: Path) -> Dict[str, pd.Series]:
    """
    Create a Module: gene_ids dict

    Args:
        base_dir: directory containing *-nodes-*.tsv records

    Returns:
        Dict with list of corresponding gene_ids for each module
    """
    terms_dict: Dict[str, pd.Series] = {}
    for nodefile in base_dir.glob('*-nodes-*.tsv'):
        nodes_records = pd.DataFrame(pd.read_csv(nodefile,
                                                 delimiter='\t',
                                                 index_col=0),
                                     dtype=str)
        terms_dict[_fp_to_farb(nodefile)] = nodes_records['altName']

    return terms_dict


def cli() -> Dict[str, Any]:
    """Parse command line"""
    parser = ArgumentParser(
        formatter_class=ArgumentDefaultsHelpFormatter,
        description='''Count frequent terms from modules.''')
    parser.add_argument('base-dir',
                        default='.',
                        type=Path,
                        help='Analyse files in this directory.')
    parser.add_argument('--out-dir',
                        default='term_frequencies',
                        type=Path,
                        help='Directory created inside BASE-DIR')
    parser.add_argument('--ignore',
                        nargs='*',
                        type=str,
                        help='Terms to ignore, they are not informative.',
                        default=UNINFORMATIVE)
    parser.add_argument('--term-sep',
                        type=str,
                        nargs='+',
                        help='Terms are separated by',
                        default=('_', '-', '/', ' '))
    parser.add_argument('--min-freq',
                        type=float,
                        help='minimum frequency of terms for overview',
                        default=(1 / 7))
    parser.add_argument('--num',
                        type=int,
                        help='number of most frequent terms',
                        default=10)
    return vars(parser.parse_args())


class AnnotCounter():
    """
    Annotation Counter, inspired by `:pyclass:collections.Counter`

    Args:
        annotations: Series: index feature id, Value: annotation name
        uninformative: Iterable of terms that are not informative
        sep: terms are delimited in annotation name by these separators
    """

    def __init__(self,
                 annotations: pd.Series,
                 uninformative: Iterable[str] = ('protein', 'like',
                                                 'putative'),
                 sep: Iterable[str] = (' ', '_', '-', '/')):
        self.annotations = pd.Series(annotations, dtype=str)
        """Annotations Series"""
        self.uninformative = uninformative
        """Uninformative terms"""
        self.sep = sep
        """Term delimiters"""
        self._annotated_feats = None
        self._annot_dict: Dict[str, Tuple[str, ...]] = {}
        self._all_terms: Set[str] = set()
        self._feat_freq: Dict[str, int] = {}

    @property
    def annot_dict(self) -> Dict[str, Tuple[str, ...]]:
        """
        Dictionary associating each index (feature) from annotations
        with all informative terms in its annotations
        """
        if not self._annot_dict:
            for feat, annot in self.annotations.to_dict().items():
                annot_terms = [annot.lower()]
                for t_sep in self.sep:
                    annot_terms = [
                        s_terms for term in annot_terms
                        for s_terms in term.split(t_sep)
                        if s_terms not in self.uninformative
                    ]
                self._annot_dict[feat] = tuple(annot_terms)
        return self._annot_dict

    @property
    def all_terms(self):
        """All 'informative' terms that occur in the complete feature set"""
        if not self._all_terms:
            self._all_terms = set(
                [val for name in self.annot_dict.values() for val in name])
        return self._all_terms

    @property
    def feat_num(self) -> Dict[str, int]:
        """
        Dict associating each term to the frequency with which
        its feature appears in the complete feature set.
        """
        if not self._feat_freq:
            for term in self.all_terms:
                self._feat_freq[term] = len([
                    feat for feat, annot_terms in self.annot_dict.items()
                    if term in annot_terms
                ])
            self._feat_freq = dict(
                sorted(self._feat_freq.items(),
                       key=lambda x: x[1],
                       reverse=True))
        return self._feat_freq

    @property
    def annotated_feats(self) -> int:
        """
        Number of features that are annotated.

        A feature is annotated if its annotation value does not contain
        any of following terms.

        Unannotation terms:
            - hypothetical
            - unknown
            - uncharacterized
            - uncharacterised
        """
        if self._annotated_feats is None:
            self._annotated_feats = len([
                feat for feat, name in self.annotations.to_dict().items()
                if not any((unchr in name
                            for unchr in ('unknown', 'uncharacterized',
                                          'uncharacterised', 'hypothetical')))
            ])
        return self._annotated_feats

    @property
    def informative_feats(self) -> int:
        """Features that contain terms other than uninformative terms"""
        return len([
            feat for feat, terms in self.annotations.to_dict().items()
            if not any((unchar in terms
                        for unchar in ('unknown', 'uncharacteri',
                                       'hypothetical')))
        ])

    def most_common(self, num: int = 0) -> Dict[str, int]:
        """
        Most commonly occurring feature terms and their counts.

        Args:
            num: list top ``num`` feature terms only (0 => all)
        """
        if num == 0:
            num = len(self._feat_freq)
        return dict(list(self.feat_num.items())[:num])

    def least_common(self, num: int = 0) -> Dict[str, int]:
        """
        Least commonly occurring feature terms and their counts.

        Args:
            num: list bottom ``num`` feature terms only (0 => all)
        """
        if num == 0:
            num = len(self._feat_freq)
        return dict(list(reversed(self.feat_num.items()))[:num])

    def most_freq(self, num: int = 0):
        """
        Most commonly occurring feature terms: their frequency (of annotated).

        Args:
            num: list top ``num`` feature terms only (0 => all)
        """
        return {
            term: count / self.annotated_feats
            for term, count in self.most_common(num).items()
        }

    def least_freq(self, num: int = 0):
        """
        Least commonly occurring feature terms: their frequency (of annotated).

        Args:
            num: list top ``num`` feature terms only (0 => all)
        """
        return {
            term: count / self.annotated_feats
            for term, count in self.least_common(num).items()
        }

    def min_freq(self, gt: float = 0.):
        """
        Terms occurring with greater frequency than a given value.

        Args:
            gt: frequency greater than
        """
        if gt > 1:
            return {}
        all_terms = self.most_freq()
        if gt <= 0:
            return all_terms
        return {term: freq for term, freq in all_terms.items() if freq > gt}


def heat_df(df: pd.DataFrame, filename: Path, min_freq: float):
    _, ax = plt.subplots()
    hmap = ax.imshow(df, cmap='copper')
    plt.colorbar(mappable=hmap)
    ax.set_xticks(range(df.shape[1]), df.columns, rotation=45)
    ax.set_yticks(range(df.shape[0]), df.index)
    ax.set_xlabel('module')
    ax.set_ylabel('term')
    ax.set_title(f'Terms with occurrence > {min_freq} in the module.')
    plt.savefig(filename, dpi=700)


def count_freq(
    annot_ser: pd.Series,
    n_best: int = 10,
    min_freq: float = 1 / 6,
    ignore: Optional[Iterable[str]] = None,
    term_sep: Iterable[str] = ('_', '/', ' ', '-')
) -> Tuple[pd.DataFrame, pd.Series, pd.Series]:
    """
    Find most frequent annot_ser from list

    Args:
        annot_ser: Annotations
        n_best: pick best n terms
        min_freq: filter terms with occurrence frequency greater than
        ignore: Terms to ignore (such as `protein`, `putative`)
        term_sep: Annotation terms delimiters

    Returns:
        Dataframe: picked terms: number frequency
        Series: total terms, annotated (number), annotated (fraction of total)
        Series: best_terms: frequency (> minimum)

    """
    ignore = ignore or ()
    """All annot_ser pooled together"""
    annot_counter = AnnotCounter(annot_ser, ignore, term_sep)
    count_ser = pd.Series(annot_counter.most_common(n_best), dtype=np.int_)
    freq_ser = pd.Series(annot_counter.most_freq(n_best), dtype=np.float_)
    filt_ser = pd.Series(annot_counter.min_freq(min_freq), dtype=np.float_)
    count_frame = pd.concat((count_ser, freq_ser), axis=1)
    count_frame.rename(columns={0: 'Num', 1: 'Freq'}, inplace=True)
    count_frame.loc['total'] = [annot_counter.annotated_feats, 1]
    num_nodes = len(annot_ser)
    annotated_ser = pd.Series({
        'total':
        num_nodes,
        'annotated_num':
        annot_counter.annotated_feats,
        'annotated_frac':
        annot_counter.annotated_feats / num_nodes,
        'informative_num':
        annot_counter.informative_feats,
        'informative_frac':
        annot_counter.informative_feats / num_nodes,
    })
    count_frame.index.name = 'Term'
    return count_frame, annotated_ser, filt_ser


def main():
    """Main routine"""
    cliargs = cli()
    freq = {}
    annotated_stats = pd.DataFrame(columns=[
        'total',
        'annotated_num',
        'annotated_frac',
        'informative_num',
        'informative_frac',
    ])
    out_dir = cliargs['base-dir'] / cliargs['out_dir']
    out_dir.mkdir(parents=True, exist_ok=True)
    filt_dict = {}
    for mod, annot_ser in module_terms(cliargs['base-dir']).items():
        freq_frame, annot_ser, filt_ser = count_freq(annot_ser, cliargs['num'],
                                                     cliargs['min_freq'],
                                                     cliargs['ignore'],
                                                     cliargs['term_sep'])
        annotated_stats.loc[mod] = annot_ser
        freq[mod] = freq_frame
        freq_frame.to_csv(out_dir / (mod + '.tsv'),
                          sep='\t',
                          float_format='%.4f')
        filt_dict[mod] = filt_ser
    annotated_stats.loc['leishmania'] = annotated_stats.sum(axis=0)
    annotated_stats.loc['leishmania', 'annotated_frac'] = annotated_stats.loc[
        'leishmania', 'annotated_num'] / annotated_stats.loc['leishmania',
                                                             'total']
    annotated_stats['total'] = annotated_stats['total'].apply(lambda x: int(x))
    annotated_stats['annotated_num'] = annotated_stats['annotated_num'].apply(
        lambda x: int(x))
    annotated_stats.loc[
        'leishmania', 'informative_frac'] = annotated_stats.loc[
            'leishmania',
            'informative_num'] / annotated_stats.loc['leishmania', 'total']
    annotated_stats.sort_values('informative_frac',
                                axis=0,
                                inplace=True,
                                ascending=False)
    annotated_stats['informative_num'] = annotated_stats[
        'informative_num'].apply(lambda x: int(x))
    annotated_stats.index.name = 'module'
    annotated_stats.to_csv(out_dir / 'annotation_stats.csv',
                           float_format='%.4f')
    filt_df = pd.DataFrame(filt_dict)
    filt_df.dropna(axis=1, how='all', inplace=True)
    filt_df.fillna(0, inplace=True)
    s = filt_df.sum(axis=1)
    filt_df = filt_df.loc[s.sort_values(ascending=False).index]
    s = filt_df.sum()
    filt_df = filt_df[s.sort_values(ascending=False).index]
    filt_file = out_dir / 'filtered_frequent_terms.csv'
    filt_df.to_csv(filt_file, float_format='%.4f')
    heat_df(filt_df, filt_file.with_suffix('.png'), cliargs['min_freq'])


if __name__ == '__main__':
    main()
