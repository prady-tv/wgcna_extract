#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-

# Copyright (c) Pradyumna Swanand Paranjape <pradyparanjpe@rediffmail.com>
#
# This file is part of wgcna_extract.
#
# wgcna_extract is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# wgcna_extract is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with wgcna_extract. If not, see <https://www.gnu.org/licenses/>.
#

import sys
from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
from pathlib import Path
from typing import Any, Dict, Iterable

import numpy as np
import pandas as pd
from psprint import print

from wgcna_extract.binom_enrich import (QualCounter, binom_test_array,
                                        module_terms, plot_excesses)
from wgcna_extract.enriched_go import GffThesaurus, GtfThesaurus


def cli() -> Dict[str, Any]:
    """Parse command line"""
    parser = ArgumentParser(
        formatter_class=ArgumentDefaultsHelpFormatter,
        description='''Count enriched GO terms from modules.''')
    parser.add_argument('base-dir',
                        nargs='?',
                        default='.',
                        type=Path,
                        help='Analyse files in this directory.')
    parser.add_argument('--gff', type=Path, help='gff file')
    parser.add_argument('--gtf', type=Path, help='gtf file')
    parser.add_argument('--dedata', type=Path, help='DExp files', nargs='+')
    parser.add_argument('--out-dir',
                        default='enrichments',
                        type=Path,
                        help='Directory created inside BASE-DIR')
    parser.add_argument('--p-value',
                        type=float,
                        help='Enrichment p-value cut off (p < P_VALUE)',
                        default=1e-3)
    return vars(parser.parse_args())


def parse_dedata(defiles: Iterable[Path], go_equal: Dict[str, str]):
    """
    Args:
        defiles: all files with de expression data series
    """
    all_data = {}
    for regul in defiles:
        dedata: pd.Series = pd.DataFrame(
            pd.read_csv(  # type: ignore
                regul, index_col=0))['regulation']
        for tid, regul in dedata.items():
            rid = go_equal.get(str(tid).replace('.', '_').upper())
            if rid is None:
                continue
            if tid not in all_data:
                all_data[rid] = []
            all_data[rid].append(regul)
    all_data = {rid: tuple(quals) for rid, quals in all_data.items()}
    all_regul = pd.Series(all_data, name='diff_regul')
    return pd.DataFrame(all_regul)


class PWCounter(QualCounter):
    pass


def main() -> int:
    """Main routine"""
    cliargs = cli()
    out_dir = cliargs['base-dir'] / cliargs['out_dir']
    out_dir.mkdir(parents=True, exist_ok=True)
    (out_dir / 'sheets').mkdir(parents=True, exist_ok=True)
    (out_dir / 'images').mkdir(parents=True, exist_ok=True)
    gff_thes = GffThesaurus(cliargs['gff'])
    gtf_thes = GtfThesaurus(cliargs['gtf'])
    go_equal = {
        gff_thes.translate(gtf_thes.translate(gid)): gid
        for gid in gtf_thes.attributes.index
    }
    go_equal = {
        key.replace('.', '_').upper(): val
        for key, val in go_equal.items() if key is not None
    }
    all_quals = parse_dedata(cliargs['dedata'], go_equal)
    genome_counter = PWCounter(all_quals)
    prob: Dict[str, pd.Series] = {
        qual: freq['frac']
        for qual, freq in genome_counter.term_feat_freq.items()
    }
    excess: Dict[str, Dict[str, pd.Series]] = {}
    for mod, go_quals in module_terms(all_quals, cliargs['base-dir']).items():
        print(f'Calculating enrichments for module {mod}')
        mod_counter = PWCounter(go_quals)
        for qual in mod_counter.qualifiers:
            occur: pd.Series = mod_counter.term_feat_freq[qual]['count']
            occur = occur.reindex(prob[qual].index)
            occur.fillna(0, inplace=True)
            expect = prob[qual] * mod_counter.num_feat
            # p_val = poisson_p_value(occur, expect)
            # p_val = binom_p_value(occur, mod_counter.num_feat, prob)
            p_val = binom_test_array(occur, mod_counter.num_feat, prob[qual])
            p_val.index.name = qual
            p_series: pd.Series = -np.log10(p_val[(p_val < cliargs['p_value'])
                                                  & (occur > expect)])
            p_series = pd.concat((p_series,
                                  np.log10(p_val[(p_val < cliargs['p_value'])
                                                 & (occur < expect)])))
            p_series.sort_values(inplace=True, ascending=False)
            p_series.name = f'-log(p-value) for {qual}'
            if len(p_series):
                (out_dir / 'sheets' / mod).mkdir(parents=True, exist_ok=True)
                (out_dir / 'images' / mod).mkdir(parents=True, exist_ok=True)
                if qual not in excess:
                    excess[qual] = {}
                excess[qual][mod] = p_series
                p_series.to_csv(
                    (out_dir / 'sheets' / mod / qual).with_suffix('.csv'))

    plot_excesses(excess, out_dir / 'images', cliargs['p_value'])
    for qual, freq in prob.items():
        sorted_freq = freq.sort_values(ascending=False)
        assert sorted_freq is not None
        sorted_freq.index.name = 'term'
        sorted_freq.to_csv(out_dir / 'sheets' / f'{qual}_frequency.csv')
    return 0


if __name__ == '__main__':
    sys.exit(main())
