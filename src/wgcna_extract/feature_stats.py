#!/usr/bin/env python3
# -*- coding: utf-8; mode:python; -*-
#
# Copyright © 2022-2023 Pradyumna Paranjape
# This file is part of WGCNA_Extract.
#
# WGCNA_Extract is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# WGCNA_Extract is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FIT FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with WGCNA_Extract.  If not, see <https://www.gnu.org/licenses/>.
#
"""Statistical organization and tests."""

from multiprocessing import Pool
from os import sched_getaffinity
from typing import Dict, Literal, Tuple, Union

import numpy as np
import pandas as pd
from scipy.stats import chi2_contingency
from scipy.stats.contingency import odds_ratio
from scipy.stats.stats import fisher_exact

from wgcna_extract.contingency import contingency_2x2

NPROC = (len(sched_getaffinity(0)) - 1) or 1
"""Available parallel processors (spare 1)."""


def cont_mapper(args) -> Tuple[str, str, float, float]:
    """
    Parallel worker to map on contingency.

    Parameters
    ----------
    args: tuple
        idx : str
            index handle
        col : str
            column handle
        df : pd.DataFrame
            reference to pandas DataFrame

    Returns
    -------
    tuple
        idx : str
            index handle as received
        col : str
            column handle as received
        pvalue : float
            fisher-exact p-value
        logit : float
            log odds (bits) of occurrence / expectation
    """
    idx, col, df = args
    cont_2x2 = contingency_2x2(df, idx, col)
    fish_calc = fisher_exact(cont_2x2)
    return idx, col, fish_calc.pvalue, np.log2(odds_ratio(cont_2x2).statistic)


def analyse_skew(
    cont: pd.DataFrame, alpha: float
) -> Dict[Literal['chi2_score', 'pfish', 'logit'], Union[
        'Chi2ContingencyResult', pd.DataFrame]]:
    """
    Analyse for unequal partitioning of feature in modules.

    Parameters
    ----------
    cont : pd.DataFrame
        mxn contingency
    alpha : float
        significance-level

    Returns
    -------
    Dict[Literal['chi2_score', 'pfish', 'logit'], Union[float, pd.DataFrame]]
        chi2_score : float
            "cat1 independent of cat2"
        pfish : pd.DataFrame
            Fisher exact p-value
        logit : pd.DataFrame
            log odds (bits) of occurrence / expectation
    """
    # Drop rows and columns that have all 0s
    cont = cont.loc[(cont != 0).any(axis=1), (cont != 0).any(axis=0)]

    # feature_labels = gffinfo[feature].to_dict()
    yates = np.any(cont < 5)
    chi2_score = chi2_contingency(cont, correction=yates)

    # 2x2 contingency table for each index, for each column
    if chi2_score.pvalue > alpha:
        return {
            'chi2_score': chi2_score,
            'pfish': pd.DataFrame(),
            'logit': pd.DataFrame()
        }
    pfish = pd.DataFrame(0,
                         index=cont.index,
                         columns=cont.columns,
                         dtype=np.float_)
    logit = pd.DataFrame(0,
                         index=cont.index,
                         columns=cont.columns,
                         dtype=np.float_)
    with Pool(NPROC) as pool:
        res = pool.map_async(cont_mapper, ((idx, col, cont)
                                           for (idx, row) in cont.iterrows()
                                           for col in row.index))
        stats = res.get()
    for idx, col, pval, lo in stats:
        pfish.loc[idx, col] = pval
        logit.loc[idx, col] = lo

    logit = logit[pfish < alpha].dropna(how='all', axis=0).dropna(how='all',
                                                                  axis=1)
    return {'chi2_score': chi2_score, 'pfish': pfish, 'logit': logit}
