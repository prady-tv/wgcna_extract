#!/usr/bin/env python3

#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-

import sys
from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
from os import PathLike
from pathlib import Path
from typing import Any, Dict, Tuple

import pandas as pd
import yaml
from Bio import Entrez


def cli() -> Dict[str, Any]:
    """Parse command line"""
    parser = ArgumentParser(
        formatter_class=ArgumentDefaultsHelpFormatter,
        description='''Count enriched GO terms from modules.''')
    parser.add_argument('base-dir',
                        default='.',
                        type=Path,
                        help='Analyse modules from this directory.')
    return vars(parser.parse_args())


def extract(qual_file: Path) -> Tuple[str, Dict[str, Tuple[str, ...]]]:
    """Extract index as a list"""
    qual_ser = pd.DataFrame(pd.read_csv(qual_file, index_col=0))
    pos = tuple(qual_ser[qual_ser.ge(0)].dropna().index.to_list())
    neg = tuple(qual_ser[qual_ser.le(0)].dropna().index.to_list())
    desc = {}
    if pos:
        desc['enrich'] = pos
    if neg:
        desc['not'] = neg
    return qual_file.stem, desc


def discover(base_dir: PathLike):
    """Discover modules and sheets"""
    _base_dir = Path(base_dir)
    org_desc: Dict[str, Dict[str, Dict[str, Tuple[str]]]] = {}
    for module in _base_dir.glob('*'):
        if module.is_dir():
            # a module directory
            description = dict(
                extract(qualifier) for qualifier in module.glob('*')
                if (qualifier.is_file() and qualifier.stem != 'Motifs'
                    and qualifier.suffix != '.yml'))
            org_desc[module.stem] = description
            if description:
                with open(module / 'description.yml', 'w') as of_h:
                    yaml.safe_dump(description, of_h)
    org_desc = {mod: desc for mod, desc in org_desc.items() if len(desc)}
    org_desc = dict(
        sorted(org_desc.items(), key=lambda x: len(x[1]), reverse=True))
    return org_desc


def main() -> int:
    """Main routine"""
    cliargs = cli()
    org_desc = discover(cliargs['base-dir'])
    out_file = cliargs['base-dir'] / 'modules.yml'
    with open(out_file, 'w') as of_h:
        yaml.safe_dump(org_desc, of_h)
    return 0


if __name__ == '__main__':
    sys.exit(main())
