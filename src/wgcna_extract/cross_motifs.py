#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-
#
# Copyright © 2022-2023 Pradyumna Paranjape
# This file is part of WGCNA_Extract.
#
# WGCNA_Extract is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# WGCNA_Extract is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FIT FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with WGCNA_Extract.  If not, see <https://www.gnu.org/licenses/>.
#
"""
- Find propensities of A, T, G, C from UTR file [P(N)]
- Read each cross-motif, generate regular expression patterns for search
- Search cross-motifs across each transcript space, find occurrences
- Test Binomial: Global occurrence of cross-motif with [P(N)] matches random
    - p < 0.001 confidence: Skewed occurrence: indeed a 'motif'.
    - p > 0.999 confidence: exact random match: certainly *NOT* a motif.
- X For motifs, check partitioning across modules: Fisher's Exact (p < 0.001)
- X Plot Log-Odds for associated Motif-Module Pairs.
    - Color: Module
    - ordering:
        - Module-groups, Descending Log-Odds (motifs)
        - Motif-groups, Descending Log-Odds (modules)
"""

import re
from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
from functools import reduce
from multiprocessing import Pool
from os import sched_getaffinity
from pathlib import Path
from typing import Dict, Iterable, List, Optional, Tuple

import numpy as np
import pandas as pd
from Bio.SeqIO import parse as fasta_parser
from scipy.stats import binomtest

from wgcna_extract.contingency import contingency_mxn_qual
from wgcna_extract.feature_stats import analyse_skew
from wgcna_extract.parsers import parse_modules
from wgcna_extract.plots import plot_enumerate, plot_logit

NPROCS = (len(sched_getaffinity(0)) - 1) or 1
"""Available processors"""


def cli():
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter,
                            description='''
    Description:

    Binomial p-values for:
    k: Occurrence of motif in RNA-sequences
    ''')
    parser.add_argument('-m',
                        '--motif',
                        type=Path,
                        default=Path('./pwms'),
                        help='motif sequences base directory (format: PWM)')
    parser.add_argument('-M',
                        '--motif-meta',
                        type=Path,
                        default=Path('RBP_Information_all_motifs.txt'),
                        help='motif CISBP-RNA meta-data')
    parser.add_argument('-t',
                        '--threshold',
                        type=float,
                        default=0.2,
                        help='PWM cut-off for motif sequence')
    parser.add_argument('-f',
                        '--feat-seq',
                        type=Path,
                        default=Path('./transcript.fa'),
                        help='file containing target fasta sequences')
    parser.add_argument('-p',
                        '--p-value',
                        type=float,
                        default=1e-3,
                        help='p-value cut-off for tests.')
    parser.add_argument('-o',
                        '--out-dir',
                        type=Path,
                        default=Path('enrichments'),
                        nargs='?',
                        help='base directory for output files.')
    parser.add_argument('-O',
                        '--organism',
                        type=str,
                        default='LDHU3',
                        help='Code for organism to store motifs')
    parser.add_argument('base-dir',
                        type=Path,
                        default=Path('.'),
                        nargs='?',
                        help='base directory containing clusters (Cytoscape).')

    return vars(parser.parse_args())


def _arbeiter_suchen(args) -> Tuple[Tuple[str, str], int]:
    """
    Worker to parallelly search motif

    Args:
        motif_id: Identifier for motif
        motif: patter of motif
        seq_id: sequence id of transcript FASTA sequence
        seq: transcript FASTA sequence

    Returns:
        Mapping:
        (seq_id, motif_id), number of matches
    """
    motif: RBPMotif = args[0]
    seq_id: str = args[1]
    seq: str = args[2]
    return (seq_id, motif.id), len(re.findall(motif.regexp, seq))


def chargaff(transcripts: Dict[str, str]) -> Dict[str, float]:
    """
    Calculate propensities of A, T, G, C from given transcriptome space

    Args:
        transcripts: seq_id, FASTA sequence

    Returns:
        propensities of A, T, G, C
    """
    prop = {'A': 0, 'T': 0, 'G': 0, 'C': 0}
    num = 0
    for sequence in transcripts.values():
        for nuc in prop:
            prop[nuc] += sequence.upper().count(nuc)
        num += len(sequence)
    return {nuc: count / num for nuc, count in prop.items()}


class RBPMotif:
    """
    RNA-Motif handle, summarizing pattern, degeneracy, length
    """

    def __init__(self,
                 id: str,
                 wobble: Iterable[Iterable[str]],
                 species: Iterable[str],
                 replace_u: bool = True):
        self.id = id
        """Motif Identifier"""

        self.species = tuple(species)
        """"""

        self.length = 0
        """length of motif"""

        self.degen = 1
        """degeneracy associated with the sequence"""

        self.wobble = [[nuc.replace('U', 'T') for nuc in pos]
                       for pos in wobble] if replace_u else wobble

        self._regexp: Optional[str] = None
        """Regular expression pattern that describes the motif"""

    def __len__(self):
        return self.length

    @property
    def seq(self):
        return self.regexp

    @property
    def regexp(self) -> str:
        """Regular expression pattern"""
        if self._regexp is None:
            _wobble: List[List[str]] = list(
                list(bases) for bases in self.wobble)
            restr = []
            for base in _wobble:
                base_degen = len(base) or 4
                if base_degen == 1:
                    restr.append(base[0])
                elif base_degen < 4:
                    restr.append('[' + ''.join([nuc for nuc in base]) + ']')
                else:
                    restr.append('.')
                self.length += 1
                self.degen *= base_degen
            for idx, term in enumerate(restr):
                if len(term) == 5:
                    antiterm = ''.join(anuc
                                       for anuc in (set(['A', 'T', 'G', 'C']) -
                                                    set(term[1:4])))
                    restr[idx] = f'[^{antiterm}]'
            self._regexp = ''.join(restr)
        return self._regexp


def binom_motif(motif: RBPMotif,
                occurrence: int,
                transcriptome: int,
                transcripts: int,
                nuc_freq: Optional[Dict[str, float]] = None) -> float:
    """
    Test if test-motif is indeed skewed in global occurrence

    Args:
        motif: Motif object handle
        occurrence: cumulative occurrence of motif
        transcriptome: size of transcriptome
        transcripts: number of transcripts in transcriptome
        nuc_freq: frequency of nucleotides in the transcriptome

    Returns:
        p-value of binomial test
    """
    _nuc_freq: Dict[str, float] = {
        'A': 0.25,
        'T': 0.25,
        'G': 0.25,
        'C': 0.25
    } if nuc_freq is None else nuc_freq
    # This is pretty lisp-y
    # Outer reduce: probabilities of motif positions are multiplied (bool AND)
    # inner reduce: probabilities of wobble are added (bool OR)
    motif_prob: float = reduce(
        lambda a, b: a * reduce(lambda x, y: x + _nuc_freq[y], b, 0.),
        motif.wobble, 1.)
    return binomtest(occurrence, transcriptome - transcripts * len(motif),
                     motif_prob).pvalue


def parse_metafile(metafile: Path) -> Dict[str, List[str]]:
    """
    Parse tab-separated file to derive motif information

    Args:
        metafile: file containing metadata about motif

    Returns:
        Motif id: List of Species
    """
    if not metafile.is_file():
        return {}
    metadata = pd.DataFrame(
        pd.read_csv(metafile, index_col=0, header=0, sep='\t'))
    return {
        m_id.split('_')[0]: df['RBP_Species'].unique()  # type: ignore
        for m_id, df in metadata.groupby('Motif_ID')
    }


def parse_motifs(base_dir: Path,
                 metafile: Optional[Path] = None,
                 threshold: float = 0.2) -> List[RBPMotif]:
    """
    Parse motifs from directory

    Args:
        base_dir: base directory in which, M_<NNN>.txt files hold pwm values
        metafile: file containing metadata to derive motif information
        threshold: probability below which, nucleotide is ignored

    Returns:
        Motif Objects' list
    """
    metadata = {} if metafile is None else parse_metafile(metafile)
    motifs: List[RBPMotif] = []
    bases: Iterable[Iterable[str]]
    for motif_file in base_dir.glob('*.txt'):
        try:
            pwm = pd.DataFrame(
                pd.read_csv(motif_file,
                            delimiter='\t',
                            index_col=0,
                            dtype=np.float_)).T
            wobble_vals = pwm.apply(
                lambda row: row[row > threshold].index.to_list(), axis=0)
            if isinstance(wobble_vals, pd.Series):
                bases = wobble_vals.to_list()  # type: ignore [assignment]
            else:
                bases = wobble_vals.iloc[0, :].to_list()
        except pd.errors.EmptyDataError:
            continue
        m_id = motif_file.stem.split('_')[0]
        motifs.append(RBPMotif(m_id, bases, species=metadata.get(m_id, ())))
    return motifs


def find_occurrence(feat_seq: Dict[str, str], cisbp_rna: List[RBPMotif]):
    """Find occurrences of any of the wobbles of the motifs"""
    with Pool(NPROCS) as pool:
        res_map = pool.map_async(_arbeiter_suchen,
                                 ((motif, seq_id, seq) for motif in cisbp_rna
                                  for seq_id, seq in feat_seq.items()))
        occur = pd.Series(dict(res_map.get())).unstack()
    occur.index.name = 'motif'
    occur.columns.name = 'transcript'
    return occur


def main() -> int:
    """Main routine"""

    cliargs = cli()

    out_dir = cliargs['base-dir'] / cliargs['out_dir']
    out_dir.mkdir(parents=True, exist_ok=True)
    (out_dir / 'sheets/motifs').mkdir(parents=True, exist_ok=True)
    (out_dir / 'images').mkdir(parents=True, exist_ok=True)
    org_code = cliargs['organism']

    feat_seq = {
        str(record.id): str(record.seq)
        for record in fasta_parser(cliargs['feat_seq'], 'fasta')
    }
    nuc_freq = chargaff(feat_seq)
    print('Nucleotide Frequencies:\n', nuc_freq)

    cisbp_rna = parse_motifs(cliargs['motif'],
                             cliargs['motif_meta'],
                             threshold=cliargs['threshold'])
    print(f'Read {len(cisbp_rna)} database motifs.')
    pd.Series({
        motif.id: motif.seq
        for motif in cisbp_rna
    }).to_csv((out_dir / 'sheets/motifs/regexp.csv'))
    motif_occur = find_occurrence(feat_seq, cisbp_rna)
    print('Occurrence:\n', motif_occur.describe())
    cross_usage = pd.DataFrame(
        {
            motif.id: [
                binom_motif(
                    motif, motif_occur.loc[:, motif.id].sum(),
                    reduce(lambda x, y: x + len(y), feat_seq.values(), 0),
                    len(feat_seq), nuc_freq), motif.species
            ]
            for motif in cisbp_rna
        },
        index=('p-value', 'species')).T
    alpha = cliargs['p_value']
    certain_motifs = cross_usage[cross_usage['p-value'] < alpha]
    print(f'{len(certain_motifs)} motifs skewed.')
    uncertain_motifs = cross_usage[cross_usage['p-value'] >= alpha]

    print(f'{len(uncertain_motifs)} motifs uncertain.')
    certain_motifs.to_csv((out_dir / f'sheets/motifs/{org_code}-motifs.tsv'),
                          sep='\t')
    uncertain_motifs.to_csv(
        (out_dir / f'sheets/motifs/{org_code}-uncertain-motifs.tsv'), sep='\t')

    module_labels = pd.Series(parse_modules(
        cliargs['base-dir'])).groupby(level=0).value_counts().unstack()
    module_labels = module_labels.fillna(0).astype(np.bool_)
    module_labels.columns.name = 'modules'
    motif_bool = motif_occur[certain_motifs.index].apply(lambda x: x > 0)

    # Fisher exact test for each motif-module pair
    cont = contingency_mxn_qual(qual_1=motif_bool,
                                qual_2=module_labels,
                                qual_1_name='motifs',
                                qual_2_name='module')
    cont.to_csv(out_dir / f'sheets/{org_code}_motifs_contingency.csv')
    chi2_res = analyse_skew(cont, alpha)
    chi2_score, pfish, logit = chi2_res['chi2_score'], chi2_res[
        'pfish'], chi2_res['logit']
    print(f'chi2_pvalue: {chi2_score.pvalue}')
    if not pfish.empty:
        pfish.to_csv(out_dir / 'sheets/motifs_pfisher.csv')
    if not logit.empty:
        logit.to_csv(out_dir / 'sheets/motifs_logit.csv')
        plot_logit(logit,
                   'motifs',
                   out_dir,
                   groupby='module',
                   units='',
                   xlabels='module',
                   resolution=2,
                   force_resolution=True,
                   module_plots=True)
        plot_enumerate(logit, out_dir, 'motifs')
    return 0


if __name__ == '__main__':
    main()
