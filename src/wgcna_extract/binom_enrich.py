#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-

from math import log10
from multiprocessing import Pool
from os import sched_getaffinity
from pathlib import Path
from typing import Dict, Iterable, Optional, Set, Tuple, Union

import pandas as pd
from brokenaxes import BrokenAxes, brokenaxes
from matplotlib import pyplot as plt
from matplotlib.axes import Axes
from matplotlib.figure import Figure
from scipy.stats import binomtest, fisher_exact, hypergeom


def _fp_to_farb(fp: Path) -> str:
    return str(fp).split('-')[-1].split('.')[0]


def _color_axes(ax, fig, theme: str = 'light'):
    if theme == 'light':
        ax.set_facecolor('w')
        fig.set_facecolor('w')
        ax.spines['bottom'].set_color('k')
        ax.spines['top'].set_color('k')
        ax.spines['right'].set_color('k')
        ax.spines['left'].set_color('k')

        ax.tick_params(axis='x', colors='k')
        ax.tick_params(axis='y', colors='k')

        ax.yaxis.label.set_color('k')
        ax.xaxis.label.set_color('k')
        ax.title.set_color('k')
        return 'w', 'k'
    ax.set_facecolor('k')
    fig.set_facecolor('k')
    ax.spines['bottom'].set_color('w')
    ax.spines['top'].set_color('w')
    ax.spines['right'].set_color('w')
    ax.spines['left'].set_color('w')

    ax.tick_params(axis='x', colors='w')
    ax.tick_params(axis='y', colors='w')

    ax.yaxis.label.set_color('w')
    ax.xaxis.label.set_color('w')
    ax.title.set_color('w')
    return 'k', 'w'


def _gen_fig_ax(xdata, ydata, theme='light'):
    ax: Union[BrokenAxes, Axes]
    # if (max(ydata) - min(ydata)) < 30:
    _subpl = plt.subplots()
    fig: Figure = _subpl[0]  # type: ignore
    ax = _subpl[1]  # type: ignore
    _, fg = _color_axes(ax, fig, theme)
    # else:
    #     fig = plt.figure()  # type: ignore
    #     ax = brokenaxes()
    #     _, fg = _color_axes(ax, fig, theme)
    #     # fg = 'w'
    if len(xdata) < 30:
        ax.set_xticks(range(len(xdata)), xdata)  # type: ignore
    else:
        ax.set_xticks(range(len(xdata)), [None] * len(xdata))  # type: ignore

    return fig, ax, fg


def plot_excesses(excess: Dict[str, Dict[str, pd.Series]],
                  outdir: Path,
                  thresh: float = 0.001,
                  direction: int = 0):
    # subplot = 0
    logthresh = -log10(thresh)
    for qual, freq in excess.items():
        global_df = pd.DataFrame(index=pd.Index(()),
                                 columns=pd.Index(
                                     (f'-log(p-value) for {qual}', 'color')))
        for mod, feat_ser in freq.items():
            print(f'Plotting {qual} -> {mod}...')
            if any([
                    deepcol in mod
                    for deepcol in ('black', 'dark', 'night', 'deep')
            ]):
                theme = 'light'
            else:
                theme = 'dark'
            if direction == -1:
                feat_plot: pd.Series = feat_ser[feat_ser < 0]  # type: ignore
            elif direction == 1:
                feat_plot = feat_ser[feat_ser > 0]  # type: ignore
            else:
                feat_plot = feat_ser

            plot_color = 'grey' if mod == 'grey60' else mod
            feat_df = pd.DataFrame(feat_ser)
            feat_df['color'] = plot_color
            global_df = pd.concat((global_df, feat_df), axis=0)

            fig, ax, fg = _gen_fig_ax(feat_plot.index, feat_plot, theme)
            ax.bar(range(len(feat_plot)), feat_plot, color=plot_color)
            ax.set_title(f'module: {mod}')
            # line at cut-off
            # bad type-hint for y.
            ax.axhline(logthresh, color='grey', linestyle=':')  # type: ignore
            ax.axhline(0, color='grey')  # type: ignore
            ax.axhline(-logthresh, color='grey', linestyle=':')  # type: ignore

            ax.set_xlabel(qual)
            ax.set_ylabel('-log(p-value) [directed]')
            ax.tick_params(axis='x', which='major', labelsize=8)
            fig.suptitle(f'Enrichment or Depletion of Qualifier', color=fg)
            fig.autofmt_xdate()
            (outdir / mod).mkdir(parents=True, exist_ok=True)
            plt.savefig((outdir / mod / qual).with_suffix('.svg'))
        global_df.rename(
            columns={f'-log(p-value) for {qual}': '-log(p-value)'},
            inplace=True)

        # Common global overview
        print(f'Plotting {qual} for all modules together')
        global_df.sort_values(inplace=True,
                              by=f'-log(p-value)',
                              ascending=False)
        fig, ax, fg = _gen_fig_ax(global_df.index, global_df['-log(p-value)'],
                                  'light')
        ax.bar(range(len(global_df)),
               global_df['-log(p-value)'],
               color=global_df['color'])
        ax.set_title(f'All')
        ax.set_xlabel(qual)
        ax.set_ylabel('-log(p-value) [directed]')
        ax.tick_params(axis='x', which='major', labelsize=8)
        fig.suptitle(f'Enrichment or Depletion of Qualifier')

        # line at cut-off
        # bad type-hint for y.
        ax.axhline(logthresh, color='grey', linestyle=':')  # type: ignore
        ax.axhline(0, color='grey')  # type: ignore
        ax.axhline(-logthresh, color='grey', linestyle=':')  # type: ignore

        fig.autofmt_xdate()
        (outdir / 'all').mkdir(parents=True, exist_ok=True)
        plt.savefig((outdir / 'all' / qual).with_suffix('.svg'))


# Object Structures
class QualCounter():
    """
    Qualifiers Counter, inspired by `:py:class:collections.Counter`

    Args:
        quals: index: feature id, columns: qualifiers Value: tuple of qualifier terms
        uninformative: Iterable of terms that are not informative
    """

    def __init__(self,
                 quals: Union[pd.DataFrame, pd.Series],
                 vague: Optional[Iterable[str]] = None):
        self.quals = pd.DataFrame(quals)
        """qualifiers dataframe"""

        self.vague = tuple(vague or ())
        """Mask vague terms"""

        self._qualifier_feats = None
        self._qual_dict: Dict[str, Dict[str, Tuple[str, ...]]] = {}
        self._all_terms: Dict[str, Set[str]] = {}
        self._term_feat_freq: Optional[Dict[str, pd.DataFrame]] = None

    @property
    def qual_dict(self) -> Dict[str, Dict[str, Tuple[str, ...]]]:
        """
        Dictionary associating each index (feature) from qualifier
        with all informative terms in its qualifier

        Structure:
        {<feature>: {<Qualifier>: (term, ...)}}

        """
        if not self._qual_dict:
            for feat, qual_ser in self.quals.T.items():
                self._qual_dict[str(feat)] = {
                    str(qual): vals
                    for qual, vals in qual_ser.items()
                }
        return self._qual_dict

    @property
    def all_terms(self) -> Dict[str, Set[str]]:
        """
        All terms that occur in the complete feature set

        {<Qualifier>: {term, ...}}
        """
        if not self._all_terms:
            self._all_terms = {}
            for qual in self.qual_dict.values():
                for qual, terms in qual.items():
                    parsed_qual = qual.replace('|', 'NOT')
                    if parsed_qual not in self._all_terms:
                        self._all_terms[parsed_qual] = set()
                    for val in terms:
                        self._all_terms[parsed_qual].add(val)
        return self._all_terms

    @property
    def qualifiers(self) -> Set[str]:
        """Known qualifiers"""
        return set(self.all_terms.keys())

    @property
    def num_feat(self) -> int:
        """Number of features"""
        return len(self.quals)

    @property
    def num_terms(self) -> Dict[str, int]:
        """
        Number of kinds of terms in each qualifier
        {<Qualifier>: <num(term-kinds)>}
        """
        return {qual: len(terms) for qual, terms in self.all_terms.items()}

    @property
    def term_feat_freq(self) -> Dict[str, pd.DataFrame]:
        """
        Term feature frequencies as counts and fraction of total.

        Structure:
        {<qualifier>, pd.DataFrame(index=[term, ...], cols=[count, frac])}
        index: terms
        columns:
            count: number of features containing the term
            frac: occurrence of features containing the terms
                as a fraction of all features
        """
        if self._term_feat_freq is None:
            _feat_freq_dict = {}
            self._term_feat_freq = {}
            for qual, qual_vals in self.all_terms.items():
                if qual not in _feat_freq_dict:
                    _feat_freq_dict[qual] = {}
                for term in qual_vals:
                    if term:
                        _feat_freq_dict[qual][term] = len(
                            set([
                                feat
                                for feat, record in self.qual_dict.items()
                                if ((qual in record) and (term in record[qual])
                                    )
                            ]))
                self._term_feat_freq[qual] = pd.DataFrame(
                    _feat_freq_dict[qual], index=['count']).T
                self._term_feat_freq[qual]['frac'] = self._term_feat_freq[
                    qual]['count'] / self.num_feat
                vague = self._term_feat_freq[qual].index.intersection(
                    self.vague)
                self._term_feat_freq[qual].drop(index=vague, inplace=True)
        return self._term_feat_freq


def binom_test_array(occur: pd.Series, num: int, prob: pd.Series) -> pd.Series:
    """
    Test that at given probabilities in given module size space,
    what is the pvalue of occurrence

    pd.Series(index = terms)

    TODO: Replace this by fisher-exact test:
    https://en.wikipedia.org/wiki/Fisher%27s_exact_test#Purpose_and_scope

    Args:
        occur: Occurrence of terms
        num: size of module
        prob: probability of occurrence of terms in order as in ``occur``

    """
    prob_occur = pd.concat((occur, prob), axis=1)
    return pd.Series(
        prob_occur.apply(lambda x: binomtest(x['count'], num, x['frac']),
                         axis=1))


def module_terms(quals: pd.DataFrame,
                 base_dir: Path) -> Dict[str, pd.DataFrame]:
    """
    Create a Module: gene_ids dict

    Args:
        quals: dataframe of all qualifiers
        base_dir: directory containing *-nodes-*.tsv records

    Returns:
        Dict with list of corresponding feature_ids for each module
        sorted ascending by number of 1st qualifier's-values for the feature
    """
    terms_dict: Dict[str, pd.DataFrame] = {}
    for nodefile in base_dir.glob('*-nodes-*.tsv'):
        nodes_records = pd.DataFrame(pd.read_csv(nodefile,
                                                 delimiter='\t',
                                                 index_col=0),
                                     dtype=str)
        terms_dict[_fp_to_farb(nodefile)] = quals.loc[quals.index.intersection(
            nodes_records.index)]

    return dict(
        sorted([td for td in terms_dict.items()],
               key=lambda x: len(x[1]),
               reverse=True))
