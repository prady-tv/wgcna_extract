#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-

"""
Ad-hoc script

Walk across all minClust_* directories,
guess the minSize,
load *.RData files,
calculate sse
plot
"""


import sys
from pathlib import Path
from typing import Dict

from pyreadr import read_r
from psprint import print


def load_r(fpath: Path) -> Dict:
    """Load RData inside fpath and return objects"""
    print(str(fpath / "wgcna_anal.RData"))
    return read_r(str(fpath / "wgcna_anal.RData"))


def main(args):
    """main routine wrap"""
    basedir = Path(args[1]).resolve()
    for minClustDir in basedir.glob("minClust_*"):
        minClust = int(str(minClustDir.name).split("_")[-1])
        print(minClust, load_r(minClustDir).keys())


if __name__ == "__main__":
    main(sys.argv)
