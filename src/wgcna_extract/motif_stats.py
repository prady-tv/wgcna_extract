#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-
#
# Copyright © 2022-2023 Pradyumna Paranjape
# This file is part of WGCNA_Extract.
#
# WGCNA_Extract is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# WGCNA_Extract is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FIT FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with WGCNA_Extract.  If not, see <https://www.gnu.org/licenses/>.
#
"""Load Motif-map occurrence table and check significant log-odds."""

import json
from argparse import ArgumentParser, RawDescriptionHelpFormatter
from multiprocessing import Pool
from os import sched_getaffinity
from pathlib import Path
from typing import Dict, Iterable, Tuple, Union

import numpy as np
import pandas as pd
from scipy.stats import binomtest

from wgcna_extract.feature_stats import analyse_skew
from wgcna_extract.motif_map import CISMotif, GTFeature

NPROC = (len(sched_getaffinity(0)) - 1) or 1
"""Parallel available processors (spare 1)."""


def cli():
    """
    Parse command line arguments.

    Returns
    -------
    dict[str, Any]
        command-line args as a dict
    """
    parser = ArgumentParser(formatter_class=RawDescriptionHelpFormatter,
                            description='''
    Load Chargaff feature table, Motifs, Occurrence table to calculate log-odds
    ''')
    parser.add_argument('-f',
                        '--gtf',
                        type=Path,
                        required=True,
                        help='Features file (GTF)')
    parser.add_argument('-m',
                        '--motifs',
                        type=Path,
                        required=True,
                        help='Motifs (JSON)')
    parser.add_argument('-c',
                        '--chargaff',
                        type=Path,
                        required=True,
                        help='Chargaff table (CSV)')
    parser.add_argument('-o',
                        '--occurrence',
                        type=Path,
                        required=True,
                        help='Occurrence table (CSV)')
    parser.add_argument('-O',
                        '--orientation',
                        dest='revcomp',
                        action='store_true',
                        help='Orientation of motif matters')
    parser.add_argument('-p',
                        '--p-value',
                        type=float,
                        default=1e-3,
                        help='Confidence')
    return vars(parser.parse_args())


def load_data(
    feat_fp: Path, mot_fp: Path, charg_fp: Path, occur_fp: Path
) -> Tuple[Dict[str, GTFeature], Dict[str, CISMotif], pd.DataFrame,
           pd.DataFrame]:
    """
    Load stored data.

    Parameters
    ----------
    feat_fp : Path
        features (GTF)
    mot_fp : Path
        motifs (JSON)
    charg_fp : Path
        chargaff table (CSV)
    occur_fp : Path
        occurrence table (CSV)

    Returns
    -------
    tuple[dict[str, :class:`wgcna_extract.motif_map.GTFeature`], \
        dict[str, :class:`wgcna_extract.motif_map.CISMotif`], \
        :class:`pandas.DataFrame`, :class:`pandas.DataFrame`]
        Loaded data

    See Also
    --------
    :mod:`wgcna_extract.motif_map`
    """
    gtf = pd.DataFrame(
        pd.read_csv(feat_fp, sep='\t', header=None, index_col=False))

    features = {
        gtfeat.handle: gtfeat
        for gtfeat in [GTFeature(feat) for _, feat in gtf.iterrows()]
    }
    with mot_fp.open() as mot_fh:
        cisbp = {
            motif_id: CISMotif(motif_id, motif_dat)
            for motif_id, motif_dat in json.load(mot_fh).items()
        }
    chargaff = pd.DataFrame(pd.read_csv(charg_fp, index_col=0), dtype=np.int_)
    occur = pd.DataFrame(pd.read_csv(occur_fp, index_col=0), dtype=np.int_)
    return features, cisbp, chargaff, occur


def binom(chargaff: pd.DataFrame,
          motif: CISMotif,
          occur: Union[int, Iterable[int]],
          revcomp: bool = False):
    """
    Calculate binomial p-value of occurrence.

    Parameters
    ----------
    chargaff : pd.DataFrame
        ATGC counts. They will be summed along index.
    motif : :class:`CISMotif`
        motif_handle
    occur : pd.Series
        Observed occurrences. They will be summed.
    revcomp : bool
        reverse complement match

    Returns
    -------
    float
        p-value of binomial test
    """
    counts = chargaff.sum(axis=0)
    size = counts.sum()
    if not isinstance(occur, int):
        occur = sum(occur)
    probability = motif.p_occur(counts.to_dict(), revcomp)
    return binomtest(occur, size, probability).pvalue


def calc_binom(args):
    """
    Calculate binomial p-values of occurrence of motif in feature.

    Parameters
    ----------
    args: tuple
        motif_id : str
            identifier for motif
        motif : :class:`CISMotif`
            motif object-handle
        meta_occur : pd.DataFrame
            occurrence in meta-features
        meta_features : dict[str, :class:`GTFeature`]
            meta features (other than 'exon')
        chargaff : pd.DataFrame
            A, T, G, C counts for each feature
        revcomp : bool
            calculate revcomp
    """
    motif_id, motif, meta_occur, meta_features, chargaff, orient = args
    return motif_id, meta_occur.groupby(
        lambda feat: meta_features[feat].feat_type).apply(
            lambda occurrence: binom(chargaff.loc[occurrence.index], motif,
                                     occurrence[motif_id], orient))


def main():
    """Entry Point."""
    cliargs = cli()
    features, cisbp, chargaff, occur = load_data(cliargs['gtf'],
                                                 cliargs['motifs'],
                                                 cliargs['chargaff'],
                                                 cliargs['occurrence'])
    meta_features = {
        feat_loc: feat
        for feat_loc, feat in features.items() if feat.feat_type != 'exon'
    }
    meta_occur = occur.loc[list(meta_features.keys())]
    print(
        meta_occur.groupby(lambda feat: meta_features[feat].feat_type).apply(
            lambda occurrence: analyse_skew(
                occurrence,  # type: ignore [arg-type]
                cliargs['p_value'])['chi2_score'].pvalue))

    with Pool(NPROC) as pool:
        res = pool.imap_unordered(calc_binom,
                                  ((motif_id, motif, meta_occur, meta_features,
                                    chargaff, cliargs['revcomp'])
                                   for motif_id, motif in cisbp.items()))
        binom_pvalues = pd.DataFrame(dict(res))
    binom_pvalues = binom_pvalues[binom_pvalues < cliargs['p_value']].dropna(
        axis=0, how='all').dropna(axis=1, how='all')

    print(binom_pvalues.loc['upstream'].dropna())
    print(binom_pvalues.loc['polycistron'].dropna())
    print(binom_pvalues.loc['downstream'].dropna())


if __name__ == '__main__':
    main()
