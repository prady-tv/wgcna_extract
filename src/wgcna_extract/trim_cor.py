#!/usr/bin/env python
# -*- coding:utf-8; mode: python -*-
"""Wrapper for WGCNA"""

import gc
from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple

import pandas as pd
from psprint import print


def cli() -> Dict[str, Any]:
    parser = ArgumentParser(
        formatter_class=ArgumentDefaultsHelpFormatter,
        description="trim GOI's connections down with threshold for adjacency")
    parser.add_argument('--adjacency',
                        help='adjacency matrix csv file path',
                        type=Path,
                        default='adjacency.csv')
    parser.add_argument('--annotation',
                        help='annotation matrix file path',
                        type=Path,
                        default='GeneAnnotation.csv')
    parser.add_argument('--dirbase',
                        help='base directory that contains Cytoscape exports',
                        type=Path,
                        default='./')
    parser.add_argument('--threshold',
                        metavar='THR',
                        help='lower bound to adjacency filter',
                        type=float,
                        default=0.95)
    parser.add_argument('--annot-col-name',
                        help='column-name of annotation',
                        type=str,
                        default='gene_symbol')
    parser.add_argument('--gene_id', help='ID of GOI', type=str, nargs='+')
    parser.add_argument('--gene_name',
                        help="Gene's name as described in annotation.",
                        nargs='+',
                        type=str)
    parser.add_argument('--quantile',
                        action='store_true',
                        help='Return targets beyond THR{th} quantile.')
    return vars(parser.parse_args())


def _gid_from_gname(gname: str, annot: pd.Series) -> List[str]:
    gids = annot[annot == gname].index
    if len(gids) == 0:
        print(f'{gname} not found in annotation', mark='err')
        return []
    return list(gids)


def _fish_linkers(parent: Path, mod_color, target_gene_id, target_gene_name):
    edge_file = parent / f"CytoscapeInput-edges-{mod_color}.tsv"
    edges = pd.DataFrame(
        pd.read_csv(edge_file, index_col=None, header=0, sep='\t'))
    from_links = edges[(edges["fromNode"] == target_gene_id) |
                       (edges["fromAltName"] == target_gene_name)]
    to_links = edges[(edges["toNode"] == target_gene_id) |
                     (edges["toAltName"] == target_gene_name)]
    from_partners = from_links[["toNode", "toAltName"]]
    from_partners.columns = 'Node', 'AltName'
    to_partners = to_links[["fromNode", "fromAltName"]]
    to_partners.columns = 'Node', 'AltName'
    partners = pd.concat((from_partners, to_partners))
    return pd.Index(partners['Node'])


def _spot_color(parent: Path, gene_id: str, gene_name: str) -> Optional[str]:
    for nodefile in Path(parent).glob("*nodes*.tsv"):
        if any((g in nodefile.read_text() for g in (gene_name, gene_id))):
            print(f'{gene_name} ({gene_id}) was found in {nodefile}')
            return nodefile.stem.split("-")[-1]
    raise ValueError('Gene is not Clustered/exported in available files.')


def _module_partners(dirbase: Path, gene_id: str, gene_name: str):
    mod_color = _spot_color(dirbase, gene_id, gene_name)
    return _fish_linkers(dirbase, mod_color, gene_id, gene_name)


def _fish_adjacent(gene_id: str,
                   gene_name: str,
                   annot: pd.DataFrame,
                   adjacency: pd.DataFrame,
                   dirbase: Path,
                   threshold: float = 0.9,
                   quantile: bool = False) -> Tuple[float, pd.DataFrame]:
    module_partners = _module_partners(dirbase, gene_id, gene_name)
    g_adj: pd.Series = adjacency[gene_id]
    # set self's adjacency to zero, to drop it from potential 'targets'
    g_mods: pd.Series = g_adj.filter(items=module_partners)
    if quantile:
        quant = g_mods.quantile(threshold)
        g_thr_adj_mods: pd.Series = g_mods[g_mods > quant]
    else:
        g_thr_adj_mods: pd.Series = g_mods[g_mods >= threshold]
        if len(g_thr_adj_mods) < 2:
            print(f"Threshold wasn't crossed by any target.")
            print(f"Adjusting threshold to '{threshold} of maximum'.")
            threshold = g_mods.nlargest(2)[1] * threshold
            print(f'New threshold: {threshold}')
            g_thr_adj_mods: pd.Series = g_mods[g_mods >= threshold]

    interactors = pd.concat((g_thr_adj_mods, annot[g_thr_adj_mods.index]),
                            axis=1)
    interactors.sort_values(by=gene_id, ascending=False, inplace=True)
    return float(threshold), interactors


def main():
    """main routine"""
    cliargs = cli()
    annot = pd.DataFrame(pd.read_csv(cliargs['annotation'],
                                     index_col=0))[cliargs['annot_col_name']]
    print("Reading adjacency")
    adj = pd.DataFrame(pd.read_csv(cliargs['adjacency'], index_col=0))
    gene_info: Dict[str, List[str]] = {}
    gene_id = cliargs['gene_id']
    if gene_id is not None:
        gene_name: str = cliargs['gene_name'][0] or annot.loc[gene_id]
        gene_info[gene_name] = gene_id
    else:
        for gname in cliargs['gene_name']:
            gene_id: List[str] = _gid_from_gname(gname, annot)
            gene_info[gname] = gene_id
    for gname, paralogs in gene_info.items():
        for gid in paralogs:
            print(f"Processing {gid}", mark='info')
            threshold, interactors = _fish_adjacent(gid, gname, annot, adj,
                                                    cliargs['dirbase'],
                                                    cliargs['threshold'],
                                                    cliargs['quantile'])
            outfile = cliargs['dirbase'] / ('_'.join(
                (gid, f'{threshold:0.2f}', 'quant' * cliargs['quantile'])) +
                                            '.csv')
            interactors.to_csv(outfile)


if __name__ == '__main__':
    main()
