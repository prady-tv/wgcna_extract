#!/usr/bin/env python3
# -*-coding: utf-8; mode: python-*-

from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
from pathlib import Path
from typing import Any, Dict, List, Optional

import matplotlib
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from matplotlib.patches import Rectangle

CMAP = matplotlib.colormaps['Accent']


def _get_boundaries(meta: pd.DataFrame,
                    idx: Optional[pd.Index] = None) -> List[int]:
    if idx is None:
        idx = meta.idx
    boundaries: List[int] = []
    old_val = None
    phase = 0
    for phase, val in enumerate(meta.loc[idx, 'bound'].to_numpy().tolist()):
        if val != old_val:
            old_val = val
            boundaries.append(phase)
    boundaries.append(phase + 1)
    return boundaries


def _phase_marks(ax,
                 boundaries: List[int],
                 height: float = 0.2,
                 transparency: float = 0.3):
    num_bounds = len(boundaries)
    for phase, l_bound in enumerate(boundaries):
        trans_color = list(CMAP(phase / num_bounds))[:3] + [transparency]
        if phase == len(boundaries) - 1:
            return
        u_bound = boundaries[phase + 1]
        ax.add_patch(
            Rectangle((l_bound - 0.7, -1.5),
                      u_bound - l_bound + 0.2,
                      height,
                      facecolor=tuple(trans_color),
                      fill=True))


def _color_axes(ax, fig, theme: str = 'light'):
    if theme == 'light':
        ax.set_facecolor('w')
        fig.set_facecolor('w')
        ax.spines['bottom'].set_color('k')
        ax.spines['top'].set_color('k')
        ax.spines['right'].set_color('k')
        ax.spines['left'].set_color('k')

        ax.tick_params(axis='x', colors='k')
        ax.tick_params(axis='y', colors='k')

        ax.yaxis.label.set_color('k')
        ax.xaxis.label.set_color('k')
        ax.title.set_color('k')
    else:
        ax.set_facecolor('k')
        fig.set_facecolor('k')
        ax.spines['bottom'].set_color('w')
        ax.spines['top'].set_color('w')
        ax.spines['right'].set_color('w')
        ax.spines['left'].set_color('w')

        ax.tick_params(axis='x', colors='w')
        ax.tick_params(axis='y', colors='w')

        ax.yaxis.label.set_color('w')
        ax.xaxis.label.set_color('w')
        ax.title.set_color('w')


def rel_norm(gene_exp: pd.Series, log_base: Optional[int] = None):
    if log_base is None:
        rel_exp = gene_exp
    else:
        rel_exp = np.log(gene_exp / gene_exp.mean()) / np.log(log_base)
    return rel_exp / np.max(np.abs(rel_exp))


def cli() -> Dict[str, Any]:
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('--biogene',
                        default='adj_counts.csv',
                        type=Path,
                        help='Counts CSV file: rows: genes, cols: phases')
    parser.add_argument('--gene_id',
                        type=str,
                        help='gene_ids of genes to be plotted',
                        nargs='*')
    parser.add_argument('--eigen',
                        default='eigengenes.csv',
                        type=Path,
                        help='CSV file: rows: phases, cols: modules')
    parser.add_argument('--meta',
                        type=Path,
                        default='meta.csv',
                        help='metadata')
    parser.add_argument('--colors',
                        help='plot for only these colors',
                        nargs='*')
    parser.add_argument('--output', type=Path, default='eigenplot')
    return vars(parser.parse_args())


def main(cliargs: Dict[str, Any]):
    print(cliargs)
    metadata = pd.DataFrame(pd.read_csv(cliargs['meta'], index_col=0))
    eigen_frame = pd.DataFrame(pd.read_csv(cliargs['eigen'], index_col=0))
    dat_expr = pd.DataFrame(pd.read_csv(cliargs['biogene'], index_col=0)).T
    dat_expr.sort_index(key=lambda x: metadata.loc[x, 'order'], inplace=True)
    eigen_frame.sort_index(key=lambda x: metadata.loc[x, 'order'],
                           inplace=True)
    metadata.sort_index(key=lambda x: metadata.loc[x, 'order'], inplace=True)
    fig, ax = plt.subplots(1, 1)
    boundaries = _get_boundaries(metadata, idx=eigen_frame.index)
    _phase_marks(ax, boundaries, height=3)
    theme = 'light'
    restricted_colors = cliargs.get('colors')
    for module in eigen_frame.columns:
        mod_color = module[2:]
        if restricted_colors is not None:
            if mod_color not in restricted_colors:
                continue
        if any(
            [lightcol in mod_color
             for lightcol in ('black', 'dark', 'night')]) and theme != 'light':
            theme = 'light'
        eigengene_height = rel_norm(eigen_frame[module]).to_numpy()
        ax.bar(
            range(len(eigengene_height)),
            eigengene_height,
            # ls='--',
            label=mod_color,
            color=mod_color.replace('grey60', 'silver'))
    for gene in cliargs.get('gene_id') or []:
        gene_height = rel_norm(dat_expr[gene], 2).to_numpy()
        ax.plot(range(len(gene_height)), gene_height, label=gene)  # , color='k'
    xlabs = [metadata.loc[srr, 'group'] for srr in eigen_frame.index]
    ax.set_xlabel('conditions')
    ax.set_ylabel('normalized expression')
    _color_axes(ax, fig, theme)
    ax.xaxis.set_ticks(range(len(xlabs)), xlabs)
    ax.set_xticklabels(xlabs, rotation=45, fontsize=5)
    ax.set_ylim(bottom=-1.5, top=1.5)
    plt.legend()
    plt.savefig(cliargs['output'].with_suffix('.png'), dpi=700)


if __name__ == "__main__":
    main(cli())
