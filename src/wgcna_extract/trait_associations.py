#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-

from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
from os import sched_getaffinity
from pathlib import Path
from typing import Any, Dict, List, Sequence, Tuple

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from numpy.typing import NDArray
from psprint import print
from statsmodels.formula import api as smf

MAX_THREADS = max((len(sched_getaffinity(0)) - 1), 1)
"""Maximum usable threads = (# available - 1) or 1"""


def _fp_to_farb(fp: Path) -> str:
    return str(fp).split('-')[-1].split('.')[0]


def cli() -> Dict[str, Any]:
    """Command-line arguments"""
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter,
                            description='''WGCNA trait associations''')
    parser.add_argument('--base-dir',
                        help='base directory',
                        type=Path,
                        default='.')
    parser.add_argument('--counts',
                        help='feature counts: Row: genes, Col: conditions',
                        type=Path,
                        default='counts.csv')
    parser.add_argument('--eigen',
                        type=Path,
                        help='Eigengenes: Rows: conditions, cols: eigengenes')
    parser.add_argument('--meta',
                        help='meta-data',
                        type=Path,
                        default='meta.csv')
    return vars(parser.parse_args())


def module_gene_ids(base_dir: Path) -> Dict[str, Tuple[str, ...]]:
    """
    Create a Module: gene_ids dict

    Args:
        base_dir: directory containing *-nodes-*.tsv records

    Returns:
        Dict with list of corresponding gene_ids for each module
    """
    module_dict: Dict[str, Tuple[str]] = {}
    for nodefile in base_dir.glob('*-nodes-*.tsv'):
        nodes_records = pd.DataFrame(
            pd.read_csv(nodefile, delimiter='\t', index_col=0))
        module_dict[_fp_to_farb(nodefile)] = tuple(
            nodes_records.index.tolist())
    return module_dict


def load_data(
        cliargs: Dict[str,
                      Any]) -> Tuple[List[str], Dict[str, Any], pd.DataFrame]:
    meta_data = pd.DataFrame(pd.read_csv(cliargs['meta'], index_col=0))
    conditions: NDArray[np.str_] = np.unique(meta_data['group'])
    eumodules = {
        mod: genes
        for mod, genes in module_gene_ids(cliargs['base_dir']).items()
        if mod not in ('grey', 'gray')
    }
    eigen = pd.DataFrame(pd.read_csv(cliargs['eigen'], index_col=0))
    return conditions.tolist(), eumodules, eigen


def analyse(conditions: Sequence[str], modules: Dict[str, Any],
            eigen: pd.DataFrame):
    cols = ['ANOVA'], list(conditions)
    trait_p = pd.DataFrame(np.ones((len(modules), len(cols))) * np.nan,
                           dtype=np.float_,
                           columns=cols,
                           index=list(modules.keys()))
    trait_b = trait_p.copy()
    trait_se = trait_p.copy()
    for mod in modules:
        me = eigen[f'AE{mod}']
        mixlm = smf.mixedlm('me ~ Group + Age + Sex', data='metadata')
        mixlm.fit()


def main():
    """Main Subroutine"""
    cliargs = cli()


if __name__ == '__main__':
    main()
