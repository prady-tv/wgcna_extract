#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-
#
# Copyright © 2022-2023 Pradyumna Paranjape
# This file is part of WGCNA_Extract.
#
# WGCNA_Extract is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# WGCNA_Extract is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FIT FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with WGCNA_Extract.  If not, see <https://www.gnu.org/licenses/>.
#

import sys
from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
from pathlib import Path
from typing import Any, Dict, Iterable

import numpy as np
import pandas as pd

from wgcna_extract.contingency import contingency_mxn_qual
from wgcna_extract.feature_stats import analyse_skew
from wgcna_extract.parsers import parse_modules
from wgcna_extract.plots import plot_enumerate, plot_logit

VAGUE = ('', 'protein', 'conserved', 'hypothetical', 'putative', 'subunit',
         'domain', 'containing', 'unknown', 'uncharacterized',
         'uncharacterised')
"""Vague terms."""


def split_annotation(symbol: str, sep: Iterable[str] = ('_', ' ', '-', '/')):
    """Split term by all separators."""
    symlist = [str(symbol).lower()]
    for delim in sep:
        symlist = [t for term in symlist for t in term.split(delim)]
    return symlist


# Inputs
def read_annotation(annotation: Path, sep: str = ',') -> pd.Series:
    """
    Parse annotation file to generate terms used in gene-annotations.

    Parameters
    ----------
    annotation : Path
        annotation file name
    sep : str
        separator for annotation table

    Returns
    -------
    pd.Series
        occurrence of terms for each gene
    """
    return pd.DataFrame(pd.read_csv(annotation, index_col=0, header=0,
                                    sep=sep))['gene_symbol']


def parse_annotation(annotations: pd.Series,
                     vague: Iterable[str] = VAGUE,
                     keep_vague: bool = False) -> pd.DataFrame:
    """
    Parse annotation series to generate terms used in gene-annotations.

    Parameters
    ----------
    annotation : pd.Series
        annotation series
    sep : Iterable[str]
        separator for annotation table
    keep_vague : bool
        maintian account of vague terms too

    Returns
    -------
    pd.DataFrame
        occurrence of terms for each gene
    """
    annotation_terms = annotations.apply(split_annotation)
    all_terms = [
        term for gene in annotation_terms.to_dict().values() for term in gene
        if ((keep_vague or (term not in vague)) and (len(term)) > 1)
    ]

    # unique
    all_terms = list(set(all_terms))

    gene_terms = annotation_terms.apply(
        lambda terms: [test in terms for test in all_terms])
    return pd.DataFrame(gene_terms.to_dict().values(),
                        index=gene_terms.index,
                        columns=all_terms,
                        dtype=np.bool_)


def cli() -> Dict[str, Any]:
    """Parse command line."""
    parser = ArgumentParser(
        formatter_class=ArgumentDefaultsHelpFormatter,
        description='''Count enriched terms from modules.''')
    parser.add_argument('base-dir',
                        default='.',
                        type=Path,
                        help='Analyse files in this directory.')
    parser.add_argument('-A',
                        '--annotations',
                        default='GeneAnnotation.csv',
                        type=Path,
                        help='CSV of gene_ids: names')
    parser.add_argument('-o',
                        '--out-dir',
                        default='enrichments',
                        type=Path,
                        help='Directory created inside BASE-DIR')
    parser.add_argument('-s',
                        '--term-sep',
                        type=str,
                        nargs='+',
                        help='Terms are separated by',
                        default=('_', '-', '/', ' '))
    parser.add_argument('--keep-vague',
                        action='store_false',
                        dest='mask_vague',
                        help=f"Don't mask vague terms: {VAGUE}")
    parser.add_argument('-p',
                        '--p-value',
                        type=float,
                        help='Enrichment p-value cut off',
                        default=1e-3)
    return vars(parser.parse_args())


def main() -> int:
    """Entry Point."""
    cliargs = cli()
    out_dir = cliargs['base-dir'] / cliargs['out_dir']
    out_dir.mkdir(parents=True, exist_ok=True)
    (out_dir / 'sheets').mkdir(parents=True, exist_ok=True)
    (out_dir / 'images').mkdir(parents=True, exist_ok=True)
    alpha = cliargs['p_value']
    module_labels = pd.Series(parse_modules(
        cliargs['base-dir'])).groupby(level=0).value_counts().unstack()
    module_labels = module_labels.fillna(0).astype(np.bool_)
    module_labels.columns.name = 'modules'
    module_labels = module_labels.fillna(0).astype(np.bool_)
    annot_terms = parse_annotation(read_annotation(cliargs['annotations']))
    common_index = annot_terms.index.intersection(module_labels.index)
    module_labels = module_labels.loc[common_index]
    annot_terms = annot_terms.loc[common_index]
    cont = contingency_mxn_qual(qual_1=annot_terms,
                                qual_2=module_labels,
                                qual_1_name='terms',
                                qual_2_name='module')
    cont.to_csv(out_dir / 'sheets/terms_contingency.csv')
    chi2_res = analyse_skew(cont, alpha)
    chi2_score, pfish, logit = chi2_res['chi2_score'], chi2_res[
        'pfish'], chi2_res['logit']

    print(f'chi2_p-value: {chi2_score.pvalue}')
    if not pfish.empty:
        pfish.to_csv(out_dir / 'sheets/terms_pfisher.csv')
    if not logit.empty:
        logit.to_csv(out_dir / 'sheets/terms_logit.csv')
        plot_logit(logit,
                   'terms',
                   out_dir,
                   groupby='module',
                   units='',
                   xlabels='module',
                   resolution=5,
                   module_plots=True)
        plot_enumerate(logit, out_dir, 'No. of terms skewed')
    return 0


if __name__ == '__main__':
    sys.exit(main())
